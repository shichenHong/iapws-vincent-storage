Option Explicit

Private Temperatur_sat As Double
Public Function X_WD_W_PTX(ByVal Druck As Double, ByVal Temperatur As Double, ByVal x As Double) As Double
  'Hauptfunktion zur Verwaltung der Bereichsfunktionen zur Berechnung der Schallgeschwindigkeit bei vorgegebenem Druck und Temperatur
  'If CheckLicense() = False Then Return Fehlercode

  'On Error GoTo Abbruch

  Select Case WD_Bereich_PT(Druck, Temperatur)
    Case 1
      X_WD_W_PTX = WD_w1(Druck, Temperatur)   'ACHTUNG: w1 ist nicht korrekt programmiert, gibt falschen Wert zur¨¹ck(E.Diemel 2015)

    Case 2
      
      X_WD_W_PTX = WD_w2(Druck, Temperatur) 'ACHTUNG: w2 ist nicht korrekt programmiert, gibt falschen Wert zur¨¹ck'NEU EINGEBUNDEN

    Case 4                  'Schallgeschwindigkeit im Bereich 4 (2-phasengebiet) nicht durch Hebelgesetz bestimmbar
      GoTo Abbruch
      'If X < 0 Then X = 0
      'If X > 1 Then X = 1
      'Temperatur_Sat = X_WD_TS_P(Druck)
      'X_WD_W_PTX = (1 - X) * WD_w1(Druck, Temperatur_Sat) + X * WD_w2(Druck, Temperatur_Sat)

    Case 3
      X_WD_W_PTX = WD_w3(Druck, Temperatur)

    Case 5
      GoTo Abbruch
    Case -1
      GoTo Abbruch
  End Select

  Exit Function
  
Abbruch:
    X_WD_W_PTX = -1
End Function

Public Function X_WD_CV_PTX(ByVal Druck As Double, ByVal Temperatur As Double, ByVal x As Double) As Double
  'Hauptfunktion zur Verwaltung der Bereichsfunktionen zur Berechnung der spezifischen W?rmekapazit?t bei constantem Volumen bei vorgegebenem Druck und Temperatur
  'If CheckLicense() = False Then Return Fehlercode

  'On Error GoTo Abbruch

  Select Case WD_Bereich_PT(Druck, Temperatur)
    Case 1
      X_WD_CV_PTX = WD_cv1(Druck, Temperatur)

    Case 2

      X_WD_CV_PTX = WD_cv2(Druck, Temperatur) 'NEU EINGEBUNDEN    (E. Diemel 2015)

    Case 4
      ' so nicht korrekt!! FASCH!!!! CV im 2-phasengebiet nicht durch Hebelgesetz bestimmbar
      'If X < 0 Then X = 0   'NEU EINGEBUNDEN
      'If X > 1 Then X = 1
      'Temperatur_Sat = X_WD_TS_P(Druck)
      'X_WD_CV_PTX = (1 - X) * WD_cv1(Druck, Temperatur_Sat) + X * WD_cv2(Druck, Temperatur_Sat)

    Case 3
      X_WD_CV_PTX = WD_cv3(Druck, Temperatur)

    Case 5
      GoTo Abbruch
    Case -1
      GoTo Abbruch
  End Select

  Exit Function
Abbruch:
    X_WD_CV_PTX = -1
End Function

Public Function X_WD_CP_PTX(ByVal Druck As Double, ByVal Temperatur As Double, ByVal x As Double) As Double
  'Hauptfunktion zur Verwaltung der Bereichsfunktionen zur Berechnung der spezifischen W?rmekapazit?t bei constantem Druck bei vorgegebenem Druck und Temperatur
  'If CheckLicense() = False Then Return Fehlercode

  'On Error GoTo Abbruch

  Select Case WD_Bereich_PT(Druck, Temperatur)
    Case 1
      X_WD_CP_PTX = WD_cp1(Druck, Temperatur)

    Case 2

      X_WD_CP_PTX = WD_cp2(Druck, Temperatur) 'NEU EINGEBUNDEN (E. Diemel 2015)

    Case 4
      GoTo Abbruch 'Cp im bereich 4 nicht definiert!!!! Darf so nicht Berechnet werden !!
      'If X < 0 Then X = 0   'NEU EINGEBUNDEN
      'If X > 1 Then X = 1
      'Temperatur_Sat = X_WD_TS_P(Druck)
      'X_WD_CP_PTX = (1 - X) * WD_cp1(Druck, Temperatur_Sat) + X * WD_cp2(Druck, Temperatur_Sat)

    Case 3
      X_WD_CP_PTX = WD_cp3(Druck, Temperatur)

    Case 5
      GoTo Abbruch
    Case -1
      GoTo Abbruch
  End Select

  Exit Function
Abbruch:
    X_WD_CP_PTX = -1
End Function

Public Function X_WD_H_PTX(ByVal Druck As Double, ByVal Temperatur As Double, ByVal x As Double) As Double
  'Hauptfunktion zur Verwaltung der Bereichsfunktionen zur Berechnung der spezifischen Enthalpie bei vorgegebenem Druck, Temperatur und Wassergehalt
  'If CheckLicense() = False Then Return Fehlercode

  'On Error GoTo Abbruch

  Select Case WD_Bereich_PT(Druck, Temperatur)
    Case 1
      X_WD_H_PTX = WD_h1(Druck, Temperatur)

    Case 2
      X_WD_H_PTX = WD_h2(Druck, Temperatur)

    Case 4
      If x < 0 Then x = 0
      If x > 1 Then x = 1
      Temperatur_sat = X_WD_TS_P(Druck)
      X_WD_H_PTX = (1 - x) * WD_h1(Druck, Temperatur_sat) + x * WD_h2(Druck, Temperatur_sat)

    Case 3
      X_WD_H_PTX = WD_h3(Druck, Temperatur)

    Case 5
      GoTo Abbruch
    Case -1
      GoTo Abbruch
  End Select

  Exit Function

Abbruch:
    X_WD_H_PTX = -1
End Function

Public Function X_WD_T_HP(ByVal Enthalpie As Double, ByVal Druck As Double) As Double
  'Hauptfunktion zur Verwaltung der Bereichsfunktionen zur Berechnung der Temperatur bei vorgegebenem Enthalpie und Temperatur
  'If CheckLicense() = False Then Return Fehlercode

  'On Error GoTo Abbruch
  If Druck <= 0 Or Druck > 1000 Then GoTo Abbruch

  Select Case WD_Bereich_HP(Enthalpie, Druck)
    Case 1
      X_WD_T_HP = WD_t_hp1(Enthalpie, Druck) - 273.15

    Case 2
      If Druck < 40 Then
        X_WD_T_HP = WD_t_hp2a(Enthalpie, Druck) - 273.15
      Else
        If Druck < WD_PB_H(Enthalpie) Then
          X_WD_T_HP = WD_t_hp2b(Enthalpie, Druck) - 273.15
        Else
          X_WD_T_HP = WD_t_hp2c(Enthalpie, Druck) - 273.15
        End If
      End If
    Case 4
      X_WD_T_HP = X_WD_TS_P(Druck)

    Case 3
      GoTo Abbruch
    Case 5
      GoTo Abbruch
    Case -1
      GoTo Abbruch
  End Select

  Exit Function

Abbruch:
    X_WD_T_HP = -1
End Function

Public Function X_WD_S_PTX(ByVal Druck As Double, ByVal Temperatur As Double, ByVal x As Double) As Double
  'Hauptfunktion zur Verwaltung der Bereichsfunktionen zur Berechnung der spezifischen Entropie bei vorgegebenem Druck, Temperatur und Wassergehalt
  'If CheckLicense() = False Then Return Fehlercode

  'On Error GoTo Abbruch

  
  Select Case WD_Bereich_PT(Druck, Temperatur)
    Case 1
      X_WD_S_PTX = WD_s1(Druck, Temperatur)

    Case 2
      X_WD_S_PTX = WD_s2(Druck, Temperatur)

    Case 4
      If x < 0 Then x = 0
      If x > 1 Then x = 1
      Temperatur_sat = X_WD_TS_P(Druck)
      X_WD_S_PTX = (1 - x) * WD_s1(Druck, Temperatur_sat) + x * WD_s2(Druck, Temperatur_sat)

    Case 3
      X_WD_S_PTX = WD_s3(Druck, Temperatur)

    Case 5
      GoTo Abbruch
    Case -1
      GoTo Abbruch
  End Select

  Exit Function

Abbruch:
    X_WD_S_PTX = -1
End Function

Public Function X_WD_V_PTX(ByVal Druck As Double, ByVal Temperatur As Double, ByVal x As Double) As Double
  'Hauptfunktion zur Verwaltung der Bereichsfunktionen zur Berechnung des spezifischen Volumens  bei vorgegebenem Druck, Temperatur und Wassergehalt
  'If CheckLicense() = False Then Return Fehlercode

  'On Error GoTo Abbruch
  Dim Temperatur_sat As Double
  
  Select Case WD_Bereich_PT(Druck, Temperatur)
    Case 1
      X_WD_V_PTX = WD_v1(Druck, Temperatur)

    Case 2
      X_WD_V_PTX = WD_v2(Druck, Temperatur)

    Case 3
      X_WD_V_PTX = WD_v3(Druck, Temperatur)

    Case 4
      If x < 0 Then x = 0
      If x > 1 Then x = 1
      Temperatur_sat = X_WD_TS_P(Druck)
      X_WD_V_PTX = (1 - x) * WD_v1(Druck, Temperatur_sat) + x * WD_v2(Druck, Temperatur_sat)

    Case 5
      GoTo Abbruch
    Case -1
      GoTo Abbruch
  End Select

  Exit Function

Abbruch:
    X_WD_V_PTX = -1
End Function

Public Function X_WD_X_PT(ByVal Druck As Double, ByVal Temperatur As Double) As Double
  'Hauptfunktion zur Verwaltung der Bereichsfunktionen zur Berechnung des Wassergehaltes bei vorgegebenem Druck und Temperatur
  'If CheckLicense() = False Then Return Fehlercode

  'On Error GoTo Abbruch
  Select Case WD_Bereich_PT(Druck, Temperatur)
    Case 1
      X_WD_X_PT = 0

    Case 4              ' -2 == Angaben ¨¹berdefiniert, da in 2PhasenGebiet p = const. und T = const. -> Keine aussage ¨¹ber Lage m?glich. Angabe ¨¹ber extensive Gr??e spezifizieren!
      X_WD_X_PT = -2

    Case 2, 3, 5 'auch im Bereich 4!
      X_WD_X_PT = 1

    Case -1
      GoTo Abbruch
  End Select

  Exit Function

Abbruch:
    X_WD_X_PT = -1
End Function

Public Function X_WD_X_SP(ByVal Entropie As Double, ByVal Druck As Double) As Double
  'Hauptfunktion zur Verwaltung der Bereichsfunktionen zur Berechnung des Wassergehaltes bei vorgegebener Entropie und Druck
  'If CheckLicense() = False Then Return Fehlercode

  'On Error GoTo Abbruch
  If Druck <= p_min Or Druck > 1000 Then GoTo Abbruch
  If Entropie <= 0 Then GoTo Abbruch

  Select Case Druck               'p_krit = Druck des Kritischen Punktes
    Case Is <= p_krit#
      Dim Ts#
      Dim ss1#, ss2#, s0#

      Ts = X_WD_TS_P(Druck)
      ss1 = X_WD_S_PTX(Druck, Ts, 0)
      ss2 = X_WD_S_PTX(Druck, Ts, 1)
      s0 = Entropie

      X_WD_X_SP = (s0 - ss1) / (ss2 - ss1)

      If X_WD_X_SP < 0 Then X_WD_X_SP = 0
      If X_WD_X_SP > 1 Then X_WD_X_SP = 1

    Case Else
      X_WD_X_SP = 1

  End Select

  Exit Function

Abbruch:
    X_WD_X_SP = -1
End Function

Public Function X_WD_X_HT(ByVal Enthalpie As Double, ByVal Temperatur As Double) As Double
  'Hauptfunktion zur Verwaltung der Bereichsfunktionen zur Berechnung des Wassergehaltes bei vorgegebener Enthalpie und Temperatur
  'If CheckLicense() = False Then Return Fehlercode

  'On Error GoTo Abbruch
  If Temperatur < 0 Or Temperatur > 2000 Then GoTo Abbruch
  If Enthalpie <= 0 Then GoTo Abbruch

  Select Case Temperatur
    Case Is <= 350
      Dim ps#
      Dim hs1#, hs2#, h0#

      ps = X_WD_PS_T(Temperatur)
      hs1 = X_WD_H_PTX(ps, Temperatur, 0)
      hs2 = X_WD_H_PTX(ps, Temperatur, 1)
      h0 = Enthalpie

      X_WD_X_HT = (h0 - hs1) / (hs2 - hs1)

      If X_WD_X_HT < 0 Then X_WD_X_HT = 0
      If X_WD_X_HT > 1 Then X_WD_X_HT = 1

    Case Else
      X_WD_X_HT = 1

  End Select

  Exit Function

Abbruch:
    X_WD_X_HT = -1
End Function

Public Function X_WD_X_HP(ByVal Enthalpie As Double, ByVal Druck As Double) As Double
  'Hauptfunktion zur Verwaltung der Bereichsfunktionen zur Berechnung des Wassergehaltes bei vorgegebener Enthalpie und Druck
  'If CheckLicense() = False Then Return Fehlercode

  'On Error GoTo Abbruch
  If Druck <= p_min Or Druck > 1000 Then GoTo Abbruch
  If Enthalpie <= 0 Then GoTo Abbruch

  Select Case Druck               'p_krit = Druck des Kritischen Punktes
    Case Is <= p_krit#
      Dim Ts#
      Dim hs1#, hs2#, h0#

      Ts = X_WD_TS_P(Druck)
      hs1 = X_WD_H_PTX(Druck, Ts, 0)
      hs2 = X_WD_H_PTX(Druck, Ts, 1)
      h0 = Enthalpie

      X_WD_X_HP = (h0 - hs1) / (hs2 - hs1)

      If X_WD_X_HP < 0 Then X_WD_X_HP = 0
      If X_WD_X_HP > 1 Then X_WD_X_HP = 1

    Case Else
      X_WD_X_HP = 1

  End Select

  Exit Function

Abbruch:
    X_WD_X_HP = -1
End Function

Public Function X_WD_H_SP(ByVal Entropie As Double, ByVal Druck As Double) As Double
  'Hauptfunktion zur Verwaltung der Bereichsfunktionen zur Berechnung der Enthalpie bei vorgegebenem Druck und Entropie
  'If CheckLicense() = False Then Return Fehlercode

  'On Error GoTo Abbruch

  If Druck < 0.006 Or Druck > 220.64 Then GoTo Abbruch
  If Entropie < 0 Then GoTo Abbruch

  Dim Temperatur_Temp_1 As Double, Temperatur_Temp_2 As Double
  Dim EntropieSat_Temp_1 As Double, EntropieSat_Temp_2 As Double
  Dim Entropie_Temp_1 As Double, Entropie_Temp_2 As Double, Entropie_Gradient As Double
  Dim Zaehler%

  Temperatur_sat = X_WD_TS_P(Druck)
  EntropieSat_Temp_1 = X_WD_S_PTX(Druck, Temperatur_sat, 0)
  EntropieSat_Temp_2 = X_WD_S_PTX(Druck, Temperatur_sat, 1)
  Zaehler = 0

  If Entropie < EntropieSat_Temp_1 Then       'Region 1 - Water

    Temperatur_Temp_1 = Temperatur_sat
    Temperatur_Temp_2 = Temperatur_Temp_1 - 1

    Do While Math.Abs(Entropie_Temp_1 - Entropie) > Genauigkeit_Entropie And Zaehler < 1000
      Zaehler = Zaehler + 1

      Entropie_Temp_1 = X_WD_S_PTX(Druck, Temperatur_Temp_1, 0)
      Entropie_Temp_2 = X_WD_S_PTX(Druck, Temperatur_Temp_2, 0)

      Entropie_Gradient = (Entropie_Temp_2 - Entropie_Temp_1) / (Temperatur_Temp_2 - Temperatur_Temp_1)

      Temperatur_Temp_1 = Temperatur_Temp_1 + (Entropie - Entropie_Temp_1) / Entropie_Gradient
      Temperatur_Temp_2 = Temperatur_Temp_1 - 1

    Loop

    X_WD_H_SP = X_WD_H_PTX(Druck, Temperatur_Temp_1, 0)
    Temperatur = Temperatur_Temp_1
    x = 0

  ElseIf Entropie > EntropieSat_Temp_2 Then   'Region 2 - Steam

    Temperatur_Temp_1 = Temperatur_sat
    Temperatur_Temp_2 = Temperatur_Temp_1 + 1

    Do While Math.Abs(Entropie_Temp_1 - Entropie) > Genauigkeit_Entropie And Zaehler < 1000
      Zaehler = Zaehler + 1

      Entropie_Temp_1 = X_WD_S_PTX(Druck, Temperatur_Temp_1, 1)
      Entropie_Temp_2 = X_WD_S_PTX(Druck, Temperatur_Temp_2, 1)

      Entropie_Gradient = (Entropie_Temp_2 - Entropie_Temp_1) / (Temperatur_Temp_2 - Temperatur_Temp_1)

      Temperatur_Temp_1 = Temperatur_Temp_1 + (Entropie - Entropie_Temp_1) / Entropie_Gradient
      Temperatur_Temp_2 = Temperatur_Temp_1 + 1

    Loop

    X_WD_H_SP = X_WD_H_PTX(Druck, Temperatur_Temp_1, 1)
    Temperatur = Temperatur_Temp_1
    x = 1

  Else        'Region 4 - 2phase region

    Temperatur = Temperatur_sat
    x = (Entropie - EntropieSat_Temp_1) / (EntropieSat_Temp_2 - EntropieSat_Temp_1)
    X_WD_H_SP = X_WD_H_PTX(Druck, Temperatur, x)

  End If

  Exit Function

Abbruch:
    Temperatur = -1
    x = -1
    X_WD_H_SP = -1
End Function

Public Function X_WD_P_HT(ByVal Enthalpie As Double, ByVal Temperatur As Double) As Double
  'Hauptfunktion zur Verwaltung der Bereichsfunktionen zur Berechnung des Druckes bei vorgegebener Enthalpie und Temperatur
  'If CheckLicense() = False Then Return Fehlercode

  'im Bereich 1, 2 und 4!

  'On Error GoTo Abbruch
  If Temperatur < 0 Or Temperatur > 2000 Then GoTo Abbruch
  If Enthalpie < 0 Then GoTo Abbruch

  Dim h0#, hmin#, hmax#, ht#
  Dim pt#, pmin#, pmax#, ps#
  Dim Zaehler%

  h0 = Enthalpie

  Select Case WD_Bereich_HT(Enthalpie, Temperatur)
    Case Is = 1 'Bereich 1 oder 4

      X_WD_P_HT = -1
      Exit Function

      'ab hier dann die Umkehrfunktion

      pmin = X_WD_PS_T(Temperatur)
      pmax = 1000

      hmin = X_WD_H_PTX(pmin, Temperatur, 0)
      hmax = X_WD_H_PTX(pmax, Temperatur, 0)

      If h0 > hmax Then
        ps = X_WD_PS_T(Temperatur)
        hmin = X_WD_H_PTX(ps, Temperatur, 0)
        hmax = X_WD_H_PTX(ps, Temperatur, 1)

        If h0 >= hmin And h0 <= hmax Then X_WD_P_HT = ps: Exit Function
      End If

      ht = hmax
      pt = pmax

      Do While Math.Abs(h0 - ht) > 0.00000001 And Zaehler < 1000
        Zaehler = Zaehler + 1

        pt = pmin + (h0 - hmin) / (ht - hmin) * (pt - pmin)
        ht = X_WD_H_PTX(pt, Temperatur, 0)

      Loop

    Case Is = 4

      ps = X_WD_PS_T(Temperatur)
      pt = ps

    Case Is = 2

      X_WD_P_HT = -1
      Exit Function

      'ab hier dann die Umkehrfunktion

      Select Case Temperatur
        Case Is <= 373.946 '350
          pmax = X_WD_PS_T(Temperatur)

        Case 373.946 To 590
          pmax = WD_PB_T(Temperatur)

        Case 590 To 800
          pmax = 1000

      End Select

      pmin = 0

      hmax = X_WD_H_PTX(pmax, Temperatur, 1)
      ht = X_WD_H_PTX(pmin, Temperatur, 1)
      pt = pmin

      Do While Math.Abs(h0 - ht) > 0.00000001 And Zaehler < 1000
        Zaehler = Zaehler + 1

        pt = pmax - (h0 - hmax) / (ht - hmax) * (pmax - pt)
        ht = X_WD_H_PTX(pt, Temperatur, 1)

      Loop

    Case Else 'weder Bereich 1 noch 2
      pt = -1

  End Select

  X_WD_P_HT = pt

  Exit Function

Abbruch:

    X_WD_P_HT = -1
End Function

Public Function X_WD_T_SP(ByVal Entropie As Double, ByVal Druck As Double) As Double
  'Hauptfunktion zur Verwaltung der Bereichsfunktionen zur Berechnung der Temperatur bei vorgegebenem Druck und Entropie
  'If CheckLicense() = False Then Return Fehlercode

  'On Error GoTo Abbruch

  Select Case WD_Bereich_SP(Entropie, Druck)
    Case 1

      'Exit Function
      X_WD_T_SP = WD_t_sp1(Entropie, Druck) - 273.15

    Case 2
      If Druck < 40 Then
        'Exit Function
        X_WD_T_SP = WD_t_sp2a(Entropie, Druck) - 273.15
      Else
        'Exit Function
        If Entropie > 5.85 Then
          X_WD_T_SP = WD_t_sp2b(Entropie, Druck) - 273.15
        Else
          X_WD_T_SP = WD_t_sp2c(Entropie, Druck) - 273.15
        End If
      End If
    Case 4
      Exit Function
      X_WD_T_SP = X_WD_TS_P(Druck)

    Case 3
      GoTo Abbruch
    Case 5
      GoTo Abbruch
    Case -1
      GoTo Abbruch
  End Select

  Exit Function

Abbruch:
    X_WD_T_SP = -1
End Function

Public Function X_WD_dViscosity_PT(ByVal Druck As Double, ByVal Temperatur As Double) As Double
  'Hauptfunktion zur Verwaltung der Bereichsfunktionen zur Berechnung der dynamische Viskositaet bei vorgegebenem Druck und Temperatur
  'Einheit 10^-6 Pas
  If Temperatur < 0 Or Temperatur > 800 Then GoTo Abbruch
  If Druck < 0.006 Or Druck > 1000 Then GoTo Abbruch
  
  Dim Density As Double
  
  Density = 1 / X_WD_V_PTX(Druck, Temperatur, X_WD_X_PT(Druck, Temperatur))   'speizifische Volumen zu Dichte umgerechnet
  
  X_WD_dViscosity_PT = WD_dvisko_DT(Density, Temperatur)   'Aufruf der Viskositaetsfunktion nach IAPWS 2008


  If X_WD_dViscosity_PT < 0 Then GoTo Abbruch

  Exit Function

Abbruch:
    X_WD_dViscosity_PT = -1
End Function

Public Function X_WD_kViskositaet_PT(ByVal Druck As Double, ByVal Temperatur As Double) As Double

  'If CheckLicense() = False Then Return Fehlercode

  'ny in 10^-6 m2/s

  'On Error GoTo Abbruch

  If Temperatur < 0 Or Temperatur > 800 Then GoTo Abbruch
  If Druck < 0.006 Or Druck > 1000 Then GoTo Abbruch
  Dim eta, rho As Double
  eta = X_WD_dViscosity_PT(Druck, Temperatur)
  rho = 1 / X_WD_V_PTX(Druck, Temperatur, X_WD_X_PT(Druck, Temperatur))

  X_WD_kViskositaet_PT = eta / rho

  If X_WD_kViskositaet_PT < 0 Then GoTo Abbruch

  Exit Function

Abbruch:
    X_WD_kViskositaet_PT = -1
End Function

Public Function X_WD_lambda_PTX(ByVal Druck As Double, ByVal Temperatur As Double) As Double

  'If CheckLicense() = False Then Return Fehlercode

  'On Error GoTo Abbruch
  'If Berechtigung = False Then GoTo Abbruch
  If Temperatur < 0 Or Temperatur > 800 Then GoTo Abbruch
  If Druck < 0.006 Or Druck > 1000 Then GoTo Abbruch

  Dim Density As Double
  Density = 1 / X_WD_V_PTX(Druck, Temperatur, X_WD_X_PT(Druck, Temperatur))
  
  X_WD_lambda_PTX = WD_WaermeLF_DT(Density, Temperatur)

  Exit Function

Abbruch:
    X_WD_lambda_PTX = -1
End Function

Public Function X_WD_Prandtl(ByVal Druck As Double, ByVal Temperatur As Double) As Double

  'If CheckLicense() = False Then Return Fehlercode

  'On Error GoTo Abbruch
  'If Berechtigung = False Then GoTo Abbruch
  If Temperatur < 0 Or Temperatur > 800 Then GoTo Abbruch
  If Druck < 0.006 Or Druck > 1000 Then GoTo Abbruch
  
  Dim eta, cp, lambda As Double
  eta = X_WD_dViscosity_PT(Druck, Temperatur)
  eta = eta * 10 ^ -6
  
  cp = X_WD_CP_PTX(Druck, Temperatur, X_WD_X_PT(Druck, Temperatur))
  cp = cp * 10 ^ 3
  
  lambda = X_WD_lambda_PTX(Druck, Temperatur)

  X_WD_Prandtl = eta * cp / lambda

  Exit Function

Abbruch:
    X_WD_Prandtl = -1
End Function

'a = lamdba / (rho * cp) Temperaturkoeffizient in Wärmeleitung
Public Function X_WD_a(ByVal Druck As Double, ByVal Temperatur As Double) As Double
  If Temperatur < 0 Or Temperatur > 800 Then GoTo Abbruch
  If Druck < 0.006 Or Druck > 1000 Then GoTo Abbruch
  
  Dim lambda, rho, cp As Double
  lambda = X_WD_lambda_PTX(Druck, Temperatur)
  rho = 1 / X_WD_V_PTX(Druck, Temperatur, X_WD_X_PT(Druck, Temperatur))
  cp = X_WD_CP_PTX(Druck, Temperatur, X_WD_X_PT(Druck, Temperatur))
  
  X_WD_a = lambda / (rho * cp)
  
  Exit Function

Abbruch:
    X_WD_Prandtl = -1
  
End Function

Public Function X_WD_IsenExp(ByVal Druck As Double, ByVal Temperatur As Double) As Double
'Achtung!!!! Diese Gleichung gilt nur fuer ideale Gase. Fuer Wasser und Dampf muss noch angepasst werden!
  If Temperatur < 0 Or Temperatur > 800 Then GoTo Abbruch
  If Druck < 0.006 Or Druck > 1000 Then GoTo Abbruch
  
'  Dim T_in_K, v, cp, dvdT, dvdp As Double
'  T_in_K = Temperatur + 273.15
'  v = X_WD_V_PTX(Druck, Temperatur, X_WD_X_PT(Druck, Temperatur))
'  cp = X_WD_CP_PTX(Druck, Temperatur, X_WD_X_PT(Druck, Temperatur))
'
'  Select Case WD_Bereich_PT(Druck, Temperatur)
'
'  Case 1
'    dvdT = v * WD_alphaV1(Druck, Temperatur)
'    dvdp = -v * WD_kappaT1(Druck, Temperatur)
'  Case 2
'    dvdT = v * WD_alphaV2(Druck, Temperatur)
'    dvdp = -v * WD_kappaT2(Druck, Temperatur)
'  Case 3
'    dvdT = WD_alphaP3(Druck, Temperatur) / WD_betaP3(Druck, Temperatur)
'    dvdp = -1 / (Druck * WD_betaP3(Druck, Temperatur))
'  Case 4
'    GoTo Abbruch
'
'  End Select
'
'  X_WD_IsenExp = -v / Druck / (T_in_K / cp * dvdT ^ 2 + dvdp)
  Dim v, cp, cv, dpdv As Double
  v = X_WD_V_PTX(Druck, Temperatur, X_WD_X_PT(Druck, Temperatur))
  cp = X_WD_CP_PTX(Druck, Temperatur, X_WD_X_PT(Druck, Temperatur))
  cv = X_WD_CV_PTX(Druck, Temperatur, X_WD_X_PT(Druck, Temperatur))
  
  Select Case WD_Bereich_PT(Druck, Temperatur)
  Case 1
    dpdv = -1 / (v * WD_kappaT1(Druck, Temperatur))
  Case 2
    dpdv = -1 / (v * WD_kappaT2(Druck, Temperatur))
  Case 3
    dpdv = -Druck * WD_betaP3(Druck, Temperatur)
  Case 4
    GoTo Abbruch
    
  End Select
  
  X_WD_IsenExp = -v / Druck * dpdv * cp / cv
    
  Exit Function
  
Abbruch:
    X_WD_IsenExp = -1

End Function

Public Function X_WD_cpm_wahr_PTX(ByVal Druck As Double, ByVal Temperatur As Double, ByVal x As Double) As Double

  'If CheckLicense() = False Then Return Fehlercode

  'On Error GoTo Abbruch
  'If Berechtigung = False Then GoTo Abbruch
  If Temperatur < 0 Then GoTo Abbruch
  If Druck < 0.006 Or Druck > 220.64 Then GoTo Abbruch

  'X_WD_cpm_wahr_PTX = cpW(Temperatur + 273.15, ByVal Druck as Double)

  Exit Function

Abbruch:
    X_WD_cpm_wahr_PTX = -1
End Function

Public Function X_WD_emissionsgrad(ByVal Schichtdicke As Double, ByVal Druck As Double, ByVal Temperatur As Double) As Double

  'If CheckLicense() = False Then Return Fehlercode

  'On Error GoTo Abbruch
  'If Berechtigung = False Then GoTo Abbruch
  If Temperatur < 0 Then GoTo Abbruch
  If Druck < 0.006 Or Druck > 220.64 Then GoTo Abbruch

  Dim emissionsgrad_H2O As Double, f_H2O As Double, a As Double, A1 As Double, A2 As Double, A3 As Double, A4 As Double, tau As Double, Temperatur_K As Double, y As Double, z As Double, i As Double

  X_WD_emissionsgrad = 0
  Temperatur_K = Temperatur + 273.15

  If Temperatur_K > 750 Then
    tau = Temperatur_K / 1000
  Else
    tau = 0.75
  End If

  A1 = 1.888 - 2.053 * Math.Log(tau)
  A2 = Druck * (1 + 4.9 / Druck * (273.15 / Temperatur_K) ^ 0.5) + 1.1 * (Temperatur_K / 1000 ^ -1.4)
  A3 = 0.888 - 2.053 * Math.Log(tau)
  A4 = Druck * (1 + 4.9 / Druck * (273.15 / Temperatur_K) ^ 0.5) + 1.1 * (Temperatur_K / 1000 ^ -1.4)
  a = A1 * A2 / (A3 + A4)

  f_H2O = 1 + (a - 1) * Math.Exp(-0.5 * (Math.Log(0.132 * (Temperatur_K / 1000) ^ 2 / (Druck * Schichtdicke))) ^ 2)

  If f_H2O > a Then f_H2O = a

  y = Math.Log(1 * Druck * Schichtdicke)
  z = Math.Log(Temperatur_K)

  i = Emissionsgrad_H2O_1 * z ^ 0 * y ^ 0 + Emissionsgrad_H2O_2 * z ^ 1 * y ^ 0 + Emissionsgrad_H2O_3 * z ^ 2 * y ^ 0 + Emissionsgrad_H2O_4 * z ^ 3 * y ^ 0 + Emissionsgrad_H2O_5 * z ^ 0 * y ^ 1 + Emissionsgrad_H2O_6 * z ^ 1 * y ^ 1 + Emissionsgrad_H2O_7 * z ^ 2 * y ^ 1 + Emissionsgrad_H2O_8 * z ^ 3 * y ^ 1 + Emissionsgrad_H2O_9 * z ^ 0 * y ^ 2 + Emissionsgrad_H2O_10 * z ^ 1 * y ^ 2 + Emissionsgrad_H2O_11 * z ^ 2 * y ^ 2 + Emissionsgrad_H2O_12 * z ^ 3 * y ^ 2 + Emissionsgrad_H2O_13 * z ^ 0 * y ^ 3 + Emissionsgrad_H2O_14 * z ^ 1 * y ^ 3 + Emissionsgrad_H2O_15 * z ^ 2 * y ^ 3 + Emissionsgrad_H2O_16 * z ^ 3 * y ^ 3

  emissionsgrad_H2O = Math.Exp(i)

  X_WD_emissionsgrad = f_H2O * emissionsgrad_H2O = Math.Exp(i) ' X_AG_emissionsgrad(ByVal Schichtdicke as Double, 0, 1, 1, ByVal Temperatur as Double)

  Exit Function

Abbruch:
    X_WD_emissionsgrad = -1
End Function

Public Function X_WD_absorptionsgrad(ByVal Schichtdicke As Double, ByVal Druck As Double, ByVal Temperatur_Gas As Double, ByVal Temperatur_Wand As Double) As Double

  'If CheckLicense() = False Then Return Fehlercode

  'On Error GoTo Abbruch
  'If Berechtigung = False Then GoTo Abbruch
  If Temperatur < 0 Then GoTo Abbruch
  If Druck < 0.006 Or Druck > 220.64 Then GoTo Abbruch

  Dim absorptionsgrad_H2O As Double, f_H2O As Double, a As Double, A1 As Double, A2 As Double, A3 As Double, A4 As Double, tau As Double, Temperatur_K As Double, y As Double, z As Double, i As Double

  X_WD_absorptionsgrad = 0

  X_WD_absorptionsgrad = X_WD_emissionsgrad(Schichtdicke, 1 * ((Temperatur_Wand + 273.15) / (Temperatur_Gas + 273.15)), Temperatur)

  Exit Function

Abbruch:
    X_WD_absorptionsgrad = -1
End Function
