'##############################################################################################################################
'################################ Ab hier die Konstanten für die Berechnung der Stoffwerte ##################################
'##################################### von Wasser und Wasserdampf nach IAPWS IF 97 ############################################
'################################# Alle Tabellen- und Seitennummern beziehen sich auf die ####################################
'################################## offizielle Release aus 2007 auf Grundlage der IF - 97 #####################################
'##############################################################################################################################

'#######################-Bereich1-NEU-############################################################################
'#######################-Koeffizienten li - ji - ni (Tabelle 2, S7) -#############################################

Option Private Module

Function Array_li_Bereich1()
  Dim li_Bereich1() As Integer
  ReDim li_Bereich1(34)

  li_Bereich1(0) = 0
  li_Bereich1(1) = 0
  li_Bereich1(2) = 0
  li_Bereich1(3) = 0
  li_Bereich1(4) = 0
  li_Bereich1(5) = 0
  li_Bereich1(6) = 0
  li_Bereich1(7) = 0
  li_Bereich1(8) = 1
  li_Bereich1(9) = 1
  li_Bereich1(10) = 1
  li_Bereich1(11) = 1
  li_Bereich1(12) = 1
  li_Bereich1(13) = 1
  li_Bereich1(14) = 2
  li_Bereich1(15) = 2
  li_Bereich1(16) = 2
  li_Bereich1(17) = 2
  li_Bereich1(18) = 2
  li_Bereich1(19) = 3
  li_Bereich1(20) = 3
  li_Bereich1(21) = 3
  li_Bereich1(22) = 4
  li_Bereich1(23) = 4
  li_Bereich1(24) = 4
  li_Bereich1(25) = 5
  li_Bereich1(26) = 8
  li_Bereich1(27) = 8
  li_Bereich1(28) = 21
  li_Bereich1(29) = 23
  li_Bereich1(30) = 29
  li_Bereich1(31) = 30
  li_Bereich1(32) = 31
  li_Bereich1(33) = 32
  Array_li_Bereich1 = li_Bereich1()

  'Return li_Bereich1
End Function


Function Array_ji_Bereich1()
  Dim ji_Bereich1() As Integer
  ReDim ji_Bereich1(34)

  ji_Bereich1(0) = -2
  ji_Bereich1(1) = -1
  ji_Bereich1(2) = 0
  ji_Bereich1(3) = 1
  ji_Bereich1(4) = 2
  ji_Bereich1(5) = 3
  ji_Bereich1(6) = 4
  ji_Bereich1(7) = 5
  ji_Bereich1(8) = -9
  ji_Bereich1(9) = -7
  ji_Bereich1(10) = -1
  ji_Bereich1(11) = 0
  ji_Bereich1(12) = 1
  ji_Bereich1(13) = 3
  ji_Bereich1(14) = -3
  ji_Bereich1(15) = 0
  ji_Bereich1(16) = 1
  ji_Bereich1(17) = 3
  ji_Bereich1(18) = 17
  ji_Bereich1(19) = -4
  ji_Bereich1(20) = 0
  ji_Bereich1(21) = 6
  ji_Bereich1(22) = -5
  ji_Bereich1(23) = -2
  ji_Bereich1(24) = 10
  ji_Bereich1(25) = -8
  ji_Bereich1(26) = -11
  ji_Bereich1(27) = -6
  ji_Bereich1(28) = -29
  ji_Bereich1(29) = -31
  ji_Bereich1(30) = -38
  ji_Bereich1(31) = -39
  ji_Bereich1(32) = -40
  ji_Bereich1(33) = -41
  Array_ji_Bereich1 = ji_Bereich1()

  'Return ji_Bereich1
End Function


Function Array_ni_Bereich1()
  Dim ni_Bereich1() As Double
  ReDim ni_Bereich1(34)

  ni_Bereich1(0) = 0.14632971213167
  ni_Bereich1(1) = -0.84548187169114
  ni_Bereich1(2) = -0.3756360367204 * 10 ^ 1
  ni_Bereich1(3) = 0.33855169168385 * 10 ^ 1
  ni_Bereich1(4) = -0.95791963387872
  ni_Bereich1(5) = 0.15772038513228
  ni_Bereich1(6) = -0.16616417199501 * 10 ^ -1
  ni_Bereich1(7) = 0.81214629983568 * 10 ^ -3
  ni_Bereich1(8) = 0.28319080123804 * 10 ^ -3
  ni_Bereich1(9) = -0.60706301565874 * 10 ^ -3
  ni_Bereich1(10) = -0.18990068218419 * 10 ^ -1
  ni_Bereich1(11) = -0.32529748770505 * 10 ^ -1
  ni_Bereich1(12) = -0.21841717175414 * 10 ^ -1
  ni_Bereich1(13) = -0.5283835796993 * 10 ^ -4
  ni_Bereich1(14) = -0.47184321073267 * 10 ^ -3
  ni_Bereich1(15) = -0.30001780793026 * 10 ^ -3
  ni_Bereich1(16) = 0.47661393906987 * 10 ^ -4
  ni_Bereich1(17) = -0.44141845330846 * 10 ^ -5
  ni_Bereich1(18) = -0.72694996297594 * 10 ^ -15
  ni_Bereich1(19) = -0.31679644845054 * 10 ^ -4
  ni_Bereich1(20) = -0.28270797985312 * 10 ^ -5
  ni_Bereich1(21) = -0.85205128120103 * 10 ^ -9
  ni_Bereich1(22) = -0.22425281908 * 10 ^ -5
  ni_Bereich1(23) = -0.65171222895601 * 10 ^ -6
  ni_Bereich1(24) = -0.14341729937924 * 10 ^ -12
  ni_Bereich1(25) = -0.40516996860117 * 10 ^ -6
  ni_Bereich1(26) = -0.12734301741641 * 10 ^ -8
  ni_Bereich1(27) = -0.17424871230634 * 10 ^ -9
  ni_Bereich1(28) = -0.68762131295531 * 10 ^ -18
  ni_Bereich1(29) = 0.14478307828521 * 10 ^ -19
  ni_Bereich1(30) = 0.26335781662795 * 10 ^ -22
  ni_Bereich1(31) = -0.11947622640071 * 10 ^ -22
  ni_Bereich1(32) = 0.18228094581404 * 10 ^ -23
  ni_Bereich1(33) = -0.93537087292458 * 10 ^ -25
  Array_ni_Bereich1 = ni_Bereich1()

  'Return ni_Bereich1
End Function

'#######################-Bereich2-NEU-############################################################################
'#######################-Koeffizienten ji0 - ni0 (Tabelle 10, S.13) -#############################################

Function Array_ji0_Bereich2()
  Dim ji0_Bereich2() As Integer
  ReDim ji0_Bereich2(9)

  ji0_Bereich2(0) = 0
  ji0_Bereich2(1) = 1
  ji0_Bereich2(2) = -5
  ji0_Bereich2(3) = -4
  ji0_Bereich2(4) = -3
  ji0_Bereich2(5) = -2
  ji0_Bereich2(6) = -1
  ji0_Bereich2(7) = 2
  ji0_Bereich2(8) = 3
  Array_ji0_Bereich2 = ji0_Bereich2()

  'Return ji0_Bereich2
End Function


Function Array_ni0_Bereich2()
  Dim ni0_Bereich2() As Double
  ReDim ni0_Bereich2(9)

  ni0_Bereich2(0) = -0.96927686500217 * 10 ^ 1
  ni0_Bereich2(1) = 0.10086655968018 * 10 ^ 2
  ni0_Bereich2(2) = -0.5608791128302 * 10 ^ -2
  ni0_Bereich2(3) = 0.71452738081455 * 10 ^ -1
  ni0_Bereich2(4) = -0.40710498223928
  ni0_Bereich2(5) = 0.14240819171444 * 10 ^ 1
  ni0_Bereich2(6) = -0.4383951131945 * 10 ^ 1
  ni0_Bereich2(7) = -0.28408632460772
  ni0_Bereich2(8) = 0.21268463753307 * 10 ^ -1
  Array_ni0_Bereich2 = ni0_Bereich2()

  'Return ni0_Bereich2
End Function

' siehe kommentar Tabelle 10
Function Array_ni0_Bereich2m()
  Dim ni0_Bereich2m() As Double        ' ni0a_Bereich2 weil abweichende Koeffizienten n10a und n20a für Gl. 18
  ReDim ni0_Bereich2m(9)

  ni0_Bereich2m(0) = -0.96937268393049 * 10 ^ 1
  ni0_Bereich2m(1) = 0.10087275970006 * 10 ^ 2
  ni0_Bereich2m(2) = -0.5608791128302 * 10 ^ -2
  ni0_Bereich2m(3) = 0.71452738081455 * 10 ^ -1
  ni0_Bereich2m(4) = -0.40710498223928
  ni0_Bereich2m(5) = 0.14240819171444 * 10 ^ 1
  ni0_Bereich2m(6) = -0.4383951131945 * 10 ^ 1
  ni0_Bereich2m(7) = -0.28408632460772
  ni0_Bereich2m(8) = 0.21268463753307 * 10 ^ -1
  Array_ni0_Bereich2m = ni0_Bereich2m()

  'Return ni0_Bereich2m
End Function

'#######################-Koeffizienten li- ji - ni -  (Tabelle 11, S.14) -#############################################


Function Array_li_Bereich2()
  Dim li_Bereich2() As Integer
  ReDim li_Bereich2(43)

  li_Bereich2(0) = 1
  li_Bereich2(1) = 1
  li_Bereich2(2) = 1
  li_Bereich2(3) = 1
  li_Bereich2(4) = 1
  li_Bereich2(5) = 2
  li_Bereich2(6) = 2
  li_Bereich2(7) = 2
  li_Bereich2(8) = 2
  li_Bereich2(9) = 2
  li_Bereich2(10) = 3
  li_Bereich2(11) = 3
  li_Bereich2(12) = 3
  li_Bereich2(13) = 3
  li_Bereich2(14) = 3
  li_Bereich2(15) = 4
  li_Bereich2(16) = 4
  li_Bereich2(17) = 4
  li_Bereich2(18) = 5
  li_Bereich2(19) = 6
  li_Bereich2(20) = 6
  li_Bereich2(21) = 6
  li_Bereich2(22) = 7
  li_Bereich2(23) = 7
  li_Bereich2(24) = 7
  li_Bereich2(25) = 8
  li_Bereich2(26) = 8
  li_Bereich2(27) = 9
  li_Bereich2(28) = 10
  li_Bereich2(29) = 10
  li_Bereich2(30) = 10
  li_Bereich2(31) = 16
  li_Bereich2(32) = 16
  li_Bereich2(33) = 18
  li_Bereich2(34) = 20
  li_Bereich2(35) = 20
  li_Bereich2(36) = 20
  li_Bereich2(37) = 21
  li_Bereich2(38) = 22
  li_Bereich2(39) = 23
  li_Bereich2(40) = 24
  li_Bereich2(41) = 24
  li_Bereich2(42) = 24
  Array_li_Bereich2 = li_Bereich2()

  'Return li_Bereich2
End Function


Function Array_ji_Bereich2()
  Dim ji_Bereich2() As Integer
  ReDim ji_Bereich2(43)

  ji_Bereich2(0) = 0
  ji_Bereich2(1) = 1
  ji_Bereich2(2) = 2
  ji_Bereich2(3) = 3
  ji_Bereich2(4) = 6
  ji_Bereich2(5) = 1
  ji_Bereich2(6) = 2
  ji_Bereich2(7) = 4
  ji_Bereich2(8) = 7
  ji_Bereich2(9) = 36
  ji_Bereich2(10) = 0
  ji_Bereich2(11) = 1
  ji_Bereich2(12) = 3
  ji_Bereich2(13) = 6
  ji_Bereich2(14) = 35
  ji_Bereich2(15) = 1
  ji_Bereich2(16) = 2
  ji_Bereich2(17) = 3
  ji_Bereich2(18) = 7
  ji_Bereich2(19) = 3
  ji_Bereich2(20) = 16
  ji_Bereich2(21) = 35
  ji_Bereich2(22) = 0
  ji_Bereich2(23) = 11
  ji_Bereich2(24) = 25
  ji_Bereich2(25) = 8
  ji_Bereich2(26) = 36
  ji_Bereich2(27) = 13
  ji_Bereich2(28) = 4
  ji_Bereich2(29) = 10
  ji_Bereich2(30) = 14
  ji_Bereich2(31) = 29
  ji_Bereich2(32) = 50
  ji_Bereich2(33) = 57
  ji_Bereich2(34) = 20
  ji_Bereich2(35) = 35
  ji_Bereich2(36) = 48
  ji_Bereich2(37) = 21
  ji_Bereich2(38) = 53
  ji_Bereich2(39) = 39
  ji_Bereich2(40) = 26
  ji_Bereich2(41) = 40
  ji_Bereich2(42) = 58
  
  Array_ji_Bereich2 = ji_Bereich2()

  'Return ji_Bereich2
End Function


Function Array_ni_Bereich2()
  Dim ni_Bereich2() As Double
  ReDim ni_Bereich2(42)

  ni_Bereich2(0) = -0.17731742473213 * 10 ^ -2
  ni_Bereich2(1) = -0.17834862292358 * 10 ^ -1
  ni_Bereich2(2) = -0.45996013696365 * 10 ^ -1
  ni_Bereich2(3) = -0.57581259083432 * 10 ^ -1
  ni_Bereich2(4) = -0.5032527872793 * 10 ^ -1
  ni_Bereich2(5) = -0.33032641670203 * 10 ^ -4
  ni_Bereich2(6) = -0.18948987516315 * 10 ^ -3
  ni_Bereich2(7) = -0.39392777243355 * 10 ^ -2
  ni_Bereich2(8) = -0.43797295650573 * 10 ^ -1
  ni_Bereich2(9) = -0.26674547914087 * 10 ^ -4
  ni_Bereich2(10) = 0.20481737692309 * 10 ^ -7
  ni_Bereich2(11) = 0.43870667284435 * 10 ^ -6
  ni_Bereich2(12) = -0.3227767723857 * 10 ^ -4
  ni_Bereich2(13) = -0.15033924542148 * 10 ^ -2
  ni_Bereich2(14) = -0.40668253562649 * 10 ^ -1
  ni_Bereich2(15) = -0.78847309559367 * 10 ^ -9
  ni_Bereich2(16) = 0.12790717852285 * 10 ^ -7
  ni_Bereich2(17) = 0.48225372718507 * 10 ^ -6
  ni_Bereich2(18) = 0.22922076337661 * 10 ^ -5
  ni_Bereich2(19) = -0.16714766451061 * 10 ^ -10
  ni_Bereich2(20) = -0.21171472321355 * 10 ^ -2
  ni_Bereich2(21) = -0.23895741934104 * 10 ^ 2
  ni_Bereich2(22) = -0.5905956432427 * 10 ^ -17
  ni_Bereich2(23) = -0.12621808899101 * 10 ^ -5
  ni_Bereich2(24) = -0.38946842435739 * 10 ^ -1
  ni_Bereich2(25) = 0.11256211360459 * 10 ^ -10
  ni_Bereich2(26) = -0.82311340897998 * 10 ^ 1
  ni_Bereich2(27) = 0.19809712802088 * 10 ^ -7
  ni_Bereich2(28) = 0.10406965210174 * 10 ^ -18
  ni_Bereich2(29) = -0.10234747095929 * 10 ^ -12
  ni_Bereich2(30) = -0.10018179379511 * 10 ^ -8
  ni_Bereich2(31) = -0.80882908646985 * 10 ^ -10
  ni_Bereich2(32) = 0.10693031879409
  ni_Bereich2(33) = -0.33662250574171
  ni_Bereich2(34) = 0.89185845355421 * 10 ^ -24
  ni_Bereich2(35) = 0.30629316876232 * 10 ^ -12
  ni_Bereich2(36) = -0.42002467698208 * 10 ^ -5
  ni_Bereich2(37) = -0.59056029685639 * 10 ^ -25
  ni_Bereich2(38) = 0.37826947613457 * 10 ^ -5
  ni_Bereich2(39) = -0.12768608934681 * 10 ^ -14
  ni_Bereich2(40) = 0.73087610595061 * 10 ^ -28
  ni_Bereich2(41) = 0.55414715350778 * 10 ^ -16
  ni_Bereich2(42) = -0.9436970724121 * 10 ^ -6
  Array_ni_Bereich2 = ni_Bereich2()

  'Return ni_Bereich2
End Function

'########################################## Bereich 2 METASTABIL p < 10 MPA ########################################
'#######################-Koeffizienten li- ji - ni - (Tabelle 16, S.18) -###########################################


Function Array_li_Bereich2m()
  Dim li_Bereich2m() As Integer
  ReDim li_Bereich2m(13)

  li_Bereich2m(0) = 1
  li_Bereich2m(1) = 1
  li_Bereich2m(2) = 1
  li_Bereich2m(3) = 1
  li_Bereich2m(4) = 2
  li_Bereich2m(5) = 2
  li_Bereich2m(6) = 2
  li_Bereich2m(7) = 3
  li_Bereich2m(8) = 3
  li_Bereich2m(9) = 4
  li_Bereich2m(10) = 4
  li_Bereich2m(11) = 5
  li_Bereich2m(12) = 5
  Array_li_Bereich2m = li_Bereich2m()

  'Return li_Bereich2m
End Function


Function Array_ji_Bereich2m()
  Dim ji_Bereich2m() As Integer
  ReDim ji_Bereich2m(13)

  ji_Bereich2m(0) = 0
  ji_Bereich2m(1) = 2
  ji_Bereich2m(2) = 5
  ji_Bereich2m(3) = 11
  ji_Bereich2m(4) = 1
  ji_Bereich2m(5) = 7
  ji_Bereich2m(6) = 16
  ji_Bereich2m(7) = 4
  ji_Bereich2m(8) = 16
  ji_Bereich2m(9) = 7
  ji_Bereich2m(10) = 10
  ji_Bereich2m(11) = 9
  ji_Bereich2m(12) = 10
  Array_ji_Bereich2m = ji_Bereich2m()

  'Return ji_Bereich2m
End Function


Function Array_ni_Bereich2m()
  Dim ni_Bereich2m() As Double
  ReDim ni_Bereich2m(13)

  ni_Bereich2m(0) = -0.73362260186506 * 10 ^ -2
  ni_Bereich2m(1) = -0.88223831943146 * 10 ^ -1
  ni_Bereich2m(2) = -0.72334555213245 * 10 ^ -1
  ni_Bereich2m(3) = -0.40813178534455 * 10 ^ -2
  ni_Bereich2m(4) = 0.20097803380207 * 10 ^ -2
  ni_Bereich2m(5) = -0.53045921898642 * 10 ^ -1
  ni_Bereich2m(6) = -0.7619040908697 * 10 ^ -2
  ni_Bereich2m(7) = -0.63498037657313 * 10 ^ -2
  ni_Bereich2m(8) = -0.86043093028588 * 10 ^ -1
  ni_Bereich2m(9) = 0.7532158152277 * 10 ^ -2
  ni_Bereich2m(10) = -0.79238375446139 * 10 ^ -2
  ni_Bereich2m(11) = -0.22888160778447 * 10 ^ -3
  ni_Bereich2m(12) = -0.2645650148281 * 10 ^ -2
  Array_ni_Bereich2m = ni_Bereich2m()

  'Return ni_Bereich2m
End Function

'########################################## Bereich 3 ########################################
'#######################-Koeffizienten li- ji - ni - (Tabelle 30, S.30) -###########################################
Function Array_li_Bereich3()
  Dim li_Bereich3(40) As Integer
li_Bereich3(1)=0
li_Bereich3(2)=0
li_Bereich3(3)=0
li_Bereich3(4)=0
li_Bereich3(5)=0
li_Bereich3(6)=0
li_Bereich3(7)=0
li_Bereich3(8)=1
li_Bereich3(9)=1
li_Bereich3(10)=1
li_Bereich3(11)=1
li_Bereich3(12)=2
li_Bereich3(13)=2
li_Bereich3(14)=2
li_Bereich3(15)=2
li_Bereich3(16)=2
li_Bereich3(17)=2
li_Bereich3(18)=3
li_Bereich3(19)=3
li_Bereich3(20)=3
li_Bereich3(21)=3
li_Bereich3(22)=3
li_Bereich3(23)=4
li_Bereich3(24)=4
li_Bereich3(25)=4
li_Bereich3(26)=4
li_Bereich3(27)=5
li_Bereich3(28)=5
li_Bereich3(29)=5
li_Bereich3(30)=6
li_Bereich3(31)=6
li_Bereich3(32)=6
li_Bereich3(33)=7
li_Bereich3(34)=8
li_Bereich3(35)=9
li_Bereich3(36)=9
li_Bereich3(37)=10
li_Bereich3(38)=10
li_Bereich3(39)=11

	Array_li_Bereich3 = li_Bereich3
End Function


Function Array_ji_Bereich3()
  Dim ji_Bereich3() As Integer
  ReDim ji_Bereich3(40)

ji_Bereich3(1)=0
ji_Bereich3(2)=1
ji_Bereich3(3)=2
ji_Bereich3(4)=7
ji_Bereich3(5)=10
ji_Bereich3(6)=12
ji_Bereich3(7)=23
ji_Bereich3(8)=2
ji_Bereich3(9)=6
ji_Bereich3(10)=15
ji_Bereich3(11)=17
ji_Bereich3(12)=0
ji_Bereich3(13)=2
ji_Bereich3(14)=6
ji_Bereich3(15)=7
ji_Bereich3(16)=22
ji_Bereich3(17)=26
ji_Bereich3(18)=0
ji_Bereich3(19)=2
ji_Bereich3(20)=4
ji_Bereich3(21)=16
ji_Bereich3(22)=26
ji_Bereich3(23)=0
ji_Bereich3(24)=2
ji_Bereich3(25)=4
ji_Bereich3(26)=26
ji_Bereich3(27)=1
ji_Bereich3(28)=3
ji_Bereich3(29)=26
ji_Bereich3(30)=0
ji_Bereich3(31)=2
ji_Bereich3(32)=26
ji_Bereich3(33)=2
ji_Bereich3(34)=26
ji_Bereich3(35)=2
ji_Bereich3(36)=26
ji_Bereich3(37)=0
ji_Bereich3(38)=1
ji_Bereich3(39)=26

  Array_ji_Bereich3 = ji_Bereich3
End Function


Function Array_ni_Bereich3()
  Dim ni_Bereich3() As Double
  ReDim ni_Bereich3(40)
ni_Bereich3(0)=0.10658070028513E1
ni_Bereich3(1)=-0.15732845290239E2
ni_Bereich3(2)=0.20944396974307E2
ni_Bereich3(3)=-0.76867707878716E1
ni_Bereich3(4)=0.26185947787954E1
ni_Bereich3(5)=-0.28080781148620E1
ni_Bereich3(6)=0.12053369696517E1
ni_Bereich3(7)=-0.84566812812502E-2
ni_Bereich3(8)=-0.12654315477714E1
ni_Bereich3(9)=-0.11524407806681E1
ni_Bereich3(10)=0.88521043984318
ni_Bereich3(11)=-0.64207765181607
ni_Bereich3(12)=0.38493460186671
ni_Bereich3(13)=-0.85214708824206
ni_Bereich3(14)=0.48972281541877E1
ni_Bereich3(15)=-0.30502617256965E1
ni_Bereich3(16)=0.39420536879154E-1
ni_Bereich3(17)=0.12558408424308
ni_Bereich3(18)=-0.27999329698710
ni_Bereich3(19)=0.13899799569460E1
ni_Bereich3(20)=-0.20189915023570E1
ni_Bereich3(21)=-0.82147637173963E-2
ni_Bereich3(22)=-0.47596035734923
ni_Bereich3(23)=0.43984074473500E-1
ni_Bereich3(24)=-0.44476435428739
ni_Bereich3(25)=0.90572070719733
ni_Bereich3(26)=0.70522450087967
ni_Bereich3(27)=0.10770512626332
ni_Bereich3(28)=-0.32913623258954
ni_Bereich3(29)=-0.50871062041158
ni_Bereich3(30)=-0.22175400873096E-1
ni_Bereich3(31)=0.94260751665092E-1
ni_Bereich3(32)=0.16436278447961
ni_Bereich3(33)=-0.13503372241348E-1
ni_Bereich3(34)=-0.14834345352472E-1
ni_Bereich3(35)=0.57922953628084E-3
ni_Bereich3(36)=0.32308904703711E-2
ni_Bereich3(37)=0.80964802996215E-4
ni_Bereich3(38)=-0.16557679795037E-3
ni_Bereich3(39)=-0.44923899061815E-4

  Array_ni_Bereich3 = ni_Bereich3
End Function

'#######################-Umkehrfunktionen-NEU-############################################################################
'#######################-Koeffizienten li - ji - ni (Tabelle 6, S.10) - für Bereich 1 Umkehrfunktionen-###################

Function Array_li_Bereich1BW_TPH()
  Dim li_Bereich1BW_TPH() As Integer
  ReDim li_Bereich1BW_TPH(20)

  li_Bereich1BW_TPH(0) = 0
  li_Bereich1BW_TPH(1) = 0
  li_Bereich1BW_TPH(2) = 0
  li_Bereich1BW_TPH(3) = 0
  li_Bereich1BW_TPH(4) = 0
  li_Bereich1BW_TPH(5) = 0
  li_Bereich1BW_TPH(6) = 1
  li_Bereich1BW_TPH(7) = 1
  li_Bereich1BW_TPH(8) = 1
  li_Bereich1BW_TPH(9) = 1
  li_Bereich1BW_TPH(10) = 1
  li_Bereich1BW_TPH(11) = 1
  li_Bereich1BW_TPH(12) = 1
  li_Bereich1BW_TPH(13) = 2
  li_Bereich1BW_TPH(14) = 2
  li_Bereich1BW_TPH(15) = 3
  li_Bereich1BW_TPH(16) = 3
  li_Bereich1BW_TPH(17) = 4
  li_Bereich1BW_TPH(18) = 5
  li_Bereich1BW_TPH(19) = 6
  Array_li_Bereich1BW_TPH = li_Bereich1BW_TPH

  'Return li_Bereich1BW_TPH
End Function


Function Array_ji_Bereich1BW_TPH()
  Dim ji_Bereich1BW_TPH() As Integer
  ReDim ji_Bereich1BW_TPH(20)

  ji_Bereich1BW_TPH(0) = 0
  ji_Bereich1BW_TPH(1) = 1
  ji_Bereich1BW_TPH(2) = 2
  ji_Bereich1BW_TPH(3) = 6
  ji_Bereich1BW_TPH(4) = 22
  ji_Bereich1BW_TPH(5) = 32
  ji_Bereich1BW_TPH(6) = 0
  ji_Bereich1BW_TPH(7) = 1
  ji_Bereich1BW_TPH(8) = 2
  ji_Bereich1BW_TPH(9) = 3
  ji_Bereich1BW_TPH(10) = 4
  ji_Bereich1BW_TPH(11) = 10
  ji_Bereich1BW_TPH(12) = 32
  ji_Bereich1BW_TPH(13) = 10
  ji_Bereich1BW_TPH(14) = 32
  ji_Bereich1BW_TPH(15) = 10
  ji_Bereich1BW_TPH(16) = 32
  ji_Bereich1BW_TPH(17) = 32
  ji_Bereich1BW_TPH(18) = 32
  ji_Bereich1BW_TPH(19) = 32
  Array_ji_Bereich1BW_TPH = ji_Bereich1BW_TPH

  'Return ji_Bereich1BW_TPH
End Function


Function Array_ni_Bereich1BW_TPH()
  Dim ni_Bereich1BW_TPH() As Double
  ReDim ni_Bereich1BW_TPH(20)

  ni_Bereich1BW_TPH(0) = -0.23872489924521 * 10 ^ 3
  ni_Bereich1BW_TPH(1) = 0.40421188637945 * 10 ^ 3
  ni_Bereich1BW_TPH(2) = 0.11349746881718 * 10 ^ 3
  ni_Bereich1BW_TPH(3) = -0.58457616048039 * 10 ^ 1
  ni_Bereich1BW_TPH(4) = -0.1528548241314 * 10 ^ -3
  ni_Bereich1BW_TPH(5) = -0.10866707695377 * 10 ^ -5
  ni_Bereich1BW_TPH(6) = -0.13391744872602 * 10 ^ 2
  ni_Bereich1BW_TPH(7) = 0.43211039183559 * 10 ^ 2
  ni_Bereich1BW_TPH(8) = -0.54010067170506 * 10 ^ 2
  ni_Bereich1BW_TPH(9) = 0.30535892203916 * 10 ^ 2
  ni_Bereich1BW_TPH(10) = -0.65964749423638 * 10 ^ 1
  ni_Bereich1BW_TPH(11) = 0.93965400878363 * 10 ^ -2
  ni_Bereich1BW_TPH(12) = 0.1157364750534 * 10 ^ -6
  ni_Bereich1BW_TPH(13) = -0.25858641282073 * 10 ^ -4
  ni_Bereich1BW_TPH(14) = -0.40644363084799 * 10 ^ -8
  ni_Bereich1BW_TPH(15) = 0.66456186191635 * 10 ^ -7
  ni_Bereich1BW_TPH(16) = 0.80670734103027 * 10 ^ -10
  ni_Bereich1BW_TPH(17) = -0.93477771213947 * 10 ^ -12
  ni_Bereich1BW_TPH(18) = 0.58265442020601 * 10 ^ -14
  ni_Bereich1BW_TPH(19) = -0.15020185953503 * 10 ^ -16
  Array_ni_Bereich1BW_TPH = ni_Bereich1BW_TPH

  'Return ni_Bereich1BW_TPH
End Function

'#######################-Umkehrfunktionen-NEU-############################################################################
'#######################-Koeffizienten li - ji - ni (Tabelle 8, S.11) - für Bereich 1 Umkehrfunktionen-###################

Function Array_li_Bereich1BW_TPS()
  Dim li_Bereich1BW_TPS() As Integer
  ReDim li_Bereich1BW_TPS(20)

  li_Bereich1BW_TPS(0) = 0
  li_Bereich1BW_TPS(1) = 0
  li_Bereich1BW_TPS(2) = 0
  li_Bereich1BW_TPS(3) = 0
  li_Bereich1BW_TPS(4) = 0
  li_Bereich1BW_TPS(5) = 0
  li_Bereich1BW_TPS(6) = 1
  li_Bereich1BW_TPS(7) = 1
  li_Bereich1BW_TPS(8) = 1
  li_Bereich1BW_TPS(9) = 1
  li_Bereich1BW_TPS(10) = 1
  li_Bereich1BW_TPS(11) = 1
  li_Bereich1BW_TPS(12) = 2
  li_Bereich1BW_TPS(13) = 2
  li_Bereich1BW_TPS(14) = 2
  li_Bereich1BW_TPS(15) = 2
  li_Bereich1BW_TPS(16) = 2
  li_Bereich1BW_TPS(17) = 3
  li_Bereich1BW_TPS(18) = 3
  li_Bereich1BW_TPS(19) = 4
  Array_li_Bereich1BW_TPS = li_Bereich1BW_TPS

  'Return li_Bereich1BW_TPS
End Function


Function Array_ji_Bereich1BW_TPS()
  Dim ji_Bereich1BW_TPS() As Integer
  ReDim ji_Bereich1BW_TPS(20)

  ji_Bereich1BW_TPS(0) = 0
  ji_Bereich1BW_TPS(1) = 1
  ji_Bereich1BW_TPS(2) = 2
  ji_Bereich1BW_TPS(3) = 3
  ji_Bereich1BW_TPS(4) = 11
  ji_Bereich1BW_TPS(5) = 31
  ji_Bereich1BW_TPS(6) = 0
  ji_Bereich1BW_TPS(7) = 1
  ji_Bereich1BW_TPS(8) = 2
  ji_Bereich1BW_TPS(9) = 3
  ji_Bereich1BW_TPS(10) = 12
  ji_Bereich1BW_TPS(11) = 31
  ji_Bereich1BW_TPS(12) = 0
  ji_Bereich1BW_TPS(13) = 1
  ji_Bereich1BW_TPS(14) = 2
  ji_Bereich1BW_TPS(15) = 9
  ji_Bereich1BW_TPS(16) = 31
  ji_Bereich1BW_TPS(17) = 10
  ji_Bereich1BW_TPS(18) = 32
  ji_Bereich1BW_TPS(19) = 32
  Array_ji_Bereich1BW_TPS = ji_Bereich1BW_TPS

  'Return ji_Bereich1BW_TPS
End Function


Function Array_ni_Bereich1BW_TPS()
  Dim ni_Bereich1BW_TPS() As Double
  ReDim ni_Bereich1BW_TPS(20)

  ni_Bereich1BW_TPS(0) = 0.17478268058307 * 10 ^ 3
  ni_Bereich1BW_TPS(1) = 0.34806930892873 * 10 ^ 2
  ni_Bereich1BW_TPS(2) = 0.65292584978455 * 10 ^ 1
  ni_Bereich1BW_TPS(3) = 0.33039981775489
  ni_Bereich1BW_TPS(4) = -0.19281382923196 * 10 ^ -6
  ni_Bereich1BW_TPS(5) = -0.24909197244573 * 10 ^ -22
  ni_Bereich1BW_TPS(6) = -0.26107636489332
  ni_Bereich1BW_TPS(7) = 0.22592965981586
  ni_Bereich1BW_TPS(8) = -0.64256463395226 * 10 ^ -1
  ni_Bereich1BW_TPS(9) = 0.78876289270526 * 10 ^ -2
  ni_Bereich1BW_TPS(10) = 0.35672110607366 * 10 ^ -9
  ni_Bereich1BW_TPS(11) = 0.17332496994891 * 10 ^ -23
  ni_Bereich1BW_TPS(12) = 0.56608900654837 * 10 ^ -3
  ni_Bereich1BW_TPS(13) = -0.32635483139717 * 10 ^ -3
  ni_Bereich1BW_TPS(14) = 0.44778286690632 * 10 ^ -4
  ni_Bereich1BW_TPS(15) = -0.51322156908507 * 10 ^ -9
  ni_Bereich1BW_TPS(16) = -0.42522657042207 * 10 ^ -25
  ni_Bereich1BW_TPS(17) = 0.26400441360689 * 10 ^ -12
  ni_Bereich1BW_TPS(18) = 0.78124600459723 * 10 ^ -28
  ni_Bereich1BW_TPS(19) = -0.30732199903668 * 10 ^ -30
  Array_ni_Bereich1BW_TPS = ni_Bereich1BW_TPS

  'Return ni_Bereich1BW_TPS
End Function

'#######################-Umkehrfunktionen-NEU-############################################################################
'#######################-Koeffizienten li - ji - ni (Tabelle 20, S.22) - für Bereich 2a Umkehrfunktionen-#################

Function Array_li_Bereich2BW_TPHa()
  Dim li_Bereich2BW_TPHa() As Integer
  ReDim li_Bereich2BW_TPHa(34)

  li_Bereich2BW_TPHa(0) = 0
  li_Bereich2BW_TPHa(1) = 0
  li_Bereich2BW_TPHa(2) = 0
  li_Bereich2BW_TPHa(3) = 0
  li_Bereich2BW_TPHa(4) = 0
  li_Bereich2BW_TPHa(5) = 0
  li_Bereich2BW_TPHa(6) = 1
  li_Bereich2BW_TPHa(7) = 1
  li_Bereich2BW_TPHa(8) = 1
  li_Bereich2BW_TPHa(9) = 1
  li_Bereich2BW_TPHa(10) = 1
  li_Bereich2BW_TPHa(11) = 1
  li_Bereich2BW_TPHa(12) = 1
  li_Bereich2BW_TPHa(13) = 1
  li_Bereich2BW_TPHa(14) = 1
  li_Bereich2BW_TPHa(15) = 2
  li_Bereich2BW_TPHa(16) = 2
  li_Bereich2BW_TPHa(17) = 2
  li_Bereich2BW_TPHa(18) = 2
  li_Bereich2BW_TPHa(19) = 2
  li_Bereich2BW_TPHa(20) = 2
  li_Bereich2BW_TPHa(21) = 2
  li_Bereich2BW_TPHa(22) = 2
  li_Bereich2BW_TPHa(23) = 3
  li_Bereich2BW_TPHa(24) = 3
  li_Bereich2BW_TPHa(25) = 4
  li_Bereich2BW_TPHa(26) = 4
  li_Bereich2BW_TPHa(27) = 4
  li_Bereich2BW_TPHa(28) = 5
  li_Bereich2BW_TPHa(29) = 5
  li_Bereich2BW_TPHa(30) = 5
  li_Bereich2BW_TPHa(31) = 6
  li_Bereich2BW_TPHa(32) = 6
  li_Bereich2BW_TPHa(33) = 7
  Array_li_Bereich2BW_TPHa = li_Bereich2BW_TPHa

  'Return li_Bereich2BW_TPHa
End Function


Function Array_ji_Bereich2BW_TPHa()
  Dim ji_Bereich2BW_TPHa() As Integer
  ReDim ji_Bereich2BW_TPHa(34)

  ji_Bereich2BW_TPHa(0) = 0
  ji_Bereich2BW_TPHa(1) = 1
  ji_Bereich2BW_TPHa(2) = 2
  ji_Bereich2BW_TPHa(3) = 3
  ji_Bereich2BW_TPHa(4) = 7
  ji_Bereich2BW_TPHa(5) = 20
  ji_Bereich2BW_TPHa(6) = 0
  ji_Bereich2BW_TPHa(7) = 1
  ji_Bereich2BW_TPHa(8) = 2
  ji_Bereich2BW_TPHa(9) = 3
  ji_Bereich2BW_TPHa(10) = 7
  ji_Bereich2BW_TPHa(11) = 9
  ji_Bereich2BW_TPHa(12) = 11
  ji_Bereich2BW_TPHa(13) = 18
  ji_Bereich2BW_TPHa(14) = 44
  ji_Bereich2BW_TPHa(15) = 0
  ji_Bereich2BW_TPHa(16) = 2
  ji_Bereich2BW_TPHa(17) = 7
  ji_Bereich2BW_TPHa(18) = 36
  ji_Bereich2BW_TPHa(19) = 38
  ji_Bereich2BW_TPHa(20) = 40
  ji_Bereich2BW_TPHa(21) = 42
  ji_Bereich2BW_TPHa(22) = 44
  ji_Bereich2BW_TPHa(23) = 24
  ji_Bereich2BW_TPHa(24) = 44
  ji_Bereich2BW_TPHa(25) = 12
  ji_Bereich2BW_TPHa(26) = 32
  ji_Bereich2BW_TPHa(27) = 44
  ji_Bereich2BW_TPHa(28) = 32
  ji_Bereich2BW_TPHa(29) = 36
  ji_Bereich2BW_TPHa(30) = 42
  ji_Bereich2BW_TPHa(31) = 34
  ji_Bereich2BW_TPHa(32) = 44
  ji_Bereich2BW_TPHa(33) = 28
  Array_ji_Bereich2BW_TPHa = ji_Bereich2BW_TPHa

  'Return ji_Bereich2BW_TPHa
End Function


Function Array_ni_Bereich2BW_TPHa()
  Dim ni_Bereich2BW_TPHa() As Double
  ReDim ni_Bereich2BW_TPHa(34)

  ni_Bereich2BW_TPHa(0) = 0.10898952318288 * 10 ^ 4
  ni_Bereich2BW_TPHa(1) = 0.84951654495535 * 10 ^ 3
  ni_Bereich2BW_TPHa(2) = -0.10781748091826 * 10 ^ 3
  ni_Bereich2BW_TPHa(3) = 0.33153654801263 * 10 ^ 2
  ni_Bereich2BW_TPHa(4) = -0.74232016790248 * 10 ^ 1
  ni_Bereich2BW_TPHa(5) = 0.11765048724356 * 10 ^ 2
  ni_Bereich2BW_TPHa(6) = 0.1844574935579 * 10 ^ 1
  ni_Bereich2BW_TPHa(7) = -0.41792700149624 * 10 ^ 1
  ni_Bereich2BW_TPHa(8) = 0.62478196935812 * 10 ^ 1
  ni_Bereich2BW_TPHa(9) = -0.17344563108114 * 10 ^ 2
  ni_Bereich2BW_TPHa(10) = -0.20058176862096 * 10 ^ 3
  ni_Bereich2BW_TPHa(11) = 0.27196065473796 * 10 ^ 3
  ni_Bereich2BW_TPHa(12) = -0.45511318285818 * 10 ^ 3
  ni_Bereich2BW_TPHa(13) = 0.30919688604755 * 10 ^ 4
  ni_Bereich2BW_TPHa(14) = 0.25226640357872 * 10 ^ 6
  ni_Bereich2BW_TPHa(15) = -0.61707422868339 * 10 ^ -2
  ni_Bereich2BW_TPHa(16) = -0.3107804662983
  ni_Bereich2BW_TPHa(17) = 0.11670873077107 * 10 ^ 2
  ni_Bereich2BW_TPHa(18) = 0.12812798404046 * 10 ^ 9
  ni_Bereich2BW_TPHa(19) = -0.98554909623276 * 10 ^ 9
  ni_Bereich2BW_TPHa(20) = 0.28224546973002 * 10 ^ 10
  ni_Bereich2BW_TPHa(21) = -0.35948971410703 * 10 ^ 10
  ni_Bereich2BW_TPHa(22) = 0.17227349913197 * 10 ^ 10
  ni_Bereich2BW_TPHa(23) = -0.13551334240775 * 10 ^ 5
  ni_Bereich2BW_TPHa(24) = 0.1284873466465 * 10 ^ 8
  ni_Bereich2BW_TPHa(25) = 0.13865724283226 * 10 ^ 1
  ni_Bereich2BW_TPHa(26) = 0.23598832556514 * 10 ^ 6
  ni_Bereich2BW_TPHa(27) = -0.13105236545054 * 10 ^ 8
  ni_Bereich2BW_TPHa(28) = 0.73999835474766 * 10 ^ 4
  ni_Bereich2BW_TPHa(29) = -0.5519669703006 * 10 ^ 6
  ni_Bereich2BW_TPHa(30) = 0.37154085996233 * 10 ^ 7
  ni_Bereich2BW_TPHa(31) = 0.1912772923966 * 10 ^ 5
  ni_Bereich2BW_TPHa(32) = -0.1912772923966 * 10 ^ 6
  ni_Bereich2BW_TPHa(33) = -0.62459855192507 * 10 ^ 2
  Array_ni_Bereich2BW_TPHa = ni_Bereich2BW_TPHa

  'Return ni_Bereich2BW_TPHa
End Function

'#######################-Umkehrfunktionen-NEU-############################################################################
'#######################-Koeffizienten li - ji - ni (Tabelle 21, S.23) - für Bereich 2b Umkehrfunktionen-#################


Function Array_li_Bereich2BW_TPHb()
  Dim li_Bereich2BW_TPHb() As Integer
  ReDim li_Bereich2BW_TPHb(38)

  li_Bereich2BW_TPHb(0) = 0
  li_Bereich2BW_TPHb(1) = 0
  li_Bereich2BW_TPHb(2) = 0
  li_Bereich2BW_TPHb(3) = 0
  li_Bereich2BW_TPHb(4) = 0
  li_Bereich2BW_TPHb(5) = 0
  li_Bereich2BW_TPHb(6) = 0
  li_Bereich2BW_TPHb(7) = 0
  li_Bereich2BW_TPHb(8) = 1
  li_Bereich2BW_TPHb(9) = 1
  li_Bereich2BW_TPHb(10) = 1
  li_Bereich2BW_TPHb(11) = 1
  li_Bereich2BW_TPHb(12) = 1
  li_Bereich2BW_TPHb(13) = 1
  li_Bereich2BW_TPHb(14) = 1
  li_Bereich2BW_TPHb(15) = 1
  li_Bereich2BW_TPHb(16) = 2
  li_Bereich2BW_TPHb(17) = 2
  li_Bereich2BW_TPHb(18) = 2
  li_Bereich2BW_TPHb(19) = 2
  li_Bereich2BW_TPHb(20) = 3
  li_Bereich2BW_TPHb(21) = 3
  li_Bereich2BW_TPHb(22) = 3
  li_Bereich2BW_TPHb(23) = 3
  li_Bereich2BW_TPHb(24) = 4
  li_Bereich2BW_TPHb(25) = 4
  li_Bereich2BW_TPHb(26) = 4
  li_Bereich2BW_TPHb(27) = 4
  li_Bereich2BW_TPHb(28) = 4
  li_Bereich2BW_TPHb(29) = 4
  li_Bereich2BW_TPHb(30) = 5
  li_Bereich2BW_TPHb(31) = 5
  li_Bereich2BW_TPHb(32) = 5
  li_Bereich2BW_TPHb(33) = 6
  li_Bereich2BW_TPHb(34) = 7
  li_Bereich2BW_TPHb(35) = 7
  li_Bereich2BW_TPHb(36) = 9
  li_Bereich2BW_TPHb(37) = 9
  Array_li_Bereich2BW_TPHb = li_Bereich2BW_TPHb

  'Return li_Bereich2BW_TPHb
End Function


Function Array_ji_Bereich2BW_TPHb()
  Dim ji_Bereich2BW_TPHb() As Integer
  ReDim ji_Bereich2BW_TPHb(38)

  ji_Bereich2BW_TPHb(0) = 0
  ji_Bereich2BW_TPHb(1) = 1
  ji_Bereich2BW_TPHb(2) = 2
  ji_Bereich2BW_TPHb(3) = 12
  ji_Bereich2BW_TPHb(4) = 18
  ji_Bereich2BW_TPHb(5) = 24
  ji_Bereich2BW_TPHb(6) = 28
  ji_Bereich2BW_TPHb(7) = 40
  ji_Bereich2BW_TPHb(8) = 0
  ji_Bereich2BW_TPHb(9) = 2
  ji_Bereich2BW_TPHb(10) = 6
  ji_Bereich2BW_TPHb(11) = 12
  ji_Bereich2BW_TPHb(12) = 18
  ji_Bereich2BW_TPHb(13) = 24
  ji_Bereich2BW_TPHb(14) = 28
  ji_Bereich2BW_TPHb(15) = 40
  ji_Bereich2BW_TPHb(16) = 2
  ji_Bereich2BW_TPHb(17) = 8
  ji_Bereich2BW_TPHb(18) = 18
  ji_Bereich2BW_TPHb(19) = 40
  ji_Bereich2BW_TPHb(20) = 1
  ji_Bereich2BW_TPHb(21) = 2
  ji_Bereich2BW_TPHb(22) = 12
  ji_Bereich2BW_TPHb(23) = 24
  ji_Bereich2BW_TPHb(24) = 2
  ji_Bereich2BW_TPHb(25) = 12
  ji_Bereich2BW_TPHb(26) = 18
  ji_Bereich2BW_TPHb(27) = 24
  ji_Bereich2BW_TPHb(28) = 28
  ji_Bereich2BW_TPHb(29) = 40
  ji_Bereich2BW_TPHb(30) = 18
  ji_Bereich2BW_TPHb(31) = 24
  ji_Bereich2BW_TPHb(32) = 40
  ji_Bereich2BW_TPHb(33) = 28
  ji_Bereich2BW_TPHb(34) = 2
  ji_Bereich2BW_TPHb(35) = 28
  ji_Bereich2BW_TPHb(36) = 1
  ji_Bereich2BW_TPHb(37) = 40
  Array_ji_Bereich2BW_TPHb = ji_Bereich2BW_TPHb
  'Return ji_Bereich2BW_TPHb
End Function


Function Array_ni_Bereich2BW_TPHb()
  Dim ni_Bereich2BW_TPHb() As Double
  ReDim ni_Bereich2BW_TPHb(38)

  ni_Bereich2BW_TPHb(0) = 0.14895041079516 * 10 ^ 4
  ni_Bereich2BW_TPHb(1) = 0.74307798314034 * 10 ^ 3
  ni_Bereich2BW_TPHb(2) = -0.97708318797837 * 10 ^ 2
  ni_Bereich2BW_TPHb(3) = 0.24742464705674 * 10 ^ 1
  ni_Bereich2BW_TPHb(4) = -0.63281320016026
  ni_Bereich2BW_TPHb(5) = 0.11385952129658 * 10 ^ 1
  ni_Bereich2BW_TPHb(6) = -0.47811863648625
  ni_Bereich2BW_TPHb(7) = 0.85208123431544 * 10 ^ -2
  ni_Bereich2BW_TPHb(8) = 0.93747147377932
  ni_Bereich2BW_TPHb(9) = 0.33593118604916 * 10 ^ 1
  ni_Bereich2BW_TPHb(10) = 0.33809355601454 * 10 ^ 1
  ni_Bereich2BW_TPHb(11) = 0.16844539671904
  ni_Bereich2BW_TPHb(12) = 0.73875745236695
  ni_Bereich2BW_TPHb(13) = -0.47128737436186
  ni_Bereich2BW_TPHb(14) = 0.15020273139707
  ni_Bereich2BW_TPHb(15) = -0.2176411421975 * 10 ^ -2
  ni_Bereich2BW_TPHb(16) = -0.21810755324761 * 10 ^ -1
  ni_Bereich2BW_TPHb(17) = -0.10829784403677
  ni_Bereich2BW_TPHb(18) = -0.46333324635812 * 10 ^ -1
  ni_Bereich2BW_TPHb(19) = 0.71280351959551 * 10 ^ -4
  ni_Bereich2BW_TPHb(20) = 0.11032831789999 * 10 ^ -3
  ni_Bereich2BW_TPHb(21) = 0.18955248387902 * 10 ^ -3
  ni_Bereich2BW_TPHb(22) = 0.30891541160537 * 10 ^ -2
  ni_Bereich2BW_TPHb(23) = 0.13555504554949 * 10 ^ -2
  ni_Bereich2BW_TPHb(24) = 0.28640237477456 * 10 ^ -6
  ni_Bereich2BW_TPHb(25) = -0.10779857357512 * 10 ^ -4
  ni_Bereich2BW_TPHb(26) = -0.76462712454814 * 10 ^ -4
  ni_Bereich2BW_TPHb(27) = 0.14052392818316 * 10 ^ -4
  ni_Bereich2BW_TPHb(28) = -0.31083814331434 * 10 ^ -4
  ni_Bereich2BW_TPHb(29) = -0.10302738212103 * 10 ^ -5
  ni_Bereich2BW_TPHb(30) = 0.2821728163504 * 10 ^ -6
  ni_Bereich2BW_TPHb(31) = 0.12704902271945 * 10 ^ -5
  ni_Bereich2BW_TPHb(32) = 0.73803353468292 * 10 ^ -7
  ni_Bereich2BW_TPHb(33) = -0.11030139238909 * 10 ^ -7
  ni_Bereich2BW_TPHb(34) = -0.81456365207833 * 10 ^ -13
  ni_Bereich2BW_TPHb(35) = -0.25180545682962 * 10 ^ -10
  ni_Bereich2BW_TPHb(36) = -0.17565233969407 * 10 ^ -17
  ni_Bereich2BW_TPHb(37) = 0.86934156344163 * 10 ^ -14
  Array_ni_Bereich2BW_TPHb = ni_Bereich2BW_TPHb
  'Return ni_Bereich2BW_TPHb
End Function

'#######################-Umkehrfunktionen-NEU-############################################################################
'#######################-Koeffizienten li - ji - ni (Tabelle 22, S.24) - für Bereich 2c Umkehrfunktionen-#################


Function Array_li_Bereich2BW_TPHc()
  Dim li_Bereich2BW_TPHc() As Integer
  ReDim li_Bereich2BW_TPHc(23)

  li_Bereich2BW_TPHc(0) = -7
  li_Bereich2BW_TPHc(1) = -7
  li_Bereich2BW_TPHc(2) = -6
  li_Bereich2BW_TPHc(3) = -6
  li_Bereich2BW_TPHc(4) = -5
  li_Bereich2BW_TPHc(5) = -5
  li_Bereich2BW_TPHc(6) = -2
  li_Bereich2BW_TPHc(7) = -2
  li_Bereich2BW_TPHc(8) = -1
  li_Bereich2BW_TPHc(9) = -1
  li_Bereich2BW_TPHc(10) = 0
  li_Bereich2BW_TPHc(11) = 0
  li_Bereich2BW_TPHc(12) = 1
  li_Bereich2BW_TPHc(13) = 1
  li_Bereich2BW_TPHc(14) = 2
  li_Bereich2BW_TPHc(15) = 6
  li_Bereich2BW_TPHc(16) = 6
  li_Bereich2BW_TPHc(17) = 6
  li_Bereich2BW_TPHc(18) = 6
  li_Bereich2BW_TPHc(19) = 6
  li_Bereich2BW_TPHc(20) = 6
  li_Bereich2BW_TPHc(21) = 6
  li_Bereich2BW_TPHc(22) = 6
  Array_li_Bereich2BW_TPHc = li_Bereich2BW_TPHc
  'Return li_Bereich2BW_TPHc
End Function


Function Array_ji_Bereich2BW_TPHc()
  Dim ji_Bereich2BW_TPHc() As Integer
  ReDim ji_Bereich2BW_TPHc(23)

  ji_Bereich2BW_TPHc(0) = 0
  ji_Bereich2BW_TPHc(1) = 4
  ji_Bereich2BW_TPHc(2) = 0
  ji_Bereich2BW_TPHc(3) = 2
  ji_Bereich2BW_TPHc(4) = 0
  ji_Bereich2BW_TPHc(5) = 2
  ji_Bereich2BW_TPHc(6) = 0
  ji_Bereich2BW_TPHc(7) = 1
  ji_Bereich2BW_TPHc(8) = 0
  ji_Bereich2BW_TPHc(9) = 2
  ji_Bereich2BW_TPHc(10) = 0
  ji_Bereich2BW_TPHc(11) = 1
  ji_Bereich2BW_TPHc(12) = 4
  ji_Bereich2BW_TPHc(13) = 8
  ji_Bereich2BW_TPHc(14) = 4
  ji_Bereich2BW_TPHc(15) = 0
  ji_Bereich2BW_TPHc(16) = 1
  ji_Bereich2BW_TPHc(17) = 4
  ji_Bereich2BW_TPHc(18) = 10
  ji_Bereich2BW_TPHc(19) = 12
  ji_Bereich2BW_TPHc(20) = 16
  ji_Bereich2BW_TPHc(21) = 20
  ji_Bereich2BW_TPHc(22) = 22
  Array_ji_Bereich2BW_TPHc = ji_Bereich2BW_TPHc

  'Return ji_Bereich2BW_TPHc
End Function


Function Array_ni_Bereich2BW_TPHc()
  Dim ni_Bereich2BW_TPHc() As Double
  ReDim ni_Bereich2BW_TPHc(23)

  ni_Bereich2BW_TPHc(0) = -0.32368398555242 * 10 ^ 13
  ni_Bereich2BW_TPHc(1) = 0.73263350902181 * 10 ^ 13
  ni_Bereich2BW_TPHc(2) = 0.35825089945447 * 10 ^ 12
  ni_Bereich2BW_TPHc(3) = -0.5834013185159 * 10 ^ 12
  ni_Bereich2BW_TPHc(4) = -0.1078306821747 * 10 ^ 11
  ni_Bereich2BW_TPHc(5) = 0.20825544563171 * 10 ^ 11
  ni_Bereich2BW_TPHc(6) = 0.61074783564516 * 10 ^ 6
  ni_Bereich2BW_TPHc(7) = 0.8597772253558 * 10 ^ 6
  ni_Bereich2BW_TPHc(8) = -0.2574572360417 * 10 ^ 5
  ni_Bereich2BW_TPHc(9) = 0.31081088422714 * 10 ^ 5
  ni_Bereich2BW_TPHc(10) = 0.12082315865936 * 10 ^ 4
  ni_Bereich2BW_TPHc(11) = 0.48219755109255 * 10 ^ 3
  ni_Bereich2BW_TPHc(12) = 0.37966001272486 * 10 ^ 1
  ni_Bereich2BW_TPHc(13) = -0.10842984880077 * 10 ^ 2
  ni_Bereich2BW_TPHc(14) = -0.4536417267666 * 10 ^ -1
  ni_Bereich2BW_TPHc(15) = 0.14559115658698 * 10 ^ -12
  ni_Bereich2BW_TPHc(16) = 0.1126159740723 * 10 ^ -11
  ni_Bereich2BW_TPHc(17) = -0.17804982240686 * 10 ^ -10
  ni_Bereich2BW_TPHc(18) = 0.12324579690832 * 10 ^ -6
  ni_Bereich2BW_TPHc(19) = -0.11606921130984 * 10 ^ -5
  ni_Bereich2BW_TPHc(20) = 0.27846367088554 * 10 ^ -4
  ni_Bereich2BW_TPHc(21) = -0.59270038474176 * 10 ^ -3
  ni_Bereich2BW_TPHc(22) = 0.12918582991878 * 10 ^ -2
  Array_ni_Bereich2BW_TPHc = ni_Bereich2BW_TPHc

  'Return ni_Bereich2BW_TPHc
End Function

'#######################-Umkehrfunktionen-NEU-############################################################################
'#######################-Koeffizienten li - ji - ni (Tabelle 25, S.26) - für Bereich 2a Umkehrfunktionen-#################


Function Array_li_Bereich2BW_TPSa()
  Dim li_Bereich2BW_TPSa() As Double
  ReDim li_Bereich2BW_TPSa(46)

  li_Bereich2BW_TPSa(0) = -1.5
  li_Bereich2BW_TPSa(1) = -1.5
  li_Bereich2BW_TPSa(2) = -1.5
  li_Bereich2BW_TPSa(3) = -1.5
  li_Bereich2BW_TPSa(4) = -1.5
  li_Bereich2BW_TPSa(5) = -1.5
  li_Bereich2BW_TPSa(6) = -1.25
  li_Bereich2BW_TPSa(7) = -1.25
  li_Bereich2BW_TPSa(8) = -1.25
  li_Bereich2BW_TPSa(9) = -1#
  li_Bereich2BW_TPSa(10) = -1#
  li_Bereich2BW_TPSa(11) = -1#
  li_Bereich2BW_TPSa(12) = -1#
  li_Bereich2BW_TPSa(13) = -1#
  li_Bereich2BW_TPSa(14) = -1#
  li_Bereich2BW_TPSa(15) = -0.75
  li_Bereich2BW_TPSa(16) = -0.75
  li_Bereich2BW_TPSa(17) = -0.5
  li_Bereich2BW_TPSa(18) = -0.5
  li_Bereich2BW_TPSa(19) = -0.5
  li_Bereich2BW_TPSa(20) = -0.5
  li_Bereich2BW_TPSa(21) = -0.25
  li_Bereich2BW_TPSa(22) = -0.25
  li_Bereich2BW_TPSa(23) = -0.25
  li_Bereich2BW_TPSa(24) = -0.25
  li_Bereich2BW_TPSa(25) = 0.25
  li_Bereich2BW_TPSa(26) = 0.25
  li_Bereich2BW_TPSa(27) = 0.25
  li_Bereich2BW_TPSa(28) = 0.25
  li_Bereich2BW_TPSa(29) = 0.5
  li_Bereich2BW_TPSa(30) = 0.5
  li_Bereich2BW_TPSa(31) = 0.5
  li_Bereich2BW_TPSa(32) = 0.5
  li_Bereich2BW_TPSa(33) = 0.5
  li_Bereich2BW_TPSa(34) = 0.5
  li_Bereich2BW_TPSa(35) = 0.5
  li_Bereich2BW_TPSa(36) = 0.75
  li_Bereich2BW_TPSa(37) = 0.75
  li_Bereich2BW_TPSa(38) = 0.75
  li_Bereich2BW_TPSa(39) = 0.75
  li_Bereich2BW_TPSa(40) = 1#
  li_Bereich2BW_TPSa(41) = 1#
  li_Bereich2BW_TPSa(42) = 1.25
  li_Bereich2BW_TPSa(43) = 1.25
  li_Bereich2BW_TPSa(44) = 1.5
  li_Bereich2BW_TPSa(45) = 1.5
  Array_li_Bereich2BW_TPSa = li_Bereich2BW_TPSa

  'Return li_Bereich2BW_TPSa
End Function


Function Array_ji_Bereich2BW_TPSa()
  Dim ji_Bereich2BW_TPSa() As Integer
  ReDim ji_Bereich2BW_TPSa(46)

  ji_Bereich2BW_TPSa(0) = -24
  ji_Bereich2BW_TPSa(1) = -23
  ji_Bereich2BW_TPSa(2) = -19
  ji_Bereich2BW_TPSa(3) = -13
  ji_Bereich2BW_TPSa(4) = -11
  ji_Bereich2BW_TPSa(5) = -10
  ji_Bereich2BW_TPSa(6) = -19
  ji_Bereich2BW_TPSa(7) = -15
  ji_Bereich2BW_TPSa(8) = -6
  ji_Bereich2BW_TPSa(9) = -26
  ji_Bereich2BW_TPSa(10) = -21
  ji_Bereich2BW_TPSa(11) = -17
  ji_Bereich2BW_TPSa(12) = -16
  ji_Bereich2BW_TPSa(13) = -9
  ji_Bereich2BW_TPSa(14) = -8
  ji_Bereich2BW_TPSa(15) = -15
  ji_Bereich2BW_TPSa(16) = -14
  ji_Bereich2BW_TPSa(17) = -26
  ji_Bereich2BW_TPSa(18) = -13
  ji_Bereich2BW_TPSa(19) = -9
  ji_Bereich2BW_TPSa(20) = -7
  ji_Bereich2BW_TPSa(21) = -27
  ji_Bereich2BW_TPSa(22) = -25
  ji_Bereich2BW_TPSa(23) = -11
  ji_Bereich2BW_TPSa(24) = -6
  ji_Bereich2BW_TPSa(25) = 1
  ji_Bereich2BW_TPSa(26) = 4
  ji_Bereich2BW_TPSa(27) = 8
  ji_Bereich2BW_TPSa(28) = 11
  ji_Bereich2BW_TPSa(29) = 0
  ji_Bereich2BW_TPSa(30) = 1
  ji_Bereich2BW_TPSa(31) = 5
  ji_Bereich2BW_TPSa(32) = 6
  ji_Bereich2BW_TPSa(33) = 10
  ji_Bereich2BW_TPSa(34) = 14
  ji_Bereich2BW_TPSa(35) = 16
  ji_Bereich2BW_TPSa(36) = 0
  ji_Bereich2BW_TPSa(37) = 4
  ji_Bereich2BW_TPSa(38) = 9
  ji_Bereich2BW_TPSa(39) = 17
  ji_Bereich2BW_TPSa(40) = 7
  ji_Bereich2BW_TPSa(41) = 18
  ji_Bereich2BW_TPSa(42) = 3
  ji_Bereich2BW_TPSa(43) = 15
  ji_Bereich2BW_TPSa(44) = 5
  ji_Bereich2BW_TPSa(45) = 18
  Array_ji_Bereich2BW_TPSa = ji_Bereich2BW_TPSa

  'Return ji_Bereich2BW_TPSa
End Function


Function Array_ni_Bereich2BW_TPSa()
  Dim ni_Bereich2BW_TPSa() As Double
  ReDim ni_Bereich2BW_TPSa(46)

  ni_Bereich2BW_TPSa(0) = -0.39235983861984 * 10 ^ 6
  ni_Bereich2BW_TPSa(1) = 0.5152657382727 * 10 ^ 6
  ni_Bereich2BW_TPSa(2) = 0.40482443161048 * 10 ^ 5
  ni_Bereich2BW_TPSa(3) = -0.32193790923902 * 10 ^ 3
  ni_Bereich2BW_TPSa(4) = 0.96961424218694 * 10 ^ 2
  ni_Bereich2BW_TPSa(5) = -0.22867846371773 * 10 ^ 2
  ni_Bereich2BW_TPSa(6) = -0.44942914124357 * 10 ^ 6
  ni_Bereich2BW_TPSa(7) = -0.50118336020166 * 10 ^ 4
  ni_Bereich2BW_TPSa(8) = 0.35684463560015
  ni_Bereich2BW_TPSa(9) = 0.4423533584819 * 10 ^ 5
  ni_Bereich2BW_TPSa(10) = -0.13673388811708 * 10 ^ 5
  ni_Bereich2BW_TPSa(11) = 0.42163260207864 * 10 ^ 6
  ni_Bereich2BW_TPSa(12) = 0.22516925837475 * 10 ^ 5
  ni_Bereich2BW_TPSa(13) = 0.47442144865646 * 10 ^ 3
  ni_Bereich2BW_TPSa(14) = -0.14931130797647 * 10 ^ 3
  ni_Bereich2BW_TPSa(15) = -0.19781126320452 * 10 ^ 6
  ni_Bereich2BW_TPSa(16) = -0.2355439947076 * 10 ^ 5
  ni_Bereich2BW_TPSa(17) = -0.19070616302076 * 10 ^ 5
  ni_Bereich2BW_TPSa(18) = 0.55375669883164 * 10 ^ 5
  ni_Bereich2BW_TPSa(19) = 0.38293691437363 * 10 ^ 4
  ni_Bereich2BW_TPSa(20) = -0.60391860580567 * 10 ^ 3
  ni_Bereich2BW_TPSa(21) = 0.19363102620331 * 10 ^ 4
  ni_Bereich2BW_TPSa(22) = 0.4266064369861 * 10 ^ 4
  ni_Bereich2BW_TPSa(23) = -0.59780638872718 * 10 ^ 4
  ni_Bereich2BW_TPSa(24) = -0.70401463926862 * 10 ^ 3
  ni_Bereich2BW_TPSa(25) = 0.33836784107553 * 10 ^ 3
  ni_Bereich2BW_TPSa(26) = 0.20862786635187 * 10 ^ 2
  ni_Bereich2BW_TPSa(27) = 0.33834172656196 * 10 ^ -1
  ni_Bereich2BW_TPSa(28) = -0.43124428414893 * 10 ^ -4
  ni_Bereich2BW_TPSa(29) = 0.16653791356412 * 10 ^ 3
  ni_Bereich2BW_TPSa(30) = -0.13986292055898 * 10 ^ 3
  ni_Bereich2BW_TPSa(31) = -0.78849547999872
  ni_Bereich2BW_TPSa(32) = 0.72132411753872 * 10 ^ -1
  ni_Bereich2BW_TPSa(33) = -0.59754839398283 * 10 ^ -2
  ni_Bereich2BW_TPSa(34) = -0.12141358953904 * 10 ^ -4
  ni_Bereich2BW_TPSa(35) = 0.23227096733871 * 10 ^ -6
  ni_Bereich2BW_TPSa(36) = -0.10538463566194 * 10 ^ 2
  ni_Bereich2BW_TPSa(37) = 0.20718925496502 * 10 ^ 1
  ni_Bereich2BW_TPSa(38) = -0.72193155260427 * 10 ^ -1
  ni_Bereich2BW_TPSa(39) = 0.2074988708112 * 10 ^ -6
  ni_Bereich2BW_TPSa(40) = -0.18340657911379 * 10 ^ -1
  ni_Bereich2BW_TPSa(41) = 0.29036272348696 * 10 ^ -6
  ni_Bereich2BW_TPSa(42) = 0.21037527893619
  ni_Bereich2BW_TPSa(43) = 0.25681239729999 * 10 ^ -3
  ni_Bereich2BW_TPSa(44) = -0.12799002933781 * 10 ^ -1
  ni_Bereich2BW_TPSa(45) = -0.82198102652018 * 10 ^ -5
  Array_ni_Bereich2BW_TPSa = ni_Bereich2BW_TPSa

  'Return ni_Bereich2BW_TPSa
End Function

'#######################-Umkehrfunktionen-NEU-############################################################################
'#######################-Koeffizienten li - ji - ni (Tabelle 26, S.27) - für Bereich 2b Umkehrfunktionen-#################


Function Array_li_Bereich2BW_TPSb()
  Dim li_Bereich2BW_TPSb() As Integer
  ReDim li_Bereich2BW_TPSb(44)

  li_Bereich2BW_TPSb(0) = -6
  li_Bereich2BW_TPSb(1) = -6
  li_Bereich2BW_TPSb(2) = -5
  li_Bereich2BW_TPSb(3) = -5
  li_Bereich2BW_TPSb(4) = -4
  li_Bereich2BW_TPSb(5) = -4
  li_Bereich2BW_TPSb(6) = -4
  li_Bereich2BW_TPSb(7) = -3
  li_Bereich2BW_TPSb(8) = -3
  li_Bereich2BW_TPSb(9) = -3
  li_Bereich2BW_TPSb(10) = -3
  li_Bereich2BW_TPSb(11) = -2
  li_Bereich2BW_TPSb(12) = -2
  li_Bereich2BW_TPSb(13) = -2
  li_Bereich2BW_TPSb(14) = -2
  li_Bereich2BW_TPSb(15) = -1
  li_Bereich2BW_TPSb(16) = -1
  li_Bereich2BW_TPSb(17) = -1
  li_Bereich2BW_TPSb(18) = -1
  li_Bereich2BW_TPSb(19) = -1
  li_Bereich2BW_TPSb(20) = 0
  li_Bereich2BW_TPSb(21) = 0
  li_Bereich2BW_TPSb(22) = 0
  li_Bereich2BW_TPSb(23) = 0
  li_Bereich2BW_TPSb(24) = 0
  li_Bereich2BW_TPSb(25) = 0
  li_Bereich2BW_TPSb(26) = 0
  li_Bereich2BW_TPSb(27) = 1
  li_Bereich2BW_TPSb(28) = 1
  li_Bereich2BW_TPSb(29) = 1
  li_Bereich2BW_TPSb(30) = 1
  li_Bereich2BW_TPSb(31) = 1
  li_Bereich2BW_TPSb(32) = 1
  li_Bereich2BW_TPSb(33) = 2
  li_Bereich2BW_TPSb(34) = 2
  li_Bereich2BW_TPSb(35) = 2
  li_Bereich2BW_TPSb(36) = 3
  li_Bereich2BW_TPSb(37) = 3
  li_Bereich2BW_TPSb(38) = 3
  li_Bereich2BW_TPSb(39) = 4
  li_Bereich2BW_TPSb(40) = 4
  li_Bereich2BW_TPSb(41) = 5
  li_Bereich2BW_TPSb(42) = 5
  li_Bereich2BW_TPSb(43) = 5
  Array_li_Bereich2BW_TPSb = li_Bereich2BW_TPSb

  'Return li_Bereich2BW_TPSb
End Function


Function Array_ji_Bereich2BW_TPSb()
  Dim ji_Bereich2BW_TPSb() As Integer
  ReDim ji_Bereich2BW_TPSb(44)

  ji_Bereich2BW_TPSb(0) = 0
  ji_Bereich2BW_TPSb(1) = 11
  ji_Bereich2BW_TPSb(2) = 0
  ji_Bereich2BW_TPSb(3) = 11
  ji_Bereich2BW_TPSb(4) = 0
  ji_Bereich2BW_TPSb(5) = 1
  ji_Bereich2BW_TPSb(6) = 11
  ji_Bereich2BW_TPSb(7) = 0
  ji_Bereich2BW_TPSb(8) = 1
  ji_Bereich2BW_TPSb(9) = 11
  ji_Bereich2BW_TPSb(10) = 12
  ji_Bereich2BW_TPSb(11) = 0
  ji_Bereich2BW_TPSb(12) = 1
  ji_Bereich2BW_TPSb(13) = 6
  ji_Bereich2BW_TPSb(14) = 10
  ji_Bereich2BW_TPSb(15) = 0
  ji_Bereich2BW_TPSb(16) = 1
  ji_Bereich2BW_TPSb(17) = 5
  ji_Bereich2BW_TPSb(18) = 8
  ji_Bereich2BW_TPSb(19) = 9
  ji_Bereich2BW_TPSb(20) = 0
  ji_Bereich2BW_TPSb(21) = 1
  ji_Bereich2BW_TPSb(22) = 2
  ji_Bereich2BW_TPSb(23) = 4
  ji_Bereich2BW_TPSb(24) = 5
  ji_Bereich2BW_TPSb(25) = 6
  ji_Bereich2BW_TPSb(26) = 9
  ji_Bereich2BW_TPSb(27) = 0
  ji_Bereich2BW_TPSb(28) = 1
  ji_Bereich2BW_TPSb(29) = 2
  ji_Bereich2BW_TPSb(30) = 3
  ji_Bereich2BW_TPSb(31) = 7
  ji_Bereich2BW_TPSb(32) = 8
  ji_Bereich2BW_TPSb(33) = 0
  ji_Bereich2BW_TPSb(34) = 1
  ji_Bereich2BW_TPSb(35) = 5
  ji_Bereich2BW_TPSb(36) = 0
  ji_Bereich2BW_TPSb(37) = 1
  ji_Bereich2BW_TPSb(38) = 3
  ji_Bereich2BW_TPSb(39) = 0
  ji_Bereich2BW_TPSb(40) = 1
  ji_Bereich2BW_TPSb(41) = 0
  ji_Bereich2BW_TPSb(42) = 1
  ji_Bereich2BW_TPSb(43) = 2
  Array_ji_Bereich2BW_TPSb = ji_Bereich2BW_TPSb

  'Return ji_Bereich2BW_TPSb
End Function


Function Array_ni_Bereich2BW_TPSb()
  Dim ni_Bereich2BW_TPSb() As Double
  ReDim ni_Bereich2BW_TPSb(44)

  ni_Bereich2BW_TPSb(0) = 0.31687665083497 * 10 ^ 6
  ni_Bereich2BW_TPSb(1) = 0.20864175881858 * 10 ^ 2
  ni_Bereich2BW_TPSb(2) = -0.39859399803599 * 10 ^ 6
  ni_Bereich2BW_TPSb(3) = -0.21816058518877 * 10 ^ 2
  ni_Bereich2BW_TPSb(4) = 0.22369785194242 * 10 ^ 6
  ni_Bereich2BW_TPSb(5) = -0.27841703445817 * 10 ^ 4
  ni_Bereich2BW_TPSb(6) = 0.9920743607148 * 10 ^ 1
  ni_Bereich2BW_TPSb(7) = -0.75197512299157 * 10 ^ 5
  ni_Bereich2BW_TPSb(8) = 0.29708605951158 * 10 ^ 4
  ni_Bereich2BW_TPSb(9) = -0.34406878548526 * 10 ^ 1
  ni_Bereich2BW_TPSb(10) = 0.38815564249115
  ni_Bereich2BW_TPSb(11) = 0.1751129508575 * 10 ^ 5
  ni_Bereich2BW_TPSb(12) = -0.14237112854449 * 10 ^ 4
  ni_Bereich2BW_TPSb(13) = 0.10943803364167 * 10 ^ 1
  ni_Bereich2BW_TPSb(14) = 0.89971619308495
  ni_Bereich2BW_TPSb(15) = -0.33759740098958 * 10 ^ 4
  ni_Bereich2BW_TPSb(16) = 0.47162885818355 * 10 ^ 3
  ni_Bereich2BW_TPSb(17) = -0.19188241993679 * 10 ^ 1
  ni_Bereich2BW_TPSb(18) = 0.41078580492196
  ni_Bereich2BW_TPSb(19) = -0.33465378172097
  ni_Bereich2BW_TPSb(20) = 0.13870034777505 * 10 ^ 4
  ni_Bereich2BW_TPSb(21) = -0.40663326195838 * 10 ^ 3
  ni_Bereich2BW_TPSb(22) = 0.4172734715961 * 10 ^ 2
  ni_Bereich2BW_TPSb(23) = 0.21932549434532 * 10 ^ 1
  ni_Bereich2BW_TPSb(24) = -0.10320050009077 * 10 ^ 1
  ni_Bereich2BW_TPSb(25) = 0.35882943516703
  ni_Bereich2BW_TPSb(26) = 0.52511453726066 * 10 ^ -2
  ni_Bereich2BW_TPSb(27) = 0.12838916450705 * 10 ^ 2
  ni_Bereich2BW_TPSb(28) = -0.28642437219381 * 10 ^ 1
  ni_Bereich2BW_TPSb(29) = 0.56912683664855
  ni_Bereich2BW_TPSb(30) = -0.99962954584931 * 10 ^ -1
  ni_Bereich2BW_TPSb(31) = -0.32632037778459 * 10 ^ -2
  ni_Bereich2BW_TPSb(32) = 0.23320922576723 * 10 ^ -3
  ni_Bereich2BW_TPSb(33) = -0.1533480985745
  ni_Bereich2BW_TPSb(34) = 0.29072288239902 * 10 ^ -1
  ni_Bereich2BW_TPSb(35) = 0.37534702741167 * 10 ^ -3
  ni_Bereich2BW_TPSb(36) = 0.17296691702411 * 10 ^ -2
  ni_Bereich2BW_TPSb(37) = -0.38556050844504 * 10 ^ -3
  ni_Bereich2BW_TPSb(38) = -0.35017712292608 * 10 ^ -4
  ni_Bereich2BW_TPSb(39) = -0.14566393631492 * 10 ^ -4
  ni_Bereich2BW_TPSb(40) = 0.56420857267269 * 10 ^ -5
  ni_Bereich2BW_TPSb(41) = 0.41286150074605 * 10 ^ -7
  ni_Bereich2BW_TPSb(42) = -0.20684671118824 * 10 ^ -7
  ni_Bereich2BW_TPSb(43) = 0.16409393674725 * 10 ^ -8
  Array_ni_Bereich2BW_TPSb = ni_Bereich2BW_TPSb

  'Return ni_Bereich2BW_TPSb
End Function

'#######################-Umkehrfunktionen-NEU-############################################################################
'#######################-Koeffizienten li - ji - ni (Tabelle 27, S.28) - für Bereich 2c Umkehrfunktionen-#################


Function Array_li_Bereich2BW_TPSc()
  Dim li_Bereich2BW_TPSc() As Integer
  ReDim li_Bereich2BW_TPSc(30)

  li_Bereich2BW_TPSc(0) = -2
  li_Bereich2BW_TPSc(1) = -2
  li_Bereich2BW_TPSc(2) = -1
  li_Bereich2BW_TPSc(3) = 0
  li_Bereich2BW_TPSc(4) = 0
  li_Bereich2BW_TPSc(5) = 0
  li_Bereich2BW_TPSc(6) = 0
  li_Bereich2BW_TPSc(7) = 1
  li_Bereich2BW_TPSc(8) = 1
  li_Bereich2BW_TPSc(9) = 1
  li_Bereich2BW_TPSc(10) = 1
  li_Bereich2BW_TPSc(11) = 2
  li_Bereich2BW_TPSc(12) = 2
  li_Bereich2BW_TPSc(13) = 2
  li_Bereich2BW_TPSc(14) = 3
  li_Bereich2BW_TPSc(15) = 3
  li_Bereich2BW_TPSc(16) = 3
  li_Bereich2BW_TPSc(17) = 4
  li_Bereich2BW_TPSc(18) = 4
  li_Bereich2BW_TPSc(19) = 4
  li_Bereich2BW_TPSc(20) = 5
  li_Bereich2BW_TPSc(21) = 5
  li_Bereich2BW_TPSc(22) = 5
  li_Bereich2BW_TPSc(23) = 6
  li_Bereich2BW_TPSc(24) = 6
  li_Bereich2BW_TPSc(25) = 7
  li_Bereich2BW_TPSc(26) = 7
  li_Bereich2BW_TPSc(27) = 7
  li_Bereich2BW_TPSc(28) = 7
  li_Bereich2BW_TPSc(29) = 7
  Array_li_Bereich2BW_TPSc = li_Bereich2BW_TPSc

  'Return li_Bereich2BW_TPSc
End Function


Function Array_ji_Bereich2BW_TPSc()
  Dim ji_Bereich2BW_TPSc() As Integer
  ReDim ji_Bereich2BW_TPSc(30)

  ji_Bereich2BW_TPSc(0) = 0
  ji_Bereich2BW_TPSc(1) = 1
  ji_Bereich2BW_TPSc(2) = 0
  ji_Bereich2BW_TPSc(3) = 0
  ji_Bereich2BW_TPSc(4) = 1
  ji_Bereich2BW_TPSc(5) = 2
  ji_Bereich2BW_TPSc(6) = 3
  ji_Bereich2BW_TPSc(7) = 0
  ji_Bereich2BW_TPSc(8) = 1
  ji_Bereich2BW_TPSc(9) = 3
  ji_Bereich2BW_TPSc(10) = 4
  ji_Bereich2BW_TPSc(11) = 0
  ji_Bereich2BW_TPSc(12) = 1
  ji_Bereich2BW_TPSc(13) = 2
  ji_Bereich2BW_TPSc(14) = 0
  ji_Bereich2BW_TPSc(15) = 1
  ji_Bereich2BW_TPSc(16) = 5
  ji_Bereich2BW_TPSc(17) = 0
  ji_Bereich2BW_TPSc(18) = 1
  ji_Bereich2BW_TPSc(19) = 4
  ji_Bereich2BW_TPSc(20) = 0
  ji_Bereich2BW_TPSc(21) = 1
  ji_Bereich2BW_TPSc(22) = 2
  ji_Bereich2BW_TPSc(23) = 0
  ji_Bereich2BW_TPSc(24) = 1
  ji_Bereich2BW_TPSc(25) = 0
  ji_Bereich2BW_TPSc(26) = 1
  ji_Bereich2BW_TPSc(27) = 3
  ji_Bereich2BW_TPSc(28) = 4
  ji_Bereich2BW_TPSc(29) = 5
  Array_ji_Bereich2BW_TPSc = ji_Bereich2BW_TPSc

  'Return ji_Bereich2BW_TPSc
End Function


Function Array_ni_Bereich2BW_TPSc()
  Dim ni_Bereich2BW_TPSc() As Double
  ReDim ni_Bereich2BW_TPSc(30)

  ni_Bereich2BW_TPSc(0) = 0.90968501005365 * 10 ^ 3
  ni_Bereich2BW_TPSc(1) = 0.2404566708842 * 10 ^ 4
  ni_Bereich2BW_TPSc(2) = -0.5916232638713 * 10 ^ 3
  ni_Bereich2BW_TPSc(3) = 0.54145404128074 * 10 ^ 3
  ni_Bereich2BW_TPSc(4) = -0.27098308411192 * 10 ^ 3
  ni_Bereich2BW_TPSc(5) = 0.97976525097926 * 10 ^ 3
  ni_Bereich2BW_TPSc(6) = -0.46966772959435 * 10 ^ 3
  ni_Bereich2BW_TPSc(7) = 0.14399274604723 * 10 ^ 2
  ni_Bereich2BW_TPSc(8) = -0.19104204230429 * 10 ^ 2
  ni_Bereich2BW_TPSc(9) = 0.53299167111971 * 10 ^ 1
  ni_Bereich2BW_TPSc(10) = -0.21252975375934 * 10 ^ 2
  ni_Bereich2BW_TPSc(11) = -0.3114733441376
  ni_Bereich2BW_TPSc(12) = 0.60334840894623
  ni_Bereich2BW_TPSc(13) = -0.42764839702509 * 10 ^ -1
  ni_Bereich2BW_TPSc(14) = 0.58185597255259 * 10 ^ -2
  ni_Bereich2BW_TPSc(15) = -0.14597008284753 * 10 ^ -1
  ni_Bereich2BW_TPSc(16) = 0.56631175631027 * 10 ^ -2
  ni_Bereich2BW_TPSc(17) = -0.76155864584577 * 10 ^ -4
  ni_Bereich2BW_TPSc(18) = 0.22440342919332 * 10 ^ -3
  ni_Bereich2BW_TPSc(19) = -0.12561095013413 * 10 ^ -4
  ni_Bereich2BW_TPSc(20) = 0.63323132660934 * 10 ^ -6
  ni_Bereich2BW_TPSc(21) = -0.20541989675375 * 10 ^ -5
  ni_Bereich2BW_TPSc(22) = 0.36405370390082 * 10 ^ -7
  ni_Bereich2BW_TPSc(23) = -0.29759897789215 * 10 ^ -8
  ni_Bereich2BW_TPSc(24) = 0.10136618529763 * 10 ^ -7
  ni_Bereich2BW_TPSc(25) = 0.59925719692351 * 10 ^ -11
  ni_Bereich2BW_TPSc(26) = -0.20677870105164 * 10 ^ -10
  ni_Bereich2BW_TPSc(27) = -0.20874278181886 * 10 ^ -10
  ni_Bereich2BW_TPSc(28) = 0.10162166825089 * 10 ^ -9
  ni_Bereich2BW_TPSc(29) = -0.16429828281347 * 10 ^ -9
  Array_ni_Bereich2BW_TPSc = ni_Bereich2BW_TPSc

  'Return ni_Bereich2BW_TPSc
End Function

'Arrayfunktionen fuer Berechnung Viskositaet Wasserdampfs nach "IAPWS 2008 for the viscosity of ordinary water substance"
'H_null() fuer rho_null Glied (Tab 1 Seite 5)

Function Array_H_null()
  Dim H_null() As Double
  ReDim H_null(3)
  H_null(0) = 1.67752
  H_null(1) = 2.20462
  H_null(2) = 0.6366564
  H_null(3) = -0.241605
  Array_H_null = H_null()
  'Return H_null
End Function
'H_null() ist ein 5*6 matrix fuer rHo_eins glied (tab 2 seite 5)

Function Array_H_1()
  Dim H_1() As Double
  ReDim H_1(5, 6)     'undefinierte elemente sind null
  H_1(0, 0) = 0.520094
  H_1(1, 0) = 0.0850895
  H_1(2, 0) = -1.08374
  H_1(3, 0) = -0.289555
  H_1(0, 1) = 0.222531
  H_1(1, 1) = 0.999115
  H_1(2, 1) = 1.88797
  H_1(3, 1) = 1.26613
  H_1(5, 1) = 0.120573
  H_1(0, 2) = -0.281378
  H_1(1, 2) = -0.906851
  H_1(2, 2) = -0.772479
  H_1(3, 2) = -0.489837
  H_1(4, 2) = -0.25704
  H_1(0, 3) = 0.161913
  H_1(1, 3) = 0.257399
  H_1(0, 4) = -0.0325372
  H_1(3, 4) = 0.0698452
  H_1(4, 5) = 0.00872102
  H_1(3, 6) = -0.00435673
  H_1(5, 6) = -0.000593264
  Array_H_1 = H_1()
  'Return H_1
End Function


'Arrayfunktionen fuer Berechnung Waemeleitfaeigkeit nach dem Buchversion!!

Function Array_nLambda_null()
  Dim nLambda_null(3) As Double
  nLambda_null(0) = 0.0102811
  nLambda_null(1) = 0.0299621
  nLambda_null(2) = 0.0156146
  nLambda_null(3) = -0.00422464
  Array_nLambda_null = nLambda_null()
End Function

Function Array_nLambda_1()
  Dim nLambda_1(4) As Double
  nLambda_1(0) = -0.39707
  nLambda_1(1) = 0.400302
  nLambda_1(2) = 1.06
  nLambda_1(3) = -0.171587
  nLambda_1(4) = 2.39219
  Array_nLambda_1 = nLambda_1()
End Function

Function Array_nLambda_2()
  Dim nLambda_2(9) As Double
  nLambda_2(0) = 0.0701309
  nLambda_2(1) = 0.011852
  nLambda_2(2) = 0.642857
  nLambda_2(3) = 0.00169937
  nLambda_2(4) = -1.02
  nLambda_2(5) = -4.11717
  nLambda_2(6) = -6.17937
  nLambda_2(7) = 0.0822994
  nLambda_2(8) = 10.0932
  nLambda_2(9) = 0.00308976
  Array_nLambda_2 = nLambda_2()
End Function



'Arrayfunktionen fuer Berechnung Waemeleitfaeigkeit lambda Wasserdampfs nach "IAPWS 2011 for the thermal conductivity of ordinary water substance"
'L_null() fuer lambda_null Glied (Tab 1 Seite 5)

Function Array_L_null()
  Dim L_null() As Double
  ReDim L_null(4)
  L_null(0) = 0.002443221
  L_null(1) = 0.01323095
  L_null(2) = 0.006770357
  L_null(3) = -0.003454586
  L_null(4) = 0.0004096266
  Array_L_null = L_null()
  'Return L_null
End Function

'L_null() ist ein 5*6 Matrix fuer lambda_eins Glied (Tab 2 Seite 5)

Function Array_L_1()
  Dim L_1() As Double
  ReDim L_1(4, 5)     'undefinierte Elemente sind Null
  L_1(0, 0) = 1.60397357
  L_1(0, 1) = -0.646013523
  L_1(0, 2) = 0.111443906
  L_1(0, 3) = 0.102997357
  L_1(0, 4) = -0.0504123634
  L_1(0, 5) = 0.00609859258
  L_1(1, 0) = 2.33771842
  L_1(1, 1) = -2.78843778
  L_1(1, 2) = 1.53616167
  L_1(1, 3) = -0.463045512
  L_1(1, 4) = 0.0832827019
  L_1(1, 5) = -0.00719201245
  L_1(2, 0) = 2.19650529
  L_1(2, 1) = -4.54580785
  L_1(2, 2) = 3.55777244
  L_1(2, 3) = -1.40944978
  L_1(2, 4) = 0.275418278
  L_1(2, 5) = -0.0205938816
  L_1(3, 0) = -1.21051378
  L_1(3, 1) = 1.60812989
  L_1(3, 2) = -0.621178141
  L_1(3, 3) = 0.0716373224
  L_1(3, 4) = 0
  L_1(3, 5) = 0
  L_1(4, 0) = -2.720337
  L_1(4, 1) = 4.57586331
  L_1(4, 2) = -3.18369245
  L_1(4, 3) = 1.1168348
  L_1(4, 4) = -0.19268305
  L_1(4, 5) = 0.012913842
  Array_L_1 = L_1
  'Return L_1
End Function

Function Array_A_2()
  Dim A_2() As Double
  ReDim A_2(5, 4)
  A_2(0, 0) = 6.53786807199516
  A_2(0, 1) = 6.52717759281799
  A_2(0, 2) = 5.35500529896124
  A_2(0, 3) = 1.55225959906681
  A_2(0, 4) = 1.11999926419994
  
  A_2(1, 0) = -5.61149954923348
  A_2(1, 1) = -6.30816983387575
  A_2(1, 2) = -3.96415689925446
  A_2(1, 3) = 0.464621290821181
  A_2(1, 4) = 0.595748562571649
  
  A_2(2, 0) = 3.39624167361325
  A_2(2, 1) = 8.08379285492595
  A_2(2, 2) = 8.91990208918795
  A_2(2, 3) = 8.93237374861479
  A_2(2, 4) = 9.8895256507892
  
  A_2(3, 0) = -2.27492629730878
  A_2(3, 1) = -9.82240510197603
  A_2(3, 2) = -12.033872950579
  A_2(3, 3) = -11.0321960061126
  A_2(3, 4) = -10.325505114704
  
  A_2(4, 0) = 10.2631854662709
  A_2(4, 1) = 12.1358413791395
  A_2(4, 2) = 9.19494865194302
  A_2(4, 3) = 6.1678099993336
  A_2(4, 4) = 4.66861294457414
  
  A_2(5, 0) = 1.97815050331519
  A_2(5, 1) = -5.54349664571295
  A_2(5, 2) = -2.16866274479712
  A_2(5, 3) = -0.965458722086812
  A_2(5, 4) = -0.503243546373828
  
  Array_A_2 = A_2
End Function

