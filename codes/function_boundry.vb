Option Explicit
Option Private Module


Public Function X_WD_PS_T(ByVal Temperatur As Double) As Double 'saturation pressure ps(T)  Gl. (30) - S.33

  If Temperatur < 0 Or Temperatur > 374 Then GoTo Abbruch '373.946

  Dim A_Temp As Double
  Dim B_Temp As Double
  Dim C_Temp As Double
  Dim Temperatur_Temp#

  Temperatur_Temp = Temperatur + 273.15 + n_Bereich4_9 / (Temperatur + 273.15 - n_Bereich4_10)
  A_Temp = Temperatur_Temp ^ 2 + n_Bereich4_1 * Temperatur_Temp + n_Bereich4_2
  B_Temp = n_Bereich4_3 * Temperatur_Temp ^ 2 + n_Bereich4_4 * Temperatur_Temp + n_Bereich4_5
  C_Temp = n_Bereich4_6 * Temperatur_Temp ^ 2 + n_Bereich4_7 * Temperatur_Temp + n_Bereich4_8

  X_WD_PS_T = (2 * C_Temp / (-B_Temp + (B_Temp ^ 2 - 4 * A_Temp * C_Temp) ^ 0.5)) ^ 4 * 10

  Exit Function

Abbruch:
    X_WD_PS_T = -1
    
End Function

Public Function X_WD_TS_P(ByVal Druck As Double) As Double  'saturation temperature Ts(p)   Gl. (31) - S.35

  'On Error GoTo Abbruch
  If Druck < 0.006 Or Druck > 220.64 Then GoTo Abbruch

  Dim beta_Temp As Double
  Dim D_Temp As Double
  Dim E_Temp As Double
  Dim F_Temp As Double
  Dim G_Temp As Double

  beta_Temp = (0.1 * Druck) ^ 0.25
  E_Temp = beta_Temp ^ 2 + n_Bereich4_3 * beta_Temp + n_Bereich4_6
  F_Temp = n_Bereich4_1 * beta_Temp ^ 2 + n_Bereich4_4 * beta_Temp + n_Bereich4_7
  G_Temp = n_Bereich4_2 * beta_Temp ^ 2 + n_Bereich4_5 * beta_Temp + n_Bereich4_8
  D_Temp = 2 * G_Temp / (-F_Temp - (F_Temp ^ 2 - 4 * E_Temp * G_Temp) ^ 0.5)
  X_WD_TS_P = 0.5 * (n_Bereich4_10 + D_Temp - ((n_Bereich4_10 + D_Temp) ^ 2 - 4 * (n_Bereich4_9 + n_Bereich4_10 * D_Temp)) ^ 0.5)
  X_WD_TS_P = X_WD_TS_P - 273.15

  Exit Function

Abbruch:
    X_WD_TS_P = -1
    
End Function

Public Function X_WD_PB_T(ByVal Temperatur As Double) As Double     'Pressure Boundary pb(T)

  'If CheckLicense() = False Then Return Fehlercode

  X_WD_PB_T = WD_PB_T(Temperatur)

End Function

Public Function X_WD_TB_P(ByVal Druck As Double) As Double          'Temperature Boundary Tb(p)

  'If CheckLicense() = False Then Return Fehlercode

  X_WD_TB_P = WD_TB_P(Druck)

End Function

Public Function WD_PB_T(ByVal Temperatur As Double) As Double       'Pressure Boundary pb(T), Region 2 -> 3

  'If CheckLicense() = False Then Return Fehlercode

  If Temperatur < 350 Or Temperatur > 590 Then GoTo Abbruch

  Dim omega#
  Const n1# = 0.34805185628969 * 10 ^ 3
  Const n2# = -0.11671859879976 * 10 ^ 1
  Const n3# = 0.10192970039326 * 10 ^ -2

  omega = Temperatur + 273.15
  WD_PB_T = n1 + n2 * omega + n3 * omega ^ 2                      'Gl.(5) - S.5
  WD_PB_T = WD_PB_T * 10 'Umrechnung MPa in bar

  Exit Function

Abbruch:
    WD_PB_T = -1
End Function

Public Function WD_PB_H(ByVal Enthalpie As Double) As Double        'Pressure Boundary pb(h), Region 2 -> 3

  'If CheckLicense() = False Then Return Fehlercode

  Dim eta#
  Const n1# = 0.90584278514723 * 10 ^ 3
  Const n2# = -0.67955786399241
  Const n3# = 0.12809002730136 * 10 ^ -3

  eta = Enthalpie
  WD_PB_H = n1 + n2 * eta + n3 * eta ^ 2
  WD_PB_H = WD_PB_H * 10 'Umrechnung MPa in bar                   'Modified equation - no source available

  Exit Function

Abbruch:
    WD_PB_H = -1
    
End Function

Public Function WD_TB_P(ByVal Druck As Double) As Double            'Temperature boundary Tb(p), Region 2 -> 3

  'If CheckLicense() = False Then Return Fehlercode

  If Druck < 165.292 Or Druck > 1000 Then GoTo Abbruch

  Const n3# = 0.10192970039326 * 10 ^ -2
  Const n4# = 0.57254459862746 * 10 ^ 3
  Const n5# = 0.1391883977887 * 10 ^ 2

  WD_TB_P = n4 + ((Druck / 10 - n5) / n3) ^ 0.5 - 273.15          ' Gl.(6) - S.6

  Exit Function

Abbruch:
    WD_TB_P = -1
End Function

Public Function WD_Bereich_PT(ByVal Druck As Double, ByVal Temperatur As Double) As Integer   'Bereichsabfrage mit den Eingangsgrößen Druck und Temperatur

  If Temperatur < 0 Or Temperatur > 2000 Then GoTo Abbruch
  If Druck <= 0 Or Druck > 1000 Then GoTo Abbruch

  Select Case Temperatur
    Case Is < 374 '<= 373.946
      Select Case X_WD_PS_T(Temperatur)
        Case Druck - 0.00000001 To Druck + 0.00000001
          WD_Bereich_PT = 4
        Case Is > Druck + 0.00000001
          WD_Bereich_PT = 2
        Case Is < Druck - 0.00000001
          WD_Bereich_PT = 1
      End Select

    Case 374 To 590 ' To entspricht <= '373.946
      Select Case WD_PB_T(Temperatur)
        Case Is <= Druck
          WD_Bereich_PT = 3
        Case Is > Druck
          WD_Bereich_PT = 2
      End Select

    Case 590 To 800 ' To entspricht <=
      WD_Bereich_PT = 2

    Case 800 To 2000 ' To entspricht <=
      WD_Bereich_PT = 5

  End Select

  Exit Function

Abbruch:
  WD_Bereich_PT = -1
End Function

Public Function WD_Bereich_HP(ByVal Enthalpie As Double, ByVal Druck As Double) As Integer    'Bereichsabfrage mit den Eingangsgrößen Enthalpie und Druck

  'On Error GoTo Abbruch
  If Druck <= 0 Or Druck > 1000 Then GoTo Abbruch
  If Enthalpie < 0 Then GoTo Abbruch

  Dim hs1#, hs2#, h0#
  h0 = Enthalpie

  Select Case Druck
    Case Is <= 220.64
      Dim Ts#, hs3#

      Ts = X_WD_TS_P(Druck)
      hs1 = X_WD_H_PTX(Druck, Ts, 0)
      hs2 = X_WD_H_PTX(Druck, Ts, 1)
      hs3 = X_WD_H_PTX(Druck, 800, 1)

      If h0 < hs1 Then WD_Bereich_HP = 1
      If h0 >= hs1 And h0 <= hs2 Then WD_Bereich_HP = 4
      If h0 > hs2 And h0 <= hs3 Then WD_Bereich_HP = 2
      If h0 > hs3 And Druck <= 10 Then WD_Bereich_HP = 5
      If h0 > hs3 And Druck > 10 Then WD_Bereich_HP = -1

    Case Else
      Dim tb#

      tb = WD_TB_P(Druck)
      hs1 = X_WD_H_PTX(Druck, 350, 1)
      hs2 = X_WD_H_PTX(Druck, tb, 1)

      If h0 < hs1 Then WD_Bereich_HP = 1
      If h0 >= hs1 And h0 <= hs2 Then WD_Bereich_HP = 3
      If h0 > hs2 Then WD_Bereich_HP = 2

  End Select

  Exit Function

Abbruch:
  WD_Bereich_HP = -1
End Function

Public Function WD_Bereich_SP(ByVal Entropie As Double, ByVal Druck As Double) As Integer   'Bereichsabfrage mit den Eingangsgrößen Druck und Entropie

  'On Error GoTo Abbruch
  If Druck <= 0 Or Druck > 1000 Then GoTo Abbruch
  If Entropie < 0 Then GoTo Abbruch

  Dim ss1#, ss2#, s0#
  s0 = Entropie

  Select Case Druck
    Case Is <= 220.64
      Dim Ts#, ss3#

      Ts = X_WD_TS_P(Druck)
      ss1 = X_WD_S_PTX(Druck, Ts, 0)
      ss2 = X_WD_S_PTX(Druck, Ts, 1)
      ss3 = X_WD_S_PTX(Druck, 800, 1)

      If s0 < ss1 Then WD_Bereich_SP = 1
      If s0 >= ss1 And s0 <= ss2 Then WD_Bereich_SP = 4
      If s0 > ss2 And s0 <= ss3 Then WD_Bereich_SP = 2
      If s0 > ss3 And Druck <= 10 Then WD_Bereich_SP = 5
      If s0 > ss3 And Druck > 10 Then WD_Bereich_SP = -1

    Case Else
      Dim tb#

      tb = WD_TB_P(Druck)
      ss1 = X_WD_S_PTX(Druck, 350, 1)
      ss2 = X_WD_S_PTX(Druck, tb, 1)

      If s0 < ss1 Then WD_Bereich_SP = 1
      If s0 >= ss1 And s0 <= ss2 Then WD_Bereich_SP = 3
      If s0 > ss2 Then WD_Bereich_SP = 2

  End Select

  Exit Function

Abbruch:
  WD_Bereich_SP = -1
End Function

Public Function WD_Bereich_HT(ByVal Enthalpie As Double, ByVal Temperatur As Double) As Integer   'Bereichsabfrage mit den Eingangsgrößen Enthalpie und Temperatur

  'funktioniert nicht im Bereich 4!

  'On Error GoTo Abbruch
  If Temperatur < 0 Or Temperatur > 2000 Then GoTo Abbruch
  If Enthalpie < 0 Then GoTo Abbruch

  Dim h0#, hs1#, hs2#
  h0 = Enthalpie

  Select Case Temperatur
    Case Is < 374 '<= 373.946 '350
      Dim ps#

      ps = X_WD_PS_T(Temperatur)
      hs1 = X_WD_H_PTX(ps, Temperatur, 0)
      hs2 = X_WD_H_PTX(ps, Temperatur, 1)

      If h0 <= hs1 Then WD_Bereich_HT = 1
      If h0 > hs1 And h0 <= hs2 Then WD_Bereich_HT = 4
      If h0 > hs2 Then WD_Bereich_HT = 2

    Case 374 To 590 '373.946
      Dim pB#

      pB = WD_PB_T(Temperatur)
      hs1 = X_WD_H_PTX(pB, Temperatur, 1)

      If h0 > hs1 Then WD_Bereich_HT = 2
      If h0 <= hs1 Then WD_Bereich_HT = 3

    Case 590 To 800
      WD_Bereich_HT = 2

    Case Is > 800
      If X_WD_H_PTX(10, Temperatur, 1) < h0 Then
        WD_Bereich_HT = 5
      Else
        WD_Bereich_HT = -1
      End If

    Case Else
      WD_Bereich_HT = 1

  End Select

  Exit Function

Abbruch:
  WD_Bereich_HT = -1
End Function

