Option Explicit
Option Private Module
'##############################################################################################################################
'################################ Ab hier der Programmcode fuer die Berechnung der Stoffwerte ##################################
'##################################### von Wasser und Wasserdampf nach IAPWS IF 97 ############################################
'################################# Alle Gleichungs und Seitennummern beziehen sich auf die ####################################
'################################## offizielle Release aus 2007 auf Grundlage der IF - 97 #####################################
'##############################################################################################################################

'Wasserdampf (X_WD_...)

' ###################  BEMERKUNG!!!  ######################
' Gaskonstante_Wasser ist in Einheit kJ/(kg*K), deshalb alle berechnete Stoffwerte sind in k-Einheit
' z.B. h(spez. Enthalpie) in kJ/kg
' #########################################################

' Dim x As Double 'nur fuer x als f(s,p)

' Bereriche 1 und 2

Private x, Druck_Reduziert, Temperatur_Reduziert, Dichte_Reduziert, Enthalpie_Reduziert, _
  Entropie_Reduziert, Temperatur_sat, Druck_Sat As Double

'alphaV: isobare kubische Expansionscoeffizient
Public Function WD_alphaV1(ByVal Druck As Double, ByVal Temperatur As Double) As Double
  Druck_Reduziert = 0.1 * Druck / 16.53
  Temperatur_Reduziert = 1386 / (Temperatur + 273.15)

  Dim gpt, gp As Double
  gpt = WD_gpt_Bereich1(Druck_Reduziert, Temperatur_Reduziert)
  gp = WD_gp_Bereich1(Druck_Reduziert, Temperatur_Reduziert)

  WD_alphaV1 = 1 / (Temperatur + 273.15) * (1 - Temperatur_Reduziert * gpt / gp)

End Function

Public Function WD_alphaV2(ByVal Druck As Double, ByVal Temperatur As Double) As Double
  Druck_Reduziert = 0.1 * Druck
  Temperatur_Reduziert = 540 / (Temperatur + 273.15)

  Dim grp, grpt As Double
  grp = WD_grp_Bereich2(Druck_Reduziert, Temperatur_Reduziert)
  grpt = WD_grpt_Bereich2(Druck_Reduziert, Temperatur_Reduziert)

  WD_alphaV2 = 1 / (Temperatur + 273.15) * _
    (1 + Druck_Reduziert * grp - Temperatur_Reduziert * Druck_Reduziert * grpt) / _
    (1 + Druck_Reduziert * grp)
End Function

'kappaT: isotherme Kompressibilität
Public Function WD_kappaT1(ByVal Druck As Double, ByVal Temperatur As Double) As Double
  Druck_Reduziert = 0.1 * Druck / 16.53
  Temperatur_Reduziert = 1386 / (Temperatur + 273.15)
  Dim gpp, gp As Double
  gpp = WD_gpp_Bereich1(Druck_Reduziert, Temperatur_Reduziert)
  gp = WD_gp_Bereich1(Druck_Reduziert, Temperatur_Reduziert)
  
  WD_kappaT1 = 1 / Druck * (-Druck_Reduziert * gpp / gp)
  
End Function

Public Function WD_kappaT2(ByVal Druck As Double, ByVal Temperatur As Double) As Double
  Druck_Reduziert = 0.1 * Druck
  Temperatur_Reduziert = 540 / (Temperatur + 273.15)
  Dim grpp, grp As Double
  grpp = WD_grpp_Bereich2(Druck_Reduziert, Temperatur_Reduziert)
  grp = WD_grp_Bereich2(Druck_Reduziert, Temperatur_Reduziert)
  
  WD_kappaT2 = 1 / Druck * _
    (1 - Druck_Reduziert ^ 2 * grpp) / (1 + Druck_Reduziert * grp)
End Function

Public Function WD_cp1(ByVal Druck As Double, ByVal Temperatur As Double) As Double     'Tabelle 3 - S.8
  
  Druck_Reduziert = 0.1 * Druck / 16.53
  Temperatur_Reduziert = 1386 / (Temperatur + 273.15)

  WD_cp1 = -Gaskonstante_Wasser * Temperatur_Reduziert ^ 2 * WD_gtt_Bereich1(Druck_Reduziert, Temperatur_Reduziert)

End Function

Public Function WD_cp2(ByVal Druck As Double, ByVal Temperatur As Double) As Double     'Tabelle 12 - S.15
  
  Druck_Reduziert = 0.1 * Druck
  Temperatur_Reduziert = 540 / (Temperatur + 273.15)

  'Stable Vapor Region
  'If Druck_Reduziert > 10    Then
  WD_cp2 = -Gaskonstante_Wasser * Temperatur_Reduziert ^ 2 * (WD_g0tt_Bereich2(Temperatur_Reduziert) + WD_grtt_Bereich2(Druck_Reduziert, Temperatur_Reduziert))

  'Metastable Vapor Region
  'If Druck_Reduziert < 10 Or Druck_Reduziert = 10 Then WD_cp2 = - 0.001 * Gaskonstante_Wasser * Temperatur_Reduziert^2 * (WD_g0tt_Bereich2m(Temperatur_Reduziert) + WD_grtt_Bereich2m(Druck_Reduziert , Temperatur_Reduziert))

End Function

Public Function WD_cv1(ByVal Druck As Double, ByVal Temperatur As Double) As Double     'Tabelle 3 - S.8
  
  Druck_Reduziert = 0.1 * Druck / 16.53
  Temperatur_Reduziert = 1386 / (Temperatur + 273.15)
  Dim gtt, gp, gpt, gpp As Double
  gtt = WD_gtt_Bereich1(Druck_Reduziert, Temperatur_Reduziert)
  gp = WD_gp_Bereich1(Druck_Reduziert, Temperatur_Reduziert)
  gpt = WD_gpt_Bereich1(Druck_Reduziert, Temperatur_Reduziert)
  gpp = WD_gpp_Bereich1(Druck_Reduziert, Temperatur_Reduziert)
  
  WD_cv1 = Gaskonstante_Wasser * (-(Temperatur_Reduziert ^ 2) * gtt + (gp - Temperatur_Reduziert * gpt) ^ 2 / gpp)

End Function

Public Function WD_cv2(ByVal Druck As Double, ByVal Temperatur As Double) As Double     'Tabelle 12 - S.15
  
  Druck_Reduziert = 0.1 * Druck
  Temperatur_Reduziert = 540 / (Temperatur + 273.15)

  'Stable Vapor Region
  'if Druck_Reduziert > 10 Then
  WD_cv2 = Gaskonstante_Wasser * ((-(Temperatur_Reduziert ^ 2) * (WD_g0tt_Bereich2(Temperatur_Reduziert) + WD_grtt_Bereich2(Druck_Reduziert, Temperatur_Reduziert))) - ((1 + Druck_Reduziert * WD_grp_Bereich2(Druck_Reduziert, Temperatur_Reduziert) - Temperatur_Reduziert * Druck_Reduziert * WD_grpt_Bereich2(Druck_Reduziert, Temperatur_Reduziert)) ^ 2 / (1 - Druck_Reduziert ^ 2 * WD_grpp_Bereich2(Druck_Reduziert, Temperatur_Reduziert))))

  'Metastable Vapor Region
  'If Druck_Reduziert < 10 Or Druck_Reduziert = 10 Then WD_cv2 = 0.001 * Gaskonstante_Wasser * ( (-(Temperatur_Reduziert^2) * ( WD_g0tt_Bereich2m(Temperatur_Reduziert) + WD_grtt_Bereich2m(Druck_Reduziert , Temperatur_Reduziert) ) ) - ( ( 1 + Druck_Reduziert * WD_grp_Bereich2m(Druck_Reduziert , Temperatur_Reduziert) - Temperatur_Reduziert * Druck_Reduziert * WD_grpt_Bereich2m(Druck_Reduziert , Temperatur_Reduziert))^2 / ( 1 - Druck_Reduziert^2 * WD_grpp_Bereich2m(Druck_Reduziert , Temperatur_Reduziert) ) ) )

End Function

Public Function WD_h1(ByVal Druck As Double, ByVal Temperatur As Double) As Double      'Tabelle 3 - S.8
  
  Druck_Reduziert = 0.1 * Druck / 16.53
  Temperatur_Reduziert = 1386 / (Temperatur + 273.15)

  WD_h1 = Gaskonstante_Wasser * (Temperatur + 273.15) * Temperatur_Reduziert * WD_gt_Bereich1(Druck_Reduziert, Temperatur_Reduziert)

End Function

Public Function WD_h2(ByVal Druck As Double, ByVal Temperatur As Double) As Double      'Tabelle 12 - S.15
  
  Druck_Reduziert = 0.1 * Druck
  Temperatur_Reduziert = 540 / (Temperatur + 273.15)

  'Stable Vapor Region
  'If Druck_Reduziert > 10    Then
  WD_h2 = Gaskonstante_Wasser * (Temperatur + 273.15) * Temperatur_Reduziert * (WD_g0t_Bereich2(Temperatur_Reduziert) + WD_grt_Bereich2(Druck_Reduziert, Temperatur_Reduziert))

  'Metastable Vapor Region
  'If Druck_Reduziert < 10 Or Druck_Reduziert = 10 Then WD_h2 = 0.001 * Gaskonstante_Wasser * (Temperatur + 273.15) * Temperatur_Reduziert * (WD_g0t_Bereich2m(Temperatur_Reduziert) + WD_grt_Bereich2m(Druck_Reduziert, Temperatur_Reduziert))

End Function

Public Function WD_s1(ByVal Druck As Double, ByVal Temperatur As Double) As Double      'Tabelle 3 - S.8
  
  Druck_Reduziert = 0.1 * Druck / 16.53
  Temperatur_Reduziert = 1386 / (Temperatur + 273.15)

  WD_s1 = Gaskonstante_Wasser * (Temperatur_Reduziert * WD_gt_Bereich1(Druck_Reduziert, Temperatur_Reduziert) - WD_g_Bereich1(Druck_Reduziert, Temperatur_Reduziert))

End Function

Public Function WD_s2(ByVal Druck As Double, ByVal Temperatur As Double) As Double      'Tabelle 12 - S.15
  
  Druck_Reduziert = 0.1 * Druck
  Temperatur_Reduziert = 540 / (Temperatur + 273.15)

  'Stable Vapor Region
  'If Druck_Reduziert > 10    Then
  WD_s2 = Gaskonstante_Wasser * (Temperatur_Reduziert * (WD_g0t_Bereich2(Temperatur_Reduziert) + WD_grt_Bereich2(Druck_Reduziert, Temperatur_Reduziert)) - (WD_g0_Bereich2(Druck_Reduziert, Temperatur_Reduziert) + WD_gr_Bereich2(Druck_Reduziert, Temperatur_Reduziert)))

  'Metastable Vapor Region
  'if Druck_Reduziert < 10 Or Druck_Reduziert = 10 Then WD_s2 = 0.001 * Gaskonstante_Wasser * (Temperatur_Reduziert * (WD_g0t_Bereich2m(Temperatur_Reduziert) + WD_grt_Bereich2m(Druck_Reduziert, Temperatur_Reduziert)) - (WD_g0_Bereich2(Druck_Reduziert, Temperatur_Reduziert) + WD_gr_Bereich2m(Druck_Reduziert, Temperatur_Reduziert)))

End Function

Public Function WD_v1(ByVal Druck As Double, ByVal Temperatur As Double) As Double  'Tabelle 3 - S.8

  Druck_Reduziert = 0.1 * Druck / 16.53
  Temperatur_Reduziert = 1386 / (Temperatur + 273.15)

  WD_v1 = Gaskonstante_Wasser * (Temperatur + 273.15) * Druck_Reduziert * WD_gp_Bereich1(Druck_Reduziert, Temperatur_Reduziert) / (Druck * 100)
  'Druck *100 bedeutet bar konvertiert zu kPa, weil Gaskonstante_Wasser ist in kJ/(kg * K)

End Function

Public Function WD_v2(ByVal Druck As Double, ByVal Temperatur As Double) As Double      'Tabelle 12 - S.15
  
  Druck_Reduziert = 0.1 * Druck
  Temperatur_Reduziert = 540 / (Temperatur + 273.15)

  'Stable Vapor Region
  'If Druck_Reduziert > 10    Then
  WD_v2 = Gaskonstante_Wasser * (Temperatur + 273.15) * Druck_Reduziert * (WD_g0p_Bereich2(Druck_Reduziert) + WD_grp_Bereich2(Druck_Reduziert, Temperatur_Reduziert)) / (Druck * 100)
  'Druck *100 bedeutet bar konvertiert zu kPa, weil Gaskonstante_Wasser ist in kJ/(kg * K)

  'Metastable Vapor Region
  'If Druck_Reduziert < 10 Or Druck_Reduziert = 10 Then WD_v2 = Gaskonstante_Wasser * (Temperatur + 273.15) * Druck_Reduziert * (WD_g0p_Bereich2(Druck_Reduziert) + WD_grp_Bereich2m(Druck_Reduziert, Temperatur_Reduziert)) / (Druck * 100000)

End Function

Public Function WD_w1(ByVal Druck As Double, ByVal Temperatur As Double) As Double      'Tabelle 3 - S.8
  'ACHTUNG: Funktion fehlerhaft, gibt flaschen Wert zurueck!!!
  
  Druck_Reduziert = 0.1 * Druck / 16.53
  Temperatur_Reduziert = 1386 / (Temperatur + 273.15)

  WD_w1 = ((Gaskonstante_Wasser * (Temperatur + 273.15) * WD_gp_Bereich1(Druck_Reduziert, Temperatur_Reduziert) ^ 2) / (((WD_gp_Bereich1(Druck_Reduziert, Temperatur_Reduziert) - Temperatur_Reduziert * WD_gpt_Bereich1(Druck_Reduziert, Temperatur_Reduziert)) ^ 2 / (Temperatur_Reduziert ^ 2 * WD_gtt_Bereich1(Druck_Reduziert, Temperatur_Reduziert))) - WD_gpp_Bereich1(Druck_Reduziert, Temperatur_Reduziert))) ^ (1 / 2)

End Function

Public Function WD_w2(ByVal Druck As Double, ByVal Temperatur As Double) As Double      'Tabelle 12 - S.15
  'ACHTUNG: Funktion fehlerhaft, gibt flaschen Wert zurück!!!
  
  Druck_Reduziert = 0.1 * Druck
  Temperatur_Reduziert = 540 / (Temperatur + 273.15)

  'Stable Vapor Region
  'if Druck_Reduziert > 10 Then
  WD_w2 = (Gaskonstante_Wasser * (Temperatur + 273.15) * ((1 + 2 * Druck_Reduziert * WD_grp_Bereich2(Druck_Reduziert, Temperatur_Reduziert) + Druck_Reduziert ^ 2 * WD_grp_Bereich2(Druck_Reduziert, Temperatur_Reduziert) ^ 2) / ((1 - Druck_Reduziert ^ 2 * WD_grpp_Bereich2(Druck_Reduziert, Temperatur_Reduziert)) + ((1 + Druck_Reduziert * WD_grp_Bereich2(Druck_Reduziert, Temperatur_Reduziert) - Temperatur_Reduziert * Druck_Reduziert * WD_grpt_Bereich2(Druck_Reduziert, Temperatur_Reduziert)) ^ 2 / (Temperatur_Reduziert ^ 2 * (WD_g0tt_Bereich2(Temperatur_Reduziert) + WD_grtt_Bereich2(Druck_Reduziert, Temperatur_Reduziert))))))) ^ (1 / 2)

  'Metastable Vapor Region
  'If Druck_Reduziert < 10 Or Druck_Reduziert = 10 Then WD_w2 = (0.001 * Gaskonstante_Wasser * (Temperatur + 273.15) * ((1 + 2 * Druck_Reduziert * WD_grp_Bereich2m(Druck_Reduziert , Temperatur_Reduziert) + Druck_Reduziert^2 * WD_grp_Bereich2m(Druck_Reduziert , Temperatur_Reduziert)^2)/((1- Druck_Reduziert^2 * WD_grpp_Bereich2m(Druck_Reduziert , Temperatur_Reduziert))+((1 + Druck_Reduziert * WD_grp_Bereich2m(Druck_Reduziert , Temperatur_Reduziert) - Temperatur_Reduziert * Druck_Reduziert * WD_grpt_Bereich2m(Druck_Reduziert , Temperatur_Reduziert))^2 /(Temperatur_Reduziert^2*(WD_g0tt_Bereich2m(Temperatur_Reduziert) + WD_grtt_Bereich2m(Druck_Reduziert , Temperatur_Reduziert)))))))^(1/2)
End Function

Public Function WD_u1(ByVal Druck As Double, ByVal Temperatur As Double) As Double    'Tabelle 3 - S.8
  
  Druck_Reduziert = 0.1 * Druck / 16.53
  Temperatur_Reduziert = 1386 / (Temperatur + 273.15)
  
  Dim gt, gp As Double
  gt = WD_gt_Bereich1(Druck_Reduziert, Temperatur_Reduziert)
  gp = WD_gp_Bereich1(Druck_Reduziert, Temperatur_Reduziert)
  
  WD_u1 = Gaskonstante_Wasser * (Temperatur + 273.15) * (Temperatur_Reduziert * gt - Druck_Reduziert * gp)
  
End Function

Public Function WD_u2(ByVal Druck As Double, ByVal Temperatur As Double) As Double    'Tabelle 3 - S.8
  
  Druck_Reduziert = 0.1 * Druck
  Temperatur_Reduziert = 540 / (Temperatur + 273.15)
  
  Dim g0t, g0p, grt, grp As Double
  g0t = WD_g0t_Bereich2(Temperatur_Reduziert)
  g0p = WD_g0p_Bereich2(Temperatur_Reduziert)
  grt = WD_grt_Bereich2(Druck_Reduziert, Temperatur_Reduziert)
  grp = WD_grp_Bereich2(Druck_Reduziert, Temperatur_Reduziert)
  
  WD_u2 = Gaskonstante_Wasser * (Temperatur + 273.15) * (Temperatur_Reduziert * (g0t + grt) - Druck_Reduziert * (g0p + grp))
  
End Function


'************* Rueckwaertsfunktionen in Bereich 1 und 2 *************************

Public Function WD_t_hp1(ByVal Enthalpie As Double, ByVal Druck As Double) As Double    'Gl. 11 - S.10
  Druck_Reduziert = 0.1 * Druck
  Enthalpie_Reduziert = Enthalpie / 2500
  
  Dim li_Bereich1BW_TPH, ji_Bereich1BW_TPH, ni_Bereich1BW_TPH

  li_Bereich1BW_TPH = Array_li_Bereich1BW_TPH()
  ji_Bereich1BW_TPH = Array_ji_Bereich1BW_TPH()
  ni_Bereich1BW_TPH = Array_ni_Bereich1BW_TPH()
  
  Dim i As Integer
  
  For i = 0 To 19
    WD_t_hp1 = WD_t_hp1 + (ni_Bereich1BW_TPH(i) * Druck_Reduziert ^ (li_Bereich1BW_TPH(i)) * (Enthalpie_Reduziert + 1) ^ (ji_Bereich1BW_TPH(i)))
  Next i

End Function

Public Function WD_t_hp2a(ByVal Enthalpie As Double, ByVal Druck As Double) As Double       'Gl. 22 - S.22

  

  Druck_Reduziert = 0.1 * Druck
  Enthalpie_Reduziert = Enthalpie / 2000
  
  Dim li_Bereich2BW_TPHa, ji_Bereich2BW_TPHa, ni_Bereich2BW_TPHa
  li_Bereich2BW_TPHa = Array_li_Bereich2BW_TPHa()
  ji_Bereich2BW_TPHa = Array_ji_Bereich2BW_TPHa()
  ni_Bereich2BW_TPHa = Array_ni_Bereich2BW_TPHa()
  
  Dim i As Integer
  
  For i = 0 To 33
    WD_t_hp2a = WD_t_hp2a + (ni_Bereich2BW_TPHa(i) * Druck_Reduziert ^ (li_Bereich2BW_TPHa(i)) * (Enthalpie_Reduziert - 2.1) ^ (ji_Bereich2BW_TPHa(i)))
  Next i

End Function

Public Function WD_t_hp2b(ByVal Enthalpie As Double, ByVal Druck As Double) As Double   'Gl. 23 - S.23

  Druck_Reduziert = 0.1 * Druck
  Enthalpie_Reduziert = Enthalpie / 2000

  Dim i As Integer
  Dim li_Bereich2BW_TPHb, ji_Bereich2BW_TPHb, ni_Bereich2BW_TPHb
  li_Bereich2BW_TPHb = Array_li_Bereich2BW_TPHb()
  ji_Bereich2BW_TPHb = Array_ji_Bereich2BW_TPHb()
  ni_Bereich2BW_TPHb = Array_ni_Bereich2BW_TPHb()

  For i = 0 To 37
    WD_t_hp2b = WD_t_hp2b + (ni_Bereich2BW_TPHb(i) * (Druck_Reduziert - 2#) ^ (li_Bereich2BW_TPHb(i)) * (Enthalpie_Reduziert - 2.6) ^ (ji_Bereich2BW_TPHb(i)))
  Next i

End Function

Public Function WD_t_hp2c(ByVal Enthalpie As Double, ByVal Druck As Double) As Double       'Gl. 24 - S.23

  

  Druck_Reduziert = 0.1 * Druck
  Enthalpie_Reduziert = Enthalpie / 2000

  Dim i As Integer
  Dim li_Bereich2BW_TPHc, ji_Bereich2BW_TPHc, ni_Bereich2BW_TPHc
  li_Bereich2BW_TPHc = Array_li_Bereich2BW_TPHc()
  ji_Bereich2BW_TPHc = Array_ji_Bereich2BW_TPHc()
  ni_Bereich2BW_TPHc = Array_ni_Bereich2BW_TPHc()

  For i = 0 To 22
    WD_t_hp2c = WD_t_hp2c + (ni_Bereich2BW_TPHc(i) * (Druck_Reduziert + 25#) ^ (li_Bereich2BW_TPHc(i)) * (Enthalpie_Reduziert - 1.8) ^ (ji_Bereich2BW_TPHc(i)))
  Next i

End Function

Public Function WD_t_sp1(ByVal Entropie As Double, ByVal Druck As Double) As Double    'Gl. 13 - S.11

  

  Druck_Reduziert = 0.1 * Druck
  Entropie_Reduziert = Entropie / 1

  Dim i As Integer
  Dim li_Bereich1BW_TPS, ji_Bereich1BW_TPS, ni_Bereich1BW_TPS
  li_Bereich1BW_TPS = Array_li_Bereich1BW_TPS()
  ji_Bereich1BW_TPS = Array_ji_Bereich1BW_TPS()
  ni_Bereich1BW_TPS = Array_ni_Bereich1BW_TPS()

  For i = 0 To 19

    WD_t_sp1 = WD_t_sp1 + (ni_Bereich1BW_TPS(i) * Druck_Reduziert ^ (li_Bereich1BW_TPS(i)) * (Entropie_Reduziert + 2#) ^ (ji_Bereich1BW_TPS(i)))

  Next i

End Function

Public Function WD_t_sp2a(ByVal Entropie As Double, ByVal Druck As Double) As Double        'Gl. 25 - S.25

  

  Druck_Reduziert = 0.1 * Druck
  Entropie_Reduziert = Entropie / 2

  Dim i As Integer
  Dim li_Bereich2BW_TPSa, ji_Bereich2BW_TPSa, ni_Bereich2BW_TPSa
  li_Bereich2BW_TPSa = Array_li_Bereich2BW_TPSa()
  ji_Bereich2BW_TPSa = Array_ji_Bereich2BW_TPSa()
  ni_Bereich2BW_TPSa = Array_ni_Bereich2BW_TPSa()

  For i = 0 To 45

    WD_t_sp2a = WD_t_sp2a + (ni_Bereich2BW_TPSa(i) * Druck_Reduziert ^ (li_Bereich2BW_TPSa(i)) * (Entropie_Reduziert - 2#) ^ (ji_Bereich2BW_TPSa(i)))

  Next i

End Function

Public Function WD_t_sp2b(ByVal Entropie As Double, ByVal Druck As Double) As Double        'Gl.26 - S.26

  

  Druck_Reduziert = 0.1 * Druck
  Entropie_Reduziert = Entropie / 0.7853

  Dim i As Integer
  Dim li_Bereich2BW_TPSb, ji_Bereich2BW_TPSb, ni_Bereich2BW_TPSb
  li_Bereich2BW_TPSb = Array_li_Bereich2BW_TPSb()
  ji_Bereich2BW_TPSb = Array_ji_Bereich2BW_TPSb()
  ni_Bereich2BW_TPSb = Array_ni_Bereich2BW_TPSb()

  For i = 0 To 43

    WD_t_sp2b = WD_t_sp2b + (ni_Bereich2BW_TPSb(i) * Druck_Reduziert ^ (li_Bereich2BW_TPSb(i)) * (10# - Entropie_Reduziert) ^ (ji_Bereich2BW_TPSb(i)))

  Next i

End Function

Public Function WD_t_sp2c(ByVal Entropie As Double, ByVal Druck As Double) As Double        'Gl. 27 - S.27

  

  Druck_Reduziert = 0.1 * Druck
  Entropie_Reduziert = Entropie / 2.9251

  Dim i As Integer
  Dim li_Bereich2BW_TPSc, ji_Bereich2BW_TPSc, ni_Bereich2BW_TPSc
  li_Bereich2BW_TPSc = Array_li_Bereich2BW_TPSc()
  ji_Bereich2BW_TPSc = Array_ji_Bereich2BW_TPSc()
  ni_Bereich2BW_TPSc = Array_ni_Bereich2BW_TPSc()

  For i = 0 To 29

    WD_t_sp2c = WD_t_sp2c + (ni_Bereich2BW_TPSc(i) * Druck_Reduziert ^ (li_Bereich2BW_TPSc(i)) * (2# - Entropie_Reduziert) ^ (ji_Bereich2BW_TPSc(i)))

  Next i

End Function

'************* Stoffwerteberechnung in Bereich 3 *************************

'Eingansgroesse Druck(delta) und Temperatur(tau) zu Dichte ueberfuehren fuer Berechnung der Stoffwerte in Bereich 3

Public Function WD_Dichte3(ByVal Druck As Double, ByVal Temperatur As Double) As Double
  Dim tau As Double
  tau = T_krit / (Temperatur + 273.15)

  Dim rho_min, rho_max As Double
  rho_min = 100
  rho_max = rho_krit * 4

  Dim rho_temp, p_temp, p_diff, delta_temp As Double

  Dim i As Integer
  
  For i = 1 To 1000

    rho_temp = (rho_min + rho_max) / 2

    delta_temp = rho_temp / rho_krit

    p_temp = rho_temp * Gaskonstante_Wasser * 1000 * (Temperatur + 273.15) * _
        delta_temp * WD_fd_Bereich3(tau, delta_temp) / 100000  'p_temp calculated in Pa, converted to bar

    p_diff = Abs(Druck - p_temp)

    If p_diff < 0.0000001 Then
      WD_Dichte3 = rho_temp
      Exit Function
    End If

    If p_temp > Druck Then
      rho_max = rho_temp
    Else
      rho_min = rho_temp
    End If

  Next i

  WD_Dichte3 = -1

End Function

Public Function WD_v3(ByVal Druck As Double, ByVal Temperatur As Double) As Double
  
  WD_v3 = 1 / WD_Dichte3(Druck, Temperatur)
  
End Function

'alphaP: relative Druckkoeffizient

Public Function WD_alphaP3(ByVal Druck As Double, ByVal Temperatur As Double) As Double
  Dim tau, delta, dichte As Double
  
  dichte = WD_Dichte3(Druck, Temperatur)

  tau = T_krit / (Temperatur + 273.15)
  delta = dichte / rho_krit
  
  Dim fd, fdt As Double
  fd = WD_fd_Bereich3(tau, delta)
  fdt = WD_fdt_Bereich3(tau, delta)
  
  WD_alphaP3 = 1 / (Temperatur + 273.15) * (1 - tau * fdt / fd)

End Function

'betaP: isotherme Stresskoeffizient, betaP = -(dp/dv)_T /p
Public Function WD_betaP3(ByVal Druck As Double, ByVal Temperatur As Double) As Double
  Dim tau, delta, dichte As Double

  dichte = WD_Dichte3(Druck, Temperatur)

  tau = T_krit / (Temperatur + 273.15)
  delta = dichte / rho_krit
  
  Dim fd2, fd As Double
  fd2 = WD_fd2_Bereich3(tau, delta)
  fd = WD_fd_Bereich3(tau, delta)
  
  WD_betaP3 = dichte * (2 + delta * fd2 / fd)
  
End Function


Public Function WD_p3(ByVal Druck As Double, ByVal Temperatur As Double) As Double      'Tabelle 31 - S.31

  Dim tau, delta, dichte As Double

  dichte = WD_Dichte3(Druck, Temperatur)

  tau = T_krit / (Temperatur + 273.15)
  delta = dichte / rho_krit

  'Druck in bar
  WD_p3 = 0.01 * Gaskonstante_Wasser * dichte * (Temperatur + 273.15) * (delta * WD_fd_Bereich3(tau, delta))

End Function

Public Function WD_u3(ByVal Druck As Double, ByVal Temperatur As Double) As Double      'Tabelle 31 - S.31

  Dim tau#, delta#, dichte#

  dichte = WD_Dichte3(Druck, Temperatur)

  tau = T_krit / (Temperatur + 273.15)
  delta = dichte / rho_krit

  WD_u3 = Gaskonstante_Wasser * (Temperatur + 273.15) * (tau * WD_ft_Bereich3(tau, delta))

End Function

Public Function WD_h3(ByVal Druck As Double, ByVal Temperatur As Double) As Double      'Tabelle 31 - S.31

  Dim tau#, delta#, dichte#

  dichte = WD_Dichte3(Druck, Temperatur)

  tau = T_krit / (Temperatur + 273.15)
  delta = dichte / rho_krit

  WD_h3 = Gaskonstante_Wasser * (Temperatur + 273.15) * (tau * WD_ft_Bereich3(tau, delta) + delta * WD_fd_Bereich3(tau, delta))

End Function

Public Function WD_s3(ByVal Druck As Double, ByVal Temperatur As Double) As Double      'Tabelle 31 - S.31

  Dim tau#, delta#, dichte#

  dichte = WD_Dichte3(Druck, Temperatur)

  tau = T_krit / (Temperatur + 273.15)
  delta = dichte / rho_krit


  'Stable Vapor Region
  'If Druck_Reduziert > 10    Then
  WD_s3 = Gaskonstante_Wasser * (tau * (WD_ft_Bereich3(tau, delta) - WD_phi_Bereich3(tau, delta)))

  'Metastable Vapor Region
  'if Druck_Reduziert < 10 Or Druck_Reduziert = 10 Then WD_s2 = 0.001 * Gaskonstante_Wasser * (Temperatur_Reduziert * (WD_g0t_Bereich2m(Temperatur_Reduziert) + WD_grt_Bereich2m(Druck_Reduziert, Temperatur_Reduziert)) - (WD_g0_Bereich2(Druck_Reduziert, Temperatur_Reduziert) + WD_gr_Bereich2m(Druck_Reduziert, Temperatur_Reduziert)))

End Function

Public Function WD_cv3(ByVal Druck As Double, ByVal Temperatur As Double) As Double      'Tabelle 31 - S.31

  Dim tau#, delta#, dichte#

  dichte = WD_Dichte3(Druck, Temperatur)

  tau = T_krit / (Temperatur + 273.15)
  delta = dichte / rho_krit

  WD_cv3 = -Gaskonstante_Wasser * (tau ^ 2 * WD_ft2_Bereich3(tau, delta))

End Function

Public Function WD_cp3(ByVal Druck As Double, ByVal Temperatur As Double) As Double      'Tabelle 31 - S.31

  Dim tau#, delta#, dichte#

  dichte = WD_Dichte3(Druck, Temperatur)

  tau = T_krit / (Temperatur + 273.15)
  delta = dichte / rho_krit

  WD_cp3 = Gaskonstante_Wasser * (-tau ^ 2 * WD_ft2_Bereich3(tau, delta) + _
    (delta * WD_fd_Bereich3(tau, delta) - delta * tau * WD_fdt_Bereich3(tau, delta)) ^ 2 / (2 * delta * WD_fd_Bereich3(tau, delta) + delta ^ 2 * WD_fd2_Bereich3(tau, delta)))

End Function

Public Function WD_w3(ByVal Druck As Double, ByVal Temperatur As Double) As Double      'Tabelle 31 - S. 31
  
  Dim tau#, delta#, dichte#

  dichte = WD_Dichte3(Druck, Temperatur)
  tau = T_krit / (Temperatur + 273.15)
  delta = dichte / rho_krit
  
  Dim fd, fd2, fdt, ftt As Double
  fd = WD_fd_Bereich3(tau, delta)
  fd2 = WD_fd2_Bereich3(tau, delta)
  fdt = WD_fdt_Bereich3(tau, delta)
  ftt = WD_ft2_Bereich3(tau, delta)

  WD_w3 = 2 * delta * fd
  WD_w3 = WD_w3 + delta ^ 2 * fd2
  WD_w3 = WD_w3 - (delta * fd - delta * tau * fdt) ^ 2 / (tau ^ 2 * ftt)
  WD_w3 = WD_w3 * Gaskonstante_Wasser * Temperatur
  WD_w3 = Sqr(WD_w3)

End Function


' Partielle Ableitungen zur Berechnung Stoffwerte
Public Function WD_gt_Bereich1(ByVal Druck_Reduziert As Double, ByVal Temperatur_Reduziert As Double) As Double
  'Tabelle 4 - S.8

  Dim i As Integer
  Dim li_Bereich1, ji_Bereich1, ni_Bereich1
  li_Bereich1 = Array_li_Bereich1()
  ji_Bereich1 = Array_ji_Bereich1()
  ni_Bereich1 = Array_ni_Bereich1()

  For i = 0 To 33
    WD_gt_Bereich1 = WD_gt_Bereich1 + (ni_Bereich1(i) * (7.1 - Druck_Reduziert) ^ (li_Bereich1(i)) * ji_Bereich1(i) * (Temperatur_Reduziert - 1.222) ^ (ji_Bereich1(i) - 1#))
  Next i


  
End Function

Public Function WD_g0t_Bereich2(ByVal Temperatur_Reduziert As Double) As Double
  'Tabelle 13 - S.16
  

  Dim i As Integer

  WD_g0t_Bereich2 = 0#
  
  Dim ji0_Bereich2, ni0_Bereich2
  ji0_Bereich2 = Array_ji0_Bereich2()
  ni0_Bereich2 = Array_ni0_Bereich2()

  For i = 0 To 8
    WD_g0t_Bereich2 = WD_g0t_Bereich2 + (ni0_Bereich2(i) * ji0_Bereich2(i) * Temperatur_Reduziert ^ (ji0_Bereich2(i) - 1))
  Next i

  
End Function

Public Function WD_g0t_Bereich2m(ByVal Temperatur_Reduziert As Double) As Double
  'Tabelle 13 - S.16
  

  WD_g0t_Bereich2m = 0#

  Dim i As Integer
  Dim ji0_Bereich2, ni0_Bereich2m
  
  ji0_Bereich2 = Array_ji0_Bereich2()
  ni0_Bereich2m = Array_ni0_Bereich2m()

  For i = 0 To 8
    WD_g0t_Bereich2m = WD_g0t_Bereich2m + (ni0_Bereich2m(i) * ji0_Bereich2(i) * Temperatur_Reduziert ^ (ji0_Bereich2(i) - 1))
  Next i

End Function

Public Function WD_grpt_Bereich2(ByVal Druck_Reduziert As Double, ByVal Temperatur_Reduziert As Double) As Double
  'Tabelle 14 - S.16
  

  WD_grpt_Bereich2 = 0#

  Dim i As Integer
  Dim li_Bereich2, ji_Bereich2, ni_Bereich2
  li_Bereich2 = Array_li_Bereich2()
  ji_Bereich2 = Array_ji_Bereich2()
  ni_Bereich2 = Array_ni_Bereich2()

  For i = 0 To 42
    WD_grpt_Bereich2 = WD_grpt_Bereich2 + (ni_Bereich2(i) * Druck_Reduziert ^ (li_Bereich2(i) - 1) * li_Bereich2(i) * ji_Bereich2(i) * (Temperatur_Reduziert - 0.5) ^ (ji_Bereich2(i) - 1))
  Next i

End Function

Public Function WD_grpt_Bereich2m(ByVal Druck_Reduziert As Double, ByVal Temperatur_Reduziert As Double) As Double
  'Tabelle 17 - S.19
  

  WD_grpt_Bereich2m = 0#

  Dim i As Integer
  Dim li_Bereich2m, ji_Bereich2m, ni_Bereich2m
  li_Bereich2m = Array_li_Bereich2m()
  ji_Bereich2m = Array_ji_Bereich2m()
  ni_Bereich2m = Array_ni_Bereich2m()

  For i = 0 To 12
    WD_grpt_Bereich2m = WD_grpt_Bereich2m + (ni_Bereich2m(i) * Druck_Reduziert ^ (li_Bereich2m(i) - 1) * li_Bereich2m(i) * ji_Bereich2m(i) * (Temperatur_Reduziert - 0.5) ^ (ji_Bereich2m(i) - 1))
  Next i

End Function

Public Function WD_grtt_Bereich2(ByVal Druck_Reduziert As Double, ByVal Temperatur_Reduziert As Double) As Double
  'Tabelle 14 - S.16
  

  WD_grtt_Bereich2 = 0#

  Dim i As Integer
  Dim li_Bereich2, ji_Bereich2, ni_Bereich2
  li_Bereich2 = Array_li_Bereich2()
  ji_Bereich2 = Array_ji_Bereich2()
  ni_Bereich2 = Array_ni_Bereich2()

  For i = 0 To 42
    WD_grtt_Bereich2 = WD_grtt_Bereich2 + (ni_Bereich2(i) * Druck_Reduziert ^ (li_Bereich2(i)) * ji_Bereich2(i) * (ji_Bereich2(i) - 1#) * (Temperatur_Reduziert - 0.5) ^ (ji_Bereich2(i) - 2#))
  Next i

End Function

Public Function WD_grtt_Bereich2m(ByVal Druck_Reduziert As Double, ByVal Temperatur_Reduziert As Double) As Double
  'Tabelle 17 - S.19
  

  WD_grtt_Bereich2m = 0#

  Dim i As Integer
  Dim li_Bereich2m, ji_Bereich2m, ni_Bereich2m
  li_Bereich2m = Array_li_Bereich2m()
  ji_Bereich2m = Array_ji_Bereich2m()
  ni_Bereich2m = Array_ni_Bereich2m()

  For i = 0 To 12
    WD_grtt_Bereich2m = WD_grtt_Bereich2m + (ni_Bereich2m(i) * Druck_Reduziert ^ (li_Bereich2m(i)) * ji_Bereich2m(i) * (ji_Bereich2m(i) - 1#) * (Temperatur_Reduziert - 0.5) ^ (ji_Bereich2m(i) - 2#))
  Next i

End Function

Public Function WD_grt_Bereich2(ByVal Druck_Reduziert As Double, ByVal Temperatur_Reduziert As Double) As Double
  'Tabelle 14 - S.16
  

  WD_grt_Bereich2 = 0#

  Dim i As Integer
  Dim li_Bereich2, ji_Bereich2, ni_Bereich2
  li_Bereich2 = Array_li_Bereich2()
  ji_Bereich2 = Array_ji_Bereich2()
  ni_Bereich2 = Array_ni_Bereich2()

  For i = 0 To 42
    WD_grt_Bereich2 = WD_grt_Bereich2 + (ni_Bereich2(i) * Druck_Reduziert ^ (li_Bereich2(i)) * ji_Bereich2(i) * (Temperatur_Reduziert - 0.5) ^ (ji_Bereich2(i) - 1))
  Next i

End Function

Public Function WD_grt_Bereich2m(ByVal Druck_Reduziert As Double, ByVal Temperatur_Reduziert As Double) As Double
  'Tabelle 17 - S.19
  

  WD_grt_Bereich2m = 0#

  Dim i As Integer
  Dim li_Bereich2m, ji_Bereich2m, ni_Bereich2m
  li_Bereich2m = Array_li_Bereich2m()
  ji_Bereich2m = Array_ji_Bereich2m()
  ni_Bereich2m = Array_ni_Bereich2m()

  For i = 0 To 12
    WD_grt_Bereich2m = WD_grt_Bereich2m + (ni_Bereich2m(i) * Druck_Reduziert ^ (li_Bereich2m(i)) * ji_Bereich2m(i) * (Temperatur_Reduziert - 0.5) ^ (ji_Bereich2m(i) - 1))
  Next i

End Function

Public Function WD_g_Bereich1(ByVal Druck_Reduziert As Double, ByVal Temperatur_Reduziert As Double) As Double
  'Tabelle 4 - S.8

  Dim i As Integer
  Dim li_Bereich1, ji_Bereich1, ni_Bereich1
  li_Bereich1 = Array_li_Bereich1()
  ji_Bereich1 = Array_ji_Bereich1()
  ni_Bereich1 = Array_ni_Bereich1()

  For i = 0 To 33
    WD_g_Bereich1 = WD_g_Bereich1 + (ni_Bereich1(i) * (7.1 - Druck_Reduziert) ^ (li_Bereich1(i)) * (Temperatur_Reduziert - 1.222) ^ ji_Bereich1(i))
  Next i

End Function

Public Function WD_g0_Bereich2(ByVal Druck_Reduziert As Double, ByVal Temperatur_Reduziert As Double) As Double
  'Tabelle 13 - S.16
  

  WD_g0_Bereich2 = Log(Druck_Reduziert) 'natuerlicher Logarithmus zur Basis e - ln(...)

  Dim i As Integer
  Dim ji0_Bereich2, ni0_Bereich2
  ji0_Bereich2 = Array_ji0_Bereich2
  ni0_Bereich2 = Array_ni0_Bereich2

  For i = 0 To 8
    WD_g0_Bereich2 = WD_g0_Bereich2 + (ni0_Bereich2(i) * Temperatur_Reduziert ^ (ji0_Bereich2(i)))
  Next i

End Function

Public Function WD_g0_Bereich2m(ByVal Druck_Reduziert As Double, ByVal Temperatur_Reduziert As Double) As Double
  'Tabelle 13 - S.16
  

  WD_g0_Bereich2m = Log(Druck_Reduziert) 'natuerlicher Logarithmus zur Basis e - ln(...)

  Dim i As Integer
  Dim ji0_Bereich2, ni0_Beriech2m
  ji0_Bereich2 = Array_ji0_Bereich2
  ni0_Bereich2m = Array_ni0_Bereich2m

  For i = 0 To 8
    WD_g0_Bereich2m = WD_g0_Bereich2m + (ni0_Bereich2m(i) * Temperatur_Reduziert ^ (ji0_Bereich2(i)))
  Next i

End Function

Public Function WD_gr_Bereich2(ByVal Druck_Reduziert As Double, ByVal Temperatur_Reduziert As Double) As Double
  'Tabelle 14 - S.16
  

  Dim i As Integer
  Dim li_Bereich2, ji_Bereich2, ni_Bereich2
  li_Bereich2 = Array_li_Bereich2()
  ji_Bereich2 = Array_ji_Bereich2()
  ni_Bereich2 = Array_ni_Bereich2()

  For i = 0 To 42
    WD_gr_Bereich2 = WD_gr_Bereich2 + (ni_Bereich2(i) * Druck_Reduziert ^ (li_Bereich2(i)) * (Temperatur_Reduziert - 0.5) ^ (ji_Bereich2(i)))
  Next i

End Function

Public Function WD_gr_Bereich2m(ByVal Druck_Reduziert As Double, ByVal Temperatur_Reduziert As Double) As Double
  'Tabelle 17 - S.19
  

  Dim i As Integer
  Dim li_Bereich2m, ji_Bereich2m, ni_Bereich2m
  li_Bereich2m = Array_li_Bereich2m()
  ji_Bereich2m = Array_ji_Bereich2m()
  ni_Bereich2m = Array_ni_Bereich2m()

  For i = 0 To 12
    WD_gr_Bereich2m = WD_gr_Bereich2m + (ni_Bereich2m(i) * Druck_Reduziert ^ (li_Bereich2m(i)) * (Temperatur_Reduziert - 0.5) ^ (ji_Bereich2m(i)))
  Next i

End Function

Public Function WD_gp_Bereich1(ByVal Druck_Reduziert As Double, ByVal Temperatur_Reduziert As Double) As Double
  'Tabelle 4 - S.8
  

  Dim i As Integer
  Dim li_Bereich1, ji_Bereich1, ni_Bereich1
  li_Bereich1 = Array_li_Bereich1()
  ji_Bereich1 = Array_ji_Bereich1()
  ni_Bereich1 = Array_ni_Bereich1()

  For i = 0 To 33
    WD_gp_Bereich1 = WD_gp_Bereich1 + ((-ni_Bereich1(i) * li_Bereich1(i)) * (7.1 - Druck_Reduziert) ^ (li_Bereich1(i) - 1) * (Temperatur_Reduziert - 1.222) ^ ji_Bereich1(i))
  Next i

End Function

Public Function WD_gtt_Bereich1(ByVal Druck_Reduziert As Double, ByVal Temperatur_Reduziert As Double) As Double
  'Tabelle 4 - S.8
  

  Dim i As Integer
  Dim li_Bereich1, ji_Bereich1, ni_Bereich1
  li_Bereich1 = Array_li_Bereich1()
  ji_Bereich1 = Array_ji_Bereich1()
  ni_Bereich1 = Array_ni_Bereich1()

  For i = 0 To 33
    WD_gtt_Bereich1 = WD_gtt_Bereich1 + (ni_Bereich1(i) * (7.1 - Druck_Reduziert) ^ (li_Bereich1(i)) * ji_Bereich1(i) * (ji_Bereich1(i) - 1#) * (Temperatur_Reduziert - 1.222) ^ (ji_Bereich1(i) - 2))
  Next i

End Function

Public Function WD_gpp_Bereich1(ByVal Druck_Reduziert As Double, ByVal Temperatur_Reduziert As Double) As Double
  'Tabelle 4 - S.8
  

  Dim i As Integer
  Dim li_Bereich1, ji_Bereich1, ni_Bereich1
  li_Bereich1 = Array_li_Bereich1()
  ji_Bereich1 = Array_ji_Bereich1()
  ni_Bereich1 = Array_ni_Bereich1()

  For i = 0 To 33
    WD_gpp_Bereich1 = WD_gpp_Bereich1 + ((ni_Bereich1(i) * li_Bereich1(i)) * (li_Bereich1(i) - 1#) * (7.1 - Druck_Reduziert) ^ (li_Bereich1(i) - 2) * (Temperatur_Reduziert - 1.222) ^ (ji_Bereich1(i)))
  Next i

End Function

Public Function WD_gpt_Bereich1(ByVal Druck_Reduziert As Double, ByVal Temperatur_Reduziert As Double) As Double
  'Tabelle 4 - S.8
  

  Dim i As Integer
  Dim li_Bereich1, ji_Bereich1, ni_Bereich1
  li_Bereich1 = Array_li_Bereich1()
  ji_Bereich1 = Array_ji_Bereich1()
  ni_Bereich1 = Array_ni_Bereich1()

  For i = 0 To 33
    WD_gpt_Bereich1 = WD_gpt_Bereich1 + ((-ni_Bereich1(i) * li_Bereich1(i)) * (7.1 - Druck_Reduziert) ^ (li_Bereich1(i) - 1) * ji_Bereich1(i) * (Temperatur_Reduziert - 1.222) ^ (ji_Bereich1(i) - 1))
  Next i

End Function

Public Function WD_g0tt_Bereich2(ByVal Temperatur_Reduziert As Double) As Double
  'Tabelle 13 - S.16
  

  WD_g0tt_Bereich2 = 0#

  Dim i As Integer
  Dim ji0_Bereich2, ni0_Bereich2
  ji0_Bereich2 = Array_ji0_Bereich2()
  ni0_Bereich2 = Array_ni0_Bereich2()

  For i = 0 To 8
    WD_g0tt_Bereich2 = WD_g0tt_Bereich2 + (ni0_Bereich2(i) * ji0_Bereich2(i) * (ji0_Bereich2(i) - 1#) * Temperatur_Reduziert ^ (ji0_Bereich2(i) - 2#))
  Next i

End Function

Public Function WD_g0tt_Bereich2m(ByVal Temperatur_Reduziert As Double) As Double
  'Tabelle 13 - S.16
  

  WD_g0tt_Bereich2m = 0#

  Dim i As Integer
  Dim ji0_Bereich2, ni0_Bereich2m
  ji0_Bereich2 = Array_ji0_Bereich2()
  ni0_Bereich2m = Array_ni0_Bereich2m()

  For i = 0 To 8
    WD_g0tt_Bereich2m = WD_g0tt_Bereich2m + (ni0_Bereich2m(i) * ji0_Bereich2(i) * (ji0_Bereich2(i) - 1#) * Temperatur_Reduziert ^ (ji0_Bereich2(i) - 2#))
  Next i

End Function

Public Function WD_g0pp_Bereich2(ByVal Druck_Reduziert As Double) As Double
  'Tabelle 13 - S.16
  

  WD_g0pp_Bereich2 = -1 / Druck_Reduziert ^ 2

End Function

Public Function WD_g0p_Bereich2(ByVal Druck_Reduziert As Double) As Double
  'Tabelle 13 - S.16
  

  WD_g0p_Bereich2 = 1 / Druck_Reduziert

End Function

Public Function WD_grpp_Bereich2(ByVal Druck_Reduziert As Double, ByVal Temperatur_Reduziert As Double) As Double
  'Tabelle 14 - S.16

  WD_grpp_Bereich2 = 0#

  Dim i As Integer
  Dim li_Bereich2, ji_Bereich2, ni_Bereich2
  li_Bereich2 = Array_li_Bereich2()
  ji_Bereich2 = Array_ji_Bereich2()
  ni_Bereich2 = Array_ni_Bereich2()

  For i = 0 To 42
    WD_grpp_Bereich2 = WD_grpp_Bereich2 + (ni_Bereich2(i) * li_Bereich2(i) * (li_Bereich2(i) - 1) * Druck_Reduziert ^ (li_Bereich2(i) - 2) * (Temperatur_Reduziert - 0.5) ^ (ji_Bereich2(i)))
  Next i

End Function

Public Function WD_grpp_Bereich2m(ByVal Druck_Reduziert As Double, ByVal Temperatur_Reduziert As Double) As Double
  'Tabelle 17 - S.19
  
  WD_grpp_Bereich2m = 0#

  Dim i As Integer
  Dim li_Bereich2m, ji_Bereich2m, ni_Bereich2m
  li_Bereich2m = Array_li_Bereich2m()
  ji_Bereich2m = Array_ji_Bereich2m()
  ni_Bereich2m = Array_ni_Bereich2m()

  For i = 0 To 12
    WD_grpp_Bereich2m = WD_grpp_Bereich2m + (ni_Bereich2m(i) * li_Bereich2m(i) * (li_Bereich2m(i) - 1) * Druck_Reduziert ^ (li_Bereich2m(i) - 2) * (Temperatur_Reduziert - 0.5) ^ (ji_Bereich2m(i)))
  Next i

End Function

Public Function WD_grp_Bereich2(ByVal Druck_Reduziert As Double, ByVal Temperatur_Reduziert As Double) As Double
  'Tabelle 14 - S.16

  WD_grp_Bereich2 = 0#

  Dim i As Integer
  Dim li_Bereich2, ji_Bereich2, ni_Bereich2
  li_Bereich2 = Array_li_Bereich2()
  ji_Bereich2 = Array_ji_Bereich2()
  ni_Bereich2 = Array_ni_Bereich2()

  For i = 0 To 42
    WD_grp_Bereich2 = WD_grp_Bereich2 + (ni_Bereich2(i) * li_Bereich2(i) * Druck_Reduziert ^ (li_Bereich2(i) - 1) * (Temperatur_Reduziert - 0.5) ^ (ji_Bereich2(i)))
  Next i

End Function

Public Function WD_grp_Bereich2m(ByVal Druck_Reduziert As Double, ByVal Temperatur_Reduziert As Double) As Double
  'Tabelle 17 - S.19
  

  WD_grp_Bereich2m = 0#

  Dim i As Integer
  Dim li_Bereich2m, ji_Bereich2m, ni_Bereich2m
  li_Bereich2m = Array_li_Bereich2m()
  ji_Bereich2m = Array_ji_Bereich2m()
  ni_Bereich2m = Array_ni_Bereich2m()

  For i = 0 To 12
    WD_grp_Bereich2m = WD_grp_Bereich2m + (ni_Bereich2m(i) * li_Bereich2m(i) * Druck_Reduziert ^ (li_Bereich2m(i) - 1) * (Temperatur_Reduziert - 0.5) ^ (ji_Bereich2m(i)))
  Next i

End Function

Public Function WD_phi_Bereich3(ByVal tau As Double, ByVal delta As Double) As Double
  'Tabelle 32 - S.32
  

  Dim i As Integer
  Dim li_Bereich3, ji_Bereich3, ni_Bereich3
  li_Bereich3 = Array_li_Bereich3()
  ji_Bereich3 = Array_ji_Bereich3()
  ni_Bereich3 = Array_ni_Bereich3()

  WD_phi_Bereich3 = ni_Bereich3(0) * Log(delta)

  For i = 1 To 39
    WD_phi_Bereich3 = WD_phi_Bereich3 + (ni_Bereich3(i) * (delta ^ li_Bereich3(i)) * (tau ^ ji_Bereich3(i)))
  Next i

End Function

Public Function WD_fd_Bereich3(ByVal tau As Double, ByVal delta As Double) As Double

  'Tabelle 32 - S.32
  


  Dim i As Integer
  Dim li_Bereich3, ji_Bereich3, ni_Bereich3

  li_Bereich3 = Array_li_Bereich3()
  ji_Bereich3 = Array_ji_Bereich3()
  ni_Bereich3 = Array_ni_Bereich3()

  WD_fd_Bereich3 = ni_Bereich3(0) / delta

  For i = 1 To 39
    WD_fd_Bereich3 = WD_fd_Bereich3 + _
      (ni_Bereich3(i) * li_Bereich3(i) * delta ^ (li_Bereich3(i) - 1) * tau ^ ji_Bereich3(i))
  Next i
  
End Function


Public Function WD_fd2_Bereich3(ByVal tau As Double, ByVal delta As Double) As Double

  'Tabelle 32 - S.32
  


  Dim i As Integer
  Dim li_Bereich3, ji_Bereich3, ni_Bereich3
  li_Bereich3 = Array_li_Bereich3()
  ji_Bereich3 = Array_ji_Bereich3()
  ni_Bereich3 = Array_ni_Bereich3()

  WD_fd2_Bereich3 = -ni_Bereich3(0) / delta ^ 2

  For i = 1 To 39
    WD_fd2_Bereich3 = WD_fd2_Bereich3 + _
      (ni_Bereich3(i) * li_Bereich3(i) * (li_Bereich3(i) - 1) * delta ^ (li_Bereich3(i) - 2) * tau ^ ji_Bereich3(i))
  Next i

End Function

Public Function WD_ft_Bereich3(ByVal tau As Double, ByVal delta As Double) As Double

  'Tabelle 32 - S.32
  


  Dim i As Integer
  Dim li_Bereich3, ji_Bereich3, ni_Bereich3
  li_Bereich3 = Array_li_Bereich3()
  ji_Bereich3 = Array_ji_Bereich3()
  ni_Bereich3 = Array_ni_Bereich3()

  WD_ft_Bereich3 = 0

  For i = 1 To 39
    WD_ft_Bereich3 = WD_ft_Bereich3 + (ni_Bereich3(i) * delta ^ li_Bereich3(i) * ji_Bereich3(i) * tau ^ (ji_Bereich3(i) - 1))
  Next i

End Function

Public Function WD_ft2_Bereich3(ByVal tau As Double, ByVal delta As Double) As Double

  'Tabelle 32 - S.32
  


  Dim i As Integer
  Dim li_Bereich3, ji_Bereich3, ni_Bereich3
  li_Bereich3 = Array_li_Bereich3()
  ji_Bereich3 = Array_ji_Bereich3()
  ni_Bereich3 = Array_ni_Bereich3()

  WD_ft2_Bereich3 = 0

  For i = 1 To 39
    WD_ft2_Bereich3 = WD_ft2_Bereich3 + _
      (ni_Bereich3(i) * delta ^ li_Bereich3(i) * ji_Bereich3(i) * (ji_Bereich3(i) - 1) * tau ^ (ji_Bereich3(i) - 2))
  Next i

End Function

Public Function WD_fdt_Bereich3(ByVal tau As Double, ByVal delta As Double) As Double

  'Tabelle 32 - S.32
  


  Dim i As Integer
  Dim li_Bereich3, ji_Bereich3, ni_Bereich3
  li_Bereich3 = Array_li_Bereich3()
  ji_Bereich3 = Array_ji_Bereich3()
  ni_Bereich3 = Array_ni_Bereich3()

  'WD_fdt_Bereich3 = 0

  For i = 1 To 39
    WD_fdt_Bereich3 = WD_fdt_Bereich3 + _
      (ni_Bereich3(i) * li_Bereich3(i) * delta ^ (li_Bereich3(i) - 1) * ji_Bereich3(i) * tau ^ (ji_Bereich3(i) - 1))
  Next i

End Function    'Ende der partiellen Ableitungen von phi-Funktionen

' Theoretische Grundlage unsicher!
Public Function WD_pviskositaet(ByVal Temperatur_Reduziert As Double, ByVal delta_temp As Double) As Double

  Dim psi0 As Double
  Dim psi1 As Double

  psi0 = _
  n0visc_1 * Temperatur_Reduziert ^ 1 + _
  n0visc_2 * Temperatur_Reduziert ^ 2 + _
  n0visc_3 * Temperatur_Reduziert ^ 3
  psi0 = 1 / (Temperatur_Reduziert ^ 0.5 * psi0)

  psi1 = _
    nvisc_1 * (delta_temp - 1) ^ ivisc_1 * (Temperatur_Reduziert - 1) ^ jvisc_1 + _
    nvisc_2 * (delta_temp - 1) ^ ivisc_2 * (Temperatur_Reduziert - 1) ^ jvisc_2 + _
    nvisc_3 * (delta_temp - 1) ^ ivisc_3 * (Temperatur_Reduziert - 1) ^ jvisc_3 + _
    nvisc_4 * (delta_temp - 1) ^ ivisc_4 * (Temperatur_Reduziert - 1) ^ jvisc_4 + _
    nvisc_5 * (delta_temp - 1) ^ ivisc_5 * (Temperatur_Reduziert - 1) ^ jvisc_5 + _
    nvisc_6 * (delta_temp - 1) ^ ivisc_6 * (Temperatur_Reduziert - 1) ^ jvisc_6 + _
    nvisc_7 * (delta_temp - 1) ^ ivisc_7 * (Temperatur_Reduziert - 1) ^ jvisc_7 + _
    nvisc_8 * (delta_temp - 1) ^ ivisc_8 * (Temperatur_Reduziert - 1) ^ jvisc_8 + _
    nvisc_9 * (delta_temp - 1) ^ ivisc_9 * (Temperatur_Reduziert - 1) ^ jvisc_9

  psi1 = psi1 + _
    nvisc_10 * (delta_temp - 1) ^ ivisc_10 * (Temperatur_Reduziert - 1) ^ jvisc_10 + _
    nvisc_11 * (delta_temp - 1) ^ ivisc_11 * (Temperatur_Reduziert - 1) ^ jvisc_11 + _
    nvisc_12 * (delta_temp - 1) ^ ivisc_12 * (Temperatur_Reduziert - 1) ^ jvisc_12 + _
    nvisc_13 * (delta_temp - 1) ^ ivisc_13 * (Temperatur_Reduziert - 1) ^ jvisc_13 + _
    nvisc_14 * (delta_temp - 1) ^ ivisc_14 * (Temperatur_Reduziert - 1) ^ jvisc_14 + _
    nvisc_15 * (delta_temp - 1) ^ ivisc_15 * (Temperatur_Reduziert - 1) ^ jvisc_15 + _
    nvisc_16 * (delta_temp - 1) ^ ivisc_16 * (Temperatur_Reduziert - 1) ^ jvisc_16 + _
    nvisc_17 * (delta_temp - 1) ^ ivisc_17 * (Temperatur_Reduziert - 1) ^ jvisc_17 + _
    nvisc_18 * (delta_temp - 1) ^ ivisc_18 * (Temperatur_Reduziert - 1) ^ jvisc_18 + _
    nvisc_19 * (delta_temp - 1) ^ ivisc_19 * (Temperatur_Reduziert - 1) ^ jvisc_19

  psi1 = Exp(delta_temp * psi1)
  WD_pviskositaet = psi0 * psi1

End Function

' Quelle: IAPWS 2008 for the Viscosity of Ordinary Water Substance
' Gl. 10 bis 12, Seite 4 und 5 ohne critical enhancement
Public Function WD_dvisko_DT(ByVal dichte As Double, ByVal Temperatur As Double) As Double
  
  Temperatur_Reduziert = (Temperatur + 273.15) / T_krit
  
  Dichte_Reduziert = dichte / rho_krit
  
  Dim nu0, nu1, summe0, summe1 As Double
  
  Dim H_null
  H_null = Array_H_null
  
  Dim i As Integer
  For i = 0 To 3
    summe0 = summe0 + H_null(i) / (Temperatur_Reduziert ^ i)
  Next i

  nu0 = 100 * Sqr(Temperatur_Reduziert) / summe0

  Dim H_1
  H_1 = Array_H_1
  
  Dim i2, j As Integer
  For i2 = 0 To 5
    For j = 0 To 6
      summe1 = summe1 + (1 / Temperatur_Reduziert - 1) ^ i2 * H_1(i2, j) * (Dichte_Reduziert - 1) ^ j
    Next j
  Next i2

  nu1 = Exp(Dichte_Reduziert * summe1)
  
  WD_dvisko_DT = nu0 * nu1 * 10 ^ 6 * My_ref  'Einheit: microPa * s

End Function

'Quelle: IAPWS 2011 for the thermal conductivity of ordinary water substance
Public Function WD_lambda(ByVal T_reduziert, ByVal rho_reduziert) As Double
  Dim lambda0, lambda1, lambda2 As Double
  Dim summe0, summe1 As Double
  Dim i, j, k As Integer
  Dim L_null, L_1, A_2
  L_null() = Array_L_null()

  For k = 0 To 4
    summe0 = summe0 + L_null(k) / T_reduziert ^ k
  Next
  lambda0 = Sqrt(T_reduziert) / summe0

  L_1 = Array_L_1()
  For i = 0 To 4
    For j = 0 To 5
      summe1 = summe1 + (1 / T_reduziert - 1) ^ i * L_1(i, j) * (rho_reduziert - 1) ^ j
    Next
  Next
  lambda1 = Exp(rho_reduziert * summe1)

  WD_lambda = ((lambda0 * lambda1) + lambda2) * 0.001 * 1000      'Einheit: mW * m^-1 * K^-1
End Function

Public Function WD_WaermeLF_DT(ByVal Density As Double, ByVal Temperatur As Double)
  Dim lambda0, lambda1, lambda2 As Double
  Dim delta, theta As Double
  Dim n As Variant
  Dim i As Integer
  Dim nLambda_null, nLambda_1, nLambda_2
  
  nLambda_null = Array_nLambda_null()
  nLambda_1 = Array_nLambda_1()
  nLambda_2 = Array_nLambda_2()
  
  delta = Density / rho_krit_L
  theta = (Temperatur + 273.15) / T_krit_L
  
  i = 1
  For Each n In nLambda_null
    lambda0 = lambda0 + n * theta ^ (i - 1)
    i = i + 1
  Next n
  
  lambda0 = lambda0 * theta ^ 0.5
  
  lambda1 = nLambda_1(0) + nLambda_1(1) * delta + nLambda_1(2) * Exp(nLambda_1(3) * (delta + nLambda_1(4)) ^ 2)
  
  Dim a, b, delta_diff As Double
  
  delta_diff = Abs(theta - 1) + nLambda_2(9)
  a = 2 + nLambda_2(7) * delta_diff ^ -0.6
  b = IIf(theta >= 1, delta_diff ^ -1, nLambda_2(8) * delta_diff ^ -0.6)
  
  lambda2 = (nLambda_2(0) * theta ^ -10 + nLambda_2(1)) * (delta ^ 1.8) * Exp(nLambda_2(2) * (1 - delta ^ 2.8)) + _
        nLambda_2(3) * a * (delta ^ b) * Exp((b / (1 + b)) * (1 - delta ^ (1 + b))) + _
        nLambda_2(4) * Exp(nLambda_2(5) * (theta ^ 1.5) + nLambda_2(6) * delta ^ (-5))
  
  WD_WaermeLF_DT = lambda0 + lambda1 + lambda2

End Function
