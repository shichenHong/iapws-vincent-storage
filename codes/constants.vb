'Allgemeine


Public Const Genauigkeit_Entropie As Double = 0.0001
Public Const Genauigkeit_Enthalpie As Double = 0.0001
'molares Normvolumen
Public Const Normvolumen As Double = 22.414 '[m?kmol]
'Normdruck
Public Const Normdruck As Double = 101325 '[Pa]
Public Const Normdruck_bar As Double = 1.01325 '[bar]
'Normtemperatur
Public Const Normtemperatur As Double = 273.15 '[K]
'Verdampfungsenthalpie Wasser bei 0 grad
Public Const Verdampfungsenthalpie As Double = 2500 'kJ/kg
'Iterationstoleranz
Public Const Abweichung As Double = 10 ^ -6
'Druck Tripelpunkt
Public Const p_tp = 0.00611657

'molare Gaskonstante

Public Const molareGaskonstante_Wasser As Double = 0.00831441 '[kJ/(mol*K)]
Public Const Gaskonstante_Wasser  As Double = 0.461526 '[kJ/(kg*K)]
Public Const Gaskonstante_Wasser_2 As Double = 0.46151805 '[kJ/(kg*K)]  Diese Variante nur fuer Berechnung Waermeleitfaehigkeit

'Kritische Parameter
Public Const T_krit As Double = 647.096 '[K]
Public Const p_krit As Double = 220.64 '[bar]
Public Const rho_krit As Double = 322#  '[kg*m^-3]
Public Const p_min As Double = 0.0061 '[bar]
'die folgende zwei nur fuer Waermeleitfaehigkeit!!!!
Public Const T_krit_L As Double = 647.26 '[K]
Public Const rho_krit_L As Double = 317.7 '[kg*m^-3]

'Wasserdampftafel
' Public Const Gaskonstante_Wasser# = 461.526 '[J/kg*K]
' Public Const Temperatur_c_Wasser# = 647.096
' Public Const Druck_c_Wasser# = 220.64   'in bar
' Public Const Druck_m_Wasser# = 0.0061   '
' Public Const Dichte_c_Wasser# = 322     'in kg/m^3

'Referenzconstante

Public Const My_ref As Double = 10 ^ -6 ' [Pa*s]  Viskositaet
Public Const Lambda_ref As Double = 10 ^ -3 ' [Pa*s]   Viskositaet

'Konstanten fuer boundary function
Public Const n_Bereich4_1# = 0.11670521452767 * 10 ^ 4
Public Const n_Bereich4_2# = -0.72421316703206 * 10 ^ 6
Public Const n_Bereich4_3# = -0.17073846940092 * 10 ^ 2
Public Const n_Bereich4_4# = 0.1202082470247 * 10 ^ 5
Public Const n_Bereich4_5# = -0.32325550322333 * 10 ^ 7
Public Const n_Bereich4_6# = 0.1491510861353 * 10 ^ 2
Public Const n_Bereich4_7# = -0.48232657361591 * 10 ^ 4
Public Const n_Bereich4_8# = 0.40511340542057 * 10 ^ 6
Public Const n_Bereich4_9# = -0.23855557567849
Public Const n_Bereich4_10# = 0.65017534844798 * 10 ^ 3