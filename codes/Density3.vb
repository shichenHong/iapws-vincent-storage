Option Explicit
Option Base 1 ' *** Index of arrays begins with 1 ***

'#########################  Object properties  #########################

Private pPressureBar#, pTempCels#, pSubRegion As enuSubregion, pVol#, pDens#


'Enum Type for Property Region, "_c" means subcritical regions
Public Enum enuSubregion
  NotSet = 0
  a = 1: b: c: d: e: f: g: h: i: j: k: l: m: n: o: p: q: r: s: T: u_c: v_c: w_c: x_c: y_c: z_c = 26
End Enum

'#########################  Setter and Getter for properties  #########################

Public Property Let PressureBar(value As Double)
  pPressureBar = value
End Property

Public Property Get PressureBar() As Double
  PressureBar = pPressureBar
End Property

Public Property Let TempCels(value As Double)
  pTempCels = value
End Property

Public Property Get TempCels() As Double
  TempCels = pTempCels
End Property

Public Property Let SubRegion(value As enuSubregion)
  pSubRegion = value
End Property

Public Property Get SubRegion() As enuSubregion
  SubRegion = pSubRegion
End Property

Public Property Let Vol(value As Double)
  pVol = value
End Property

Public Property Get Vol() As Double
  Vol = pVol
End Property

Public Property Let Dens(value As Double)
  pDens = value
End Property

Public Property Get Dens() As Double
  Dens = pDens
End Property


'default value of properties by initiation of the class
Private Sub Class_Initialize()
  PressureBar = -1#
  TempCels = -1#
  SubRegion = NotSet
  Vol = -1#
  Dens = -1#
End Sub

'######################################################################
'#########################  Methods of class  #########################
'######################################################################

''Simple unit converting methods
Public Function CelsToKelv(Optional TCels As Double) As Double
  'Test condition Abs(TCels) < 1e-10 due to the fact, that Double not exactly Zero is.
  CelsToKelv = IIf(Abs(TCels) < 0.00000000001, Me.TempCels + 273.15, TCels + 273.15)
End Function

Public Function KelvToCels(TKelv As Double) As Double
  KelvToCels = TKelv - 273.15
End Function

Public Function BarToMPa(Optional pBar As Double) As Double
  BarToMPa = IIf(Abs(pBar) < 0.0000000001, Me.PressureBar * 0.1, pBar * 0.1)
End Function

Public Function MPaToBar(pMPa As Double) As Double
  MPaToBar = pMPa * 10
End Function


''Method to validate if the t,p point falls in the Region 3
Public Function RangeValidation() As Boolean
  Dim tempValid As Boolean: tempValid = False
  Dim pressureValid As Boolean: pressureValid = False
  Dim tUpper#, tUnder#, pUpper#, pUnder#
  
  tUpper = Me.KelvToCels(863.15)
  tUnder = Me.KelvToCels(623.15)
  pUpper = 1000 'bar, in Guideline 100MPa
  pUnder = WD_PB_T(pTempCels)
  
  If (pUnder = -1) Then
    RangeValidation = False
    MsgBox ("Pressure under Minimun of IAPWS-Regions")
    Exit Function
  End If
  
  tempValid = IIf(pTempCels <= tUnder Or pTempCels > tUpper, False, True)
  pressureValid = IIf(pPressureBar <= pUnder Or pPressureBar > pUpper, False, True)
  If (tempValid And pressureValid) Then
   RangeValidation = True
  Else
    RangeValidation = False
    MsgBox ("(T,p) point is outside region3!")
  End If
End Function

'Temperaturs on the Boundries are necessary for ascertain SubRegion
'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
' !!!!! Attention: the results are in the unit of KELVIN !!!!!!
'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Public Function getTBoundryValue(ByVal which As enuSRegionBoundray) As Double
  Dim Coefficient As New Density3Coeff
  '1st dimension: 1 = I, 2 = n; 2nd dimenstion: i
  Dim TBKoeff As Variant
  TBKoeff = Coefficient.getBoundryCoeff(which)
  'dimensionless variable
  Dim pi As Double
  pi = BarToMPa()
  
  Dim i As Integer, temp As Double, theta As Double
  If which = 3 Then 'Boundry ef use formular (3) on p9 without loop
    theta = 3.727888004 * (pi - 22.064) + 647.096
    getTBoundryValue = theta
  Else
    For i = LBound(TBKoeff(1)) To UBound(TBKoeff(1))
      Select Case which
      Case 1, 8, 12 'Boundry ab, op and uv use formular (1) on p9
      temp = TBKoeff(2)(i) * Log(pi) ^ TBKoeff(1)(i)
      Case Else ' Other Boundries use formular (2) on p9
      temp = TBKoeff(2)(i) * pi ^ TBKoeff(1)(i)
      End Select
      theta = theta + temp
    Next i
  getTBoundryValue = theta
  End If
End Function

'test if the (T,p) point lies in the critical area
Public Function identCrit() As Boolean
  Dim boolTCrit1 As Boolean, boolTCrit2 As Boolean, boolPCrit1 As Boolean, boolPCrit2 As Boolean
  boolTCrit1 = CelsToKelv(pTempCels) > getTBoundryValue(qu)
  boolTCrit2 = CelsToKelv(pTempCels) - getTBoundryValue(rx) <= 0.0000000001
  boolPCrit1 = pPressureBar > X_WD_PS_T(KelvToCels(643.15))
  boolPCrit2 = pPressureBar - 225 <= 0.0000000001
  
  If boolTCrit1 And boolTCrit2 And boolPCrit1 And boolPCrit2 Then
  identCrit = True
  Else: identCrit = False
  End If
End Function

'important function to ascertain the SubRegion of the (T,p) point
Public Sub getSubRegion()
' If the point is close to the critical point, jump directly to the second part
  If identCrit() Then GoTo Critical

  ' In non-critical area, first locate the position of p
  Dim arrayP(10) As Double
  arrayP(1) = X_WD_PS_T(KelvToCels(623.15)): arrayP(2) = 190.088118917393
  arrayP(3) = 205: arrayP(4) = X_WD_PS_T(KelvToCels(643.15))
  arrayP(5) = 225: arrayP(6) = 230
  arrayP(7) = 235: arrayP(8) = 250
  arrayP(9) = 400: arrayP(10) = 1000
  
  Dim indexP%  'indexP lies between 1 and 9
  indexP = 5
  
  Do While indexP > 1 And indexP < 10
    If pPressureBar <= arrayP(indexP) Then
      indexP = indexP - 1
    ElseIf (pPressureBar > arrayP(indexP + 1)) Then
      indexP = indexP + 1
    Else: Exit Do
    End If
  Loop
  
  'After p is located, locate the position of T
  'Tempetrues on the boundaries
  Dim T_K, T_Sat As Double
  T_Sat = CelsToKelv(X_WD_TS_P(pPressureBar))
  T_K = CelsToKelv()
  Dim T() As Double
  Dim regIndex As Variant
  Dim IndexT As Integer
 
  Select Case indexP 'Schema on Table 2 Pg.10
    Case 1
      If T_K <= T_Sat Then
        pSubRegion = enuSubregion.c
      Else
        pSubRegion = enuSubregion.T
      End If
    Case 2
      If T_K <= getTBoundryValue(cd) Then
        pSubRegion = enuSubregion.c
      ElseIf T_K >= T_Sat Then
        pSubRegion = enuSubregion.T
      Else: pSubRegion = enuSubregion.s
      End If
    Case 3
      ReDim T(3)
      T(1) = getTBoundryValue(cd)
      T(2) = T_Sat
      T(3) = getTBoundryValue(jk)
      If T_K <= T(1) Then
        pSubRegion = enuSubregion.c
      ElseIf T_K > T(1) And T_K <= T(2) Then
        pSubRegion = enuSubregion.s
      ElseIf T_K > T(2) And T_K <= T(3) Then
        pSubRegion = enuSubregion.r
      Else: pSubRegion = enuSubregion.k
      End If
    Case 4
      ReDim T(4)
      T(1) = getTBoundryValue(cd)
      T(2) = getTBoundryValue(qu)
      T(3) = getTBoundryValue(rx)
      T(4) = getTBoundryValue(jk)
      If T_K <= T(1) Then
      pSubRegion = enuSubregion.c
      ElseIf T_K > T(1) And T_K <= T(2) Then
      pSubRegion = enuSubregion.q
      ElseIf T_K > T(3) And T_K <= T(4) Then
      pSubRegion = enuSubregion.r
      ElseIf T_K > T(4) Then
      pSubRegion = enuSubregion.k
      End If
    Case 5
      ReDim T(7) ' T(1) bis T(7) ansteigend sortiert
      T(1) = getTBoundryValue(cd) 'lowest
      T(2) = getTBoundryValue(gh)
      T(3) = getTBoundryValue(mn)
      T(4) = getTBoundryValue(ef)
      T(5) = getTBoundryValue(op)
      T(6) = getTBoundryValue(ij)
      T(7) = getTBoundryValue(jk) 'highest
      
      regIndex = Array(enuSubregion.c, enuSubregion.l, enuSubregion.m, enuSubregion.n, _
      enuSubregion.o, enuSubregion.p, enuSubregion.j, enuSubregion.k)
      
      'Use the same binary tree algorithm to locate T
      
      IndexT = 4
      Do While IndexT > 0 And IndexT < 7
        If T_K <= T(IndexT) Then
        IndexT = IndexT - 1
        ElseIf (T_K > T(IndexT + 1)) Then
        IndexT = IndexT + 1
        Else: Exit Do
        End If
      Loop
      pSubRegion = regIndex(IndexT + 1)
    Case 6
      ReDim T(5)
      T(1) = getTBoundryValue(cd)
      T(2) = getTBoundryValue(gh)
      T(3) = getTBoundryValue(ef)
      T(4) = getTBoundryValue(ij)
      T(5) = getTBoundryValue(jk)
      
      regIndex = Array(enuSubregion.c, enuSubregion.l, enuSubregion.h, enuSubregion.i, enuSubregion.j, enuSubregion.k)
      
      IndexT = 3
      Do While IndexT > 0 And IndexT < 5
        If T_K <= T(IndexT) Then
        IndexT = IndexT - 1
        ElseIf (T_K > T(IndexT + 1)) Then
        IndexT = IndexT + 1
        Else: Exit Do
        End If
      Loop
      pSubRegion = regIndex(IndexT + 1)
    Case 7
      ReDim T(5)
      T(1) = getTBoundryValue(cd)
      T(2) = getTBoundryValue(gh)
      T(3) = getTBoundryValue(ef)
      T(4) = getTBoundryValue(ij)
      T(5) = getTBoundryValue(jk)
      
      regIndex = Array(enuSubregion.c, enuSubregion.g, enuSubregion.h, enuSubregion.i, enuSubregion.j, enuSubregion.k)
      IndexT = 3
      Do While IndexT > 0 And IndexT < 5
        If T_K <= T(IndexT) Then
        IndexT = IndexT - 1
        ElseIf (T_K > T(IndexT + 1)) Then
        IndexT = IndexT + 1
        Else: Exit Do
        End If
      Loop
      pSubRegion = regIndex(IndexT + 1)
    Case 8
      ReDim T(3)
      T(1) = getTBoundryValue(cd)
      T(2) = getTBoundryValue(ab)
      T(3) = getTBoundryValue(ef)
      
      regIndex = Array(enuSubregion.c, enuSubregion.d, enuSubregion.e, enuSubregion.f)
      IndexT = 2
      Do While IndexT > 0 And IndexT < 3
        If T_K <= T(IndexT) Then
        IndexT = IndexT - 1
        ElseIf (T_K > T(IndexT + 1)) Then
        IndexT = IndexT + 1
        Else: Exit Do
        End If
      Loop
      pSubRegion = regIndex(IndexT + 1)
    Case 9
      If T_K <= getTBoundryValue(ab) Then
        pSubRegion = enuSubregion.a
      Else
        pSubRegion = enuSubregion.b
      End If
      
  End Select
  Exit Sub
  
  '(T,p) in critical region jumps to here
Critical:
  T_K = CelsToKelv()
  ReDim T(5)
  T(1) = getTBoundryValue(qu)
  T(2) = getTBoundryValue(uv)
  T(3) = getTBoundryValue(ef)
  T(4) = getTBoundryValue(wx)
  T(5) = getTBoundryValue(rx)
  If pPressureBar > 221.1 And pPressureBar <= 225 Then
    regIndex = Array(enuSubregion.u_c, enuSubregion.v_c, enuSubregion.w_c, enuSubregion.x_c)
  ElseIf pPressureBar > 220.64 And pPressureBar <= 221.1 Then
    regIndex = Array(enuSubregion.u_c, enuSubregion.y_c, enuSubregion.z_c, enuSubregion.x_c)
  End If
  
  If T_K > T(1) And T_K <= T(2) Then
    pSubRegion = regIndex(1)
  ElseIf T_K > T(2) And T_K <= T(3) Then
    pSubRegion = regIndex(2)
  ElseIf T_K > T(3) And T_K <= T(4) Then
    pSubRegion = regIndex(3)
  ElseIf T_K > T(4) And T_K <= T(5) Then
    pSubRegion = regIndex(4)
  End If
End Sub

' calculate the density and the specific volumn
Public Sub getDensity()
  If SubRegion = NotSet Then
  Call getSubRegion
  Else
  End If
    
  'Instantiate the Density3Coeff
  Dim Coef As New Density3Coeff
  Coef.SReg = pSubRegion
  
  'CoeffPara is a 1-dim vector consists of 1-v, 2-p, 3-T, 4-N, 5-a, 6-b, 7-c, 8-d, 9-e. Ref to Table 4 on page 12
  Coef.getCoeff (Para)
  Dim CoeffPara As Variant
  CoeffPara = Coef.Coeff
  
  'CoeffPara is a 2-dim matrix, 1st dim: 1-I,2-J,3-n; 2nd dim: index i
  Coef.getCoeff (Func)
  Dim CoeffFunc As Variant
  CoeffFunc = Coef.Coeff
  
  'destroy the object to release RAM space
  Set Coef = Nothing
  
  'dimensionless variables
  Dim omega#, pi#, theta#
  omega = 0
  pi = BarToMPa(pPressureBar) / CoeffPara(2)
  theta = CelsToKelv(pTempCels) / CoeffPara(3)
  
  'index variable
  Dim i As Integer
  
  'All subRegion except n share one equation
  Select Case pSubRegion
  Case 14 ' SubRegion 14 corresponds to SubRegion n
  ' Equation(4) on Page 12
    'For i = LBound(CoeffFunc(1)) To UBound(CoeffFunc(1))
    For i = 1 To CoeffPara(4)
      omega = omega + CoeffFunc(3)(i) * (pi - CoeffPara(5)) ^ CoeffFunc(1)(i) * (theta - CoeffPara(6)) ^ CoeffFunc(2)(i)
    Next i
    omega = Exp(omega)
  Case Else
    'For i = LBound(CoeffFunc(1)) To UBound(CoeffFunc(1))
    For i = 1 To CoeffPara(4)
      omega = omega + CoeffFunc(3)(i) * ((pi - CoeffPara(5)) ^ CoeffPara(7)) ^ CoeffFunc(1)(i) * ((theta - CoeffPara(6)) ^ CoeffPara(8)) ^ CoeffFunc(2)(i)
    Next i
    omega = omega ^ CoeffPara(9)
  End Select
  
  pVol = omega * CoeffPara(1)
  pDens = 1 / pVol
End Sub


