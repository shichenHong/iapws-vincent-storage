
ProcessExcel readme
-----------------------------

Version 13.0 (Build 141111)
Juli 2016

Technische Universit�t Dresden
Fakult�t Maschinenwesen
Institut f�r Verfahrenstechnik und Umwelttechnik
Professur f�r Energieverfahrenstechnik

Website: http://www.kraftwerkstechnik-dresden.de/
Kontakt: konrad.treppe@tu-dresden.de


Installation
------------

1. Office 32 bit: ProcessExcel32.exe
2. Office 64 bit: ProcessExcel64.exe


Update History
--------------

----------------------------------
13.0 (Build 141110) - Juli 2016

- Modul Vergasung Simplex hinzugef�gt
- Kompatibilit�t des Frameworks f�r neuere Windows-Versionen (8 und 10) hergestellt

__________________________________
----------------------------------
12.1.1 (Build 141110) - November 2014

- diverse kleine Fehler aufgehoben.

Versionsnummern:
Funktionenbibl.dll	1.1.3.0
FunktionenDec.xla	3.2.3.1
VWS-Werkzeuge.xla	2.2.0.8
VWS-Modellierung.xls	12.1

----------------------------------
12.1 (Build 140917) - September 2014

- Automatische Registrierung von Funktionen in die entsprechenden Kategorien unter Excel 2010,
- Erg�nzung der Funktion zur Berechnung des bin�ren Diffusionskoeffizienten nach Fuller-Methode (Quelle: VDI-WA): VWS_WS�_D12_Fuller,
- Kompatibilit�tsproblem mit VBA 7 64 bit gel�st.

Versionsnummern:
Funktionenbibl.dll	1.1.2.1 (FunktionenLib.dll integriert)
FunktionenDec.xla	3.2.3.1
VWS-Werkzeuge.xla	2.2.0.8
VWS-Modellierung.xls	12.1

----------------------------------
12.0 (Build 121205) - Dezember 2012

- Erg�nzung des Links (Programme/SWIG) zur Berechnung von Stoffwerten (aber das Programm ist noch nicht verf�gbar) und
- Kompatibilit�tsproblem mit VBA 7 64 bit leider noch nicht gel�st.

Versionsnummern:
FunktionenLib.dll	5.0.0.29
Funktionenbibl.dll	1.1.2.1
FunktionenDec.xla	3.2.3.0
VWS-Werkzeuge.xla	2.2.0.8
VWS-Modellierung.xls	12.0

----------------------------------
11.1 (Build 110919) - September 2011

- Funktionen zur Berechnung der Emissions- und Absorptionsgrade eines H2O- und CO2-enthaltenen Gases, sowie der �quivalenten Schichtdicke bei der Gasstrahlung (Kategorie: VWS-Excel_WS�), 
- Funktionen zur Berechnung der Selbst- und bin�ren Diffuisionskoeffizienten bei der Stoff�bertragung (Kategorie: VWS-Excel_WS�),
- Funktionen und Benutzeroberfl�che (Programme/SOLIGS) zum L�sen von quadratischen linearen Gleichungssystemen,
- Bezeichnungs�nderung der Funktionen, die dynamische oder kinematische Viskosit�t eines Gases berechnen: von "Viskositaet" auf "Viskosit�t", und
- VWS-Excel 11.1 (Build 0919) f�r Windows XP x86, Windows vista/7 x86 und Windows Vista/7 x64 verf�gbar. Bitte bei der Installation die Windows-Version beachten.

Versionsnummern:
FunktionenLib.dll	5.0.0.29
Funktionenbibl.dll	1.1.2.1
FunktionenDec.xla	3.2.2.4
VWS-Werkzeuge.xla	2.2.0.8
VWS-Modellierung.xls	11.1

----------------------------------
