import java.util.Arrays;
public class BinarySearch{
	public static int rank(int key, int[] a){
		//Array muss sortiert sein
		int lo = 0 ;
		int hi = a.length - 1;
		while (lo <= hi){
			// Schlüssel key ist in a[lo..hi] oder nicht vorhanden.
			int mid = lo + (hi - lo) / 2;
			if (key < a[mid]) hi = mid - 1;
			else if (key > a[mid] lo = mid + 1);
			else		return mid;
		}
		return -1;
	}
	
	public static void main(String[] args){
		int[] whitelist = In.readInts(args[0]);
		Arrays.sort(whitelist);
		while(!StdIn.isEmpty()){
			// Liest Schlüssel ein, gibt ihn aus, wenn er nicht in der Positivliste ist.
			int key = StdIn.readInt();
			if(rank(key, whitelist) < 0)
				StdOut.println(key)
		}
	}
}